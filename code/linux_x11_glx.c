#define GLX_GLXEXT_PROTOTYPES
#include <GL/glx.h>
#include <GL/glxext.h>

GLXContext glx_Context;
Window glx_Window;
Display *glx_Display;
int glx_ContextWasCreated = 0;

// We create a dummy context to use with glXSwapBuffers for our vsync
// Note: This is not the recommended way to create a generic OpenGL context!
// You would want to use XCreateWindow and manually fill out all of the function arguments yourself
// This is just a lazy way of doing it since we don't actually need to render anything to it

void glx_CreateGLXContext(Display *Dpy)
{
	int AttribList[] =
	{
		GLX_RGBA,
		GLX_DOUBLEBUFFER,
		GLX_RED_SIZE, 1,
		GLX_GREEN_SIZE, 1,
		GLX_BLUE_SIZE, 1,
		None
	};

	glx_ContextWasCreated = 0;

	glx_Window = XCreateSimpleWindow(Dpy, DefaultRootWindow(Dpy), 0, 0, 640, 480, 0, 0, 0);
	XVisualInfo *VisualInfo = glXChooseVisual(Dpy, DefaultScreen(Dpy), AttribList);
	glx_Context = glXCreateContext(Dpy, VisualInfo, 0, True);

	// Do we want to crash here?
	if(!glx_Context)
	{
		Pretty("ERROR: Could not create GLX Context. Falling back to old vsync routine");
		return;
	}

	if(!glXMakeCurrent(Dpy, glx_Window, glx_Context))
	{
		Pretty("ERROR: Call to glXMakeCurrent failed. Falling back to old vsync routine");
		return;
	}

	glXSwapIntervalEXT(Dpy, glx_Window, 1);

	glx_ContextWasCreated = 1;

	glx_Display = Dpy;
	int minor;
	int major;
	glXQueryVersion(Dpy, &minor, &major);
	Pretty("Created GLX context: version %d.%d\n", minor, major);
}
