// If clang is desired, you should use clang-cl for compatability with Microsoft-style compiler switches
#define CC "cl.exe"

#define CFLAGS "-FC"
#define DEF "-D_CRT_SECURE_NO_WARNINGS"
const char *OPT[] = {"-O2", "-GA", "-fp:fast"};
const char *INCLUDE_THIRDPARTY[] = {"-I.", "-Iinclude", "-Igame"};

#define win32_add_common_commands() impl_win32_add_common_commands(buffer, target_name)
void impl_win32_add_common_commands(command_buffer *buffer, const char *target_name)
{
	const char *OUTPUT = concat2("-Fe:", target_name); // clang-cl doesn't understand -Fe: <file>, only Fe:file
	command_add(OUTPUT);
	command_add_from_stack_array(OPT);
	command_add(CFLAGS);
	command_add(DEF);
}


// ==============================================
// ----------------------------------------------
// GL (Windows)
// ==============================================
// ----------------------------------------------
BUILD_FUNC(win32_build_gl)
{
	const char *LIB[] = {"-link", "opengl32.lib",  "gdi32.lib", "user32.lib", "dwmapi.lib", "lib/win/glfw3dll.lib"};
	
	command_add(CC, "-nologo", "win32_gl_platform.c");
	win32_add_common_commands();
	command_add_from_stack_array(INCLUDE_THIRDPARTY);
	command_add_from_stack_array(LIB);
	
	if(AUDIO_ENABLED)
	{
		command_add("lib/win/OpenAL32.lib");
	}
}

POST_FUNC(win32_post_gl)
{
	char *glfw = make_path("lib", "win", "glfw3.dll");
	char *openal = make_path("lib", "win", "OpenAL32.dll");
	
	move_file(target_name, BUILD_DIR);
	copy_file(glfw, BUILD_DIR, CPY_NO_CLOBBER);
	copy_file(openal, BUILD_DIR, CPY_NO_CLOBBER);
}


// ==============================================
// ----------------------------------------------
// SDL2 (Windows)
// ==============================================
// ----------------------------------------------
BUILD_FUNC(win32_build_sdl2)
{
	const char *LIB[] = {"-link", "shell32.lib", "lib/win/SDL2.lib", "lib/win/SDL2main.lib"};
	
	command_add(CC, "-nologo", "win32_sdl2_platform.c");
	win32_add_common_commands();
	command_add_from_stack_array(INCLUDE_THIRDPARTY);
	command_add_from_stack_array(LIB);
	
	if(AUDIO_ENABLED)
	{
		command_add("lib/win/SDL2_mixer.lib");
	}
	
	// Note: This is to enable console logging
	// Without this linker flag, printf and friends don't generate any output
	// SDL2main.lib seems to generate a helper function in WinMain that allows us to use int main() 
	// as an entry point without explicitly specifying WinMain ourselves
	// Whether there are drawbacks to doing this are unknown by me
	command_add("-subsystem:console");
}

POST_FUNC(win32_post_sdl2)
{
	char *lib_base = make_path("lib", "win");
	
	char *sdl2 = make_path(lib_base, "SDL2.dll");
	char *mixer = make_path(lib_base, "SDL2_mixer.dll");
	
	move_file(target_name, BUILD_DIR);
	copy_file(sdl2, BUILD_DIR, CPY_NO_CLOBBER);
	
	if(AUDIO_ENABLED)
	{
		char *ogg = make_path(lib_base, "libogg-0.dll");
		char *vorbisfile = make_path(lib_base, "libvorbis-0.dll");
		char *vorbis = make_path(lib_base, "libvorbisfile-3.dll");
		
		
		copy_file(mixer, BUILD_DIR, CPY_NO_CLOBBER);
		copy_file(ogg, BUILD_DIR, CPY_NO_CLOBBER);
		copy_file(vorbis, BUILD_DIR, CPY_NO_CLOBBER);
		copy_file(vorbisfile, BUILD_DIR, CPY_NO_CLOBBER);
	}
}


// ==============================================
// ----------------------------------------------
// GDI (Windows)
// ==============================================
// ----------------------------------------------
BUILD_FUNC(win32_build_gdi)
{
	const char *INCLUDE[] = {"-I.", "-Igame"};
	const char *LIB[] = {"-link", "user32.lib", "gdi32.lib", "winmm.lib", "dsound.lib", "dwmapi.lib"};
	
	command_add(CC, "-nologo", "win32_gdi_platform.c");
	win32_add_common_commands();
	command_add_from_stack_array(INCLUDE);
	command_add_from_stack_array(LIB);
}

POST_FUNC(win32_post_gdi)
{
	move_file(target_name, BUILD_DIR);
}


#if 0

// gcc win32_gl_platform.c -O2 -Iinclude -Igame -I. -lopengl32
// -luser32 -ldwmapi -L lib/win -l:glfw3.dll -l:OpenAL32.dll
// ==============================================
// ----------------------------------------------
// GL (Windows, Mingw)
// Not sure what define to look for,
// so this is disabled for now.
// However, this code path has been tested
// and it does compile.
// ==============================================
// ----------------------------------------------
BUILD_FUNC(win32_mingw_build_gl)
{
	const char *INCLUDE[] = {"-I.", "-Iinclude", "-Igame"};
	const char *LIB[] = {"-lopengl32", "-lgdi32", "-luser32", "-ldwmapi", "-l:glfw3.dll"};
	
	command_add("gcc", "win32_gl_platform.c");
	command_add("-o", target_name);
	command_add("-O2");
	command_add_from_stack_array(INCLUDE);
	command_add("-L", "lib/win");
	command_add_from_stack_array(LIB);
	
	if(AUDIO_ENABLED)
	{
		command_add("-l:OpenAL32.dll");
	}
}


POST_FUNC(win32_mingw_post_gl)
{
	char *glfw = make_path("lib", "win", "glfw3.dll");
	char *openal = make_path("lib", "win", "OpenAL32.dll");
	char *openal_dst = make_path(BUILD_DIR, "soft_oal.dll");
	
	move_file(target_name, BUILD_DIR);
	copy_file(glfw, BUILD_DIR, CPY_NO_CLOBBER);
	copy_file(openal, openal_dst, CPY_NO_CLOBBER);
}

#endif

int main(int argc, char **argv)
{
	build_directories_create();
	
	// Pick one or more target as desired
	target_add("break-arcade-games-out-win32-gl.exe", win32_build_gl, win32_post_gl);
	// target_add("break-arcade-games-out-win32-gdi.exe", win32_build_gdi, win32_post_gdi);
	// target_add("break-arcade-games-out-win32-sdl2.exe", win32_build_sdl2, win32_post_sdl2);
	//target_add("break-arcade-games-out-win32_mingw-gl.exe", win32_mingw_build_gl, win32_mingw_post_gl);
	
	build_run_all(argc, argv);
	post_run_all();
	copy_assets();
	create_config_file();
}
