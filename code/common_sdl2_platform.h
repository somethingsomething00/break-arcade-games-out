
#ifndef _COMMON_SDL2_PLATFORM_H_
#define _COMMON_SDL2_PLATFORM_H_

/**********************************
* Platform includes
**********************************/
// Standard
//--------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// SDL2
//--------------------------------------------------------------------------------
#include <SDL2/SDL.h>

/**********************************
* Platform types
**********************************/
enum
{
	PLATFORM_WINDOWED,
	PLATFORM_FULLSCREEN,
};

typedef struct
{
	s32 xTotal;
	s32 yTotal;
	s32 xOld;
	s32 xCur;
	
	s32 yOld;
	s32 yCur;
} mouse;

// Keysym abstraction for sdl
typedef struct
{
	b32 keycode;
	b32 isDown;
	b32 isHeld;
} key;

typedef struct
{
	key Key[SDL_NUM_SCANCODES];
} keys;

#endif /* _COMMON_SDL2_PLATFORM_H_ */
