// NAME: common_audio_dummy.h
// TIME OF CREATION: 2022-04-23 13:00:01
// AUTHOR:
// DESCRIPTION: This file contains dummy function and struct definitions
// 				to satisfy the game code if it's compiled without audio support


#ifndef _COMMON_AUDIO_DUMMY_H_
#define _COMMON_AUDIO_DUMMY_H_

// Note: Loaded_Sound is not a union in the original definition
// This is simply a space saving measure
typedef struct
{
	union
	{
		int dummy;
		// Have at least one dummy struct member

		int assetId;
	};
} Loaded_Sound;

// Note: Playing_Sound is not a union in the original definition
// This is simply a space saving measure
typedef struct Playing_Sound Playing_Sound;
struct Playing_Sound
{
	union
	{
		float volume;
		float target_volume;
		float fading_speed;
		float speed_multiplier;

		float *source_x;

		Playing_Sound *synced_sound;
	};
};

Playing_Sound playing_sound_dummy = {0};
Loaded_Sound loaded_sound_dummy = {0};

// game.c functions

#define play_sound(sound, looping) &playing_sound_dummy
#define play_sound_with_variation(...) play_sound(NULL, 0)
#define set_volume(...)


// common_audio functions

#define snd_Init(...)
#define snd_Shutdown(...)
#define snd_Update(...)
#define snd_Log(...)
#define snd_PauseAllSources(...)
#define snd_ResumeAllSources(...)

#define snd_LoadWavFromMemory(...) loaded_sound_dummy
#define snd_LoadOggFromMemory(...) loaded_sound_dummy
#define snd_AsyncLoadOggFromMemory(...)


#endif /* _COMMON_AUDIO_DUMMY_H_ */
