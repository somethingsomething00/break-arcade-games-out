#define CC "gcc"

const char *CFLAGS[] = {"-std=gnu11"};
const char *LINUX_OPT_COMMON[] = {"-O2", "-ffast-math", "-march=native", "-mtune=native", "-funroll-loops"};
const char *LINUX_LIB_COMMON[] = {"-lm", "-lpthread"};
const char *LINUX_INCLUDE_COMMON[] = {"-I.", "-Iinclude", "-Igame"};

#define linux_add_common_commands() impl_linux_add_common_commands(buffer, target_name)
void impl_linux_add_common_commands(command_buffer *buffer, const char *target_name)
{
	const char *OUTPUT[] = {"-o", target_name};
	command_add_from_stack_array(OUTPUT);
	command_add_from_stack_array(CFLAGS);
	command_add_from_stack_array(LINUX_OPT_COMMON);
	command_add_from_stack_array(LINUX_INCLUDE_COMMON);
	command_add_from_stack_array(LINUX_LIB_COMMON);
}

// ==============================================
// ----------------------------------------------
// GL (Linux)
// ==============================================
// ----------------------------------------------
BUILD_FUNC(linux_build_gl)
{
	const char  *LIB[] = {"-lGL", "-lglfw"};
	
	command_add(CC, "linux_gl_platform.c");
	linux_add_common_commands();
	command_add_from_stack_array(LIB);

	if(AUDIO_ENABLED)
	{
		command_add("-lopenal");
	}
}

POST_FUNC(linux_post_gl)
{
	move_file(target_name, BUILD_DIR);
}


// ==============================================
// ----------------------------------------------
// X11 (Linux)
// ==============================================
// ----------------------------------------------
BUILD_FUNC(linux_build_x11)
{
	const char  *LIB[] = {"-lX11", "-lXrandr", "-lXext"};
	
	command_add(CC, "linux_x11_platform.c");
	linux_add_common_commands();
	command_add_from_stack_array(LIB);

	if(X_USE_GLX_FOR_VSYNC)
	{
		command_add("-lGL");
	}
	
	if(AUDIO_ENABLED)
	{
		command_add("-lopenal");
	}
}

POST_FUNC(linux_post_x11)
{
	move_file(target_name, BUILD_DIR);
}


// ==============================================
// ----------------------------------------------
// SDL2 (Linux)
// ==============================================
// ----------------------------------------------
BUILD_FUNC(linux_build_sdl2)
{
	const char *LIB[] = {"-lSDL2"};
	
	command_add(CC, "linux_sdl2_platform.c");
	linux_add_common_commands();
	command_add_from_stack_array(LIB);
	
	if(AUDIO_ENABLED)
	{
		command_add("-lSDL2_mixer");
	}
}

POST_FUNC(linux_post_sdl2)
{
	move_file(target_name, BUILD_DIR);
}

int main(int argc, char **argv)
{
	build_directories_create();
	
	// Pick one or more target for your platform
	target_add("break-arcade-games-out-linux-gl", linux_build_gl, linux_post_gl);
	// target_add("break-arcade-games-out-linux-x11", linux_build_x11, linux_post_x11);
	// target_add("break-arcade-games-out-linux-sdl2", linux_build_sdl2, linux_post_sdl2);
	
	build_run_all_threaded(argc, argv);
	post_run_all();
	copy_assets();
	create_config_file();
	
	printf("\nCompilation finished!\nYour files should be in %s\n", BUILD_DIR);
}
