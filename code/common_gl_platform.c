//Note: This code is now shared between Linux and Windows builds.
#define Dowhile(body) do {body} while(0)
#define Assert(cond) \
Dowhile(if(!(cond)) {fprintf(stderr, "%s:%d: Assertion failed for expression '%s'\n", __FILE__, __LINE__, #cond); exit(1);})

#define ArrayCount(arr_) \
(sizeof(arr_) / sizeof(arr_)[0])




/**********************************
* Config
**********************************/
#define GL_USE_RENDERER_HW 1
// Must be defined as 1 to include the OpenGL renderer in r_renderer.c

#include "config.h"


// Original game code with slight modifications
//------------------------------------------------------------------------------
#include "game.h"
#include "utils.c"
#include "maths.c"
#include "string.c"
#include "platform_common.c"

// Globals that were not predeclared anywhere as extern
global_variable Render_Buffer render_buffer;
global_variable f32 current_time;
global_variable b32 lock_fps = true;

// Compiler specific intrinsic
// Unused on linux
#ifdef _WIN32
#define interlocked_compare_exchange(a, b, c) InterlockedCompareExchange((volatile long*)a, b, c)
#endif

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#define STBI_NO_STDIO
#define STBI_NO_FAILURE_STRINGS
#define STBI_ASSERT assert
#include "stb_image.h"

// Use x86 intrinsics for certain routines
#include "common_intrinsics.c"

// Job system for OGG loading (it is general purpose though)
#include "common_jsys.h"
#include "common_jsys.c"

#include "cooker_common.c"
#include "ogg_importer.h"
#include "common_audio.c"
#include "asset_loader.c"
#include "profiler.c"
#include "r_renderer.c"
#include "console.c"
#include "game.c"
//------------------------------------------------------------------------------

// Note on the os_ functions:

// These are used internally by the game, with prototypes given in platform_common.c.
// They were already a part of the game code as originally released.
// The programmer is responsible for providing a platform specific implementation.
// that follows the interface set by the prototypes.


// Note on the naming convention:

// The variable and function names following these comments are prefixed with gl_
// to indicate that they are specific to the platform layer.

// I tried to follow this convention as appropriately as possible to distinguish functions and variables that
// one may expect to provide themselves should they choose to port this code to a different platform.

// Some are purely convenience functions, but most do serve a purpose in servicing the game loop.

// Intermediate variables do not follow this convention.


// The rest is new code
//--------------------------------------------------------------------------------
/**********************************
* Platform macros
**********************************/
#define AssertMessage(cond, ...) \
Dowhile(if(!(cond)) {fprintf(stderr, "%s:%d: Assertion failed for expression '%s'\n", __FILE__, __LINE__, #cond); \
fprintf(stderr, __VA_ARGS__); \
exit(1);})

#define Pretty(...) \
Dowhile(printf("%s:%d: ", __FILE__, __LINE__); printf(__VA_ARGS__);)

#define PrettyError(...) \
Dowhile(printf("%s:%d: ", __FILE__, __LINE__); printf("\033[31m"); printf(__VA_ARGS__); printf("\033[0m");)



#define gl_PrintEnumString(glenum)              \
Dowhile(                                    \
const GLubyte *glString;                    \
glString = glGetString((glenum));           \
if(glString)                                \
{                                           \
printf("%s: %s\n", #glenum, (glString));\
})


// @Bug: The game hardcodes the aspect ratio to be 16:9
// Other aspect ratios will work but they do tend to look funny
// Warning: Do not use dimensions above 1x with the software renderer
// Your CPU will thank you :)
#define FB_WIDTH_HW (1280 * 1)
#define FB_HEIGHT_HW (720 * 1)


// glTexImage2D is far too slow to deal with larger sizes
// We leave it at 1x
#define FB_WIDTH_SW (1280 * 1)
#define FB_HEIGHT_SW (720 * 1)

#define PLATFORM_BYTES_PER_PIXEL 4

/**********************************
* Platform headers
**********************************/
#include "common_gl_platform.h"
#include "platform_profiler.h"
#include "platform_profiler.c"




/**********************************
* Platform globals
**********************************/
static GLFWwindow *gl_Window;
static GLFWmonitor *gl_Monitor;
static const GLFWvidmode *gl_Vidmode;
static Input gl_Input;
static b32 gl_TimeToQuit;
static b32 gl_Paused;
static b32 gl_WasPaused;
static gl_Screen_t gl_Screen;
static gl_Texture gl_SoftwareBuffer;
static gl_Texture gl_PauseTexture;
static double gl_TargetRefreshSeconds;
renderer_type gl_RendererType;
b32 gl_WindowIsFullscreen;
b32 gl_FullscreenWindowIsBorderless;

/**********************************
* Platform functions
**********************************/
static void gl_CbMouseButton(GLFWwindow *Window, int button, int action, int mod);
static void gl_CbResize(GLFWwindow *Window, int x, int y);
static void gl_CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod);
static void gl_CbWinClose(GLFWwindow *Window);
static void gl_PauseGraphicDraw();
static void gl_CursorEnable(GLFWwindow *Window);
static void gl_CursorDisable(GLFWwindow *Window);
static void gl_FramebufferInit(Render_Buffer *Buffer, u32 width, u32 height, u32 bpp);
static void gl_Texture2DDrawFullscreen(GLuint id);
static void gl_UpdateAndRender(Render_Buffer *Buffer, gl_Texture *Texture, f32 dt);
static void gl_CallbackInit();
static void gl_PollKeys(GLFWwindow *Win, Input *input);
static void gl_PrintDiagnostics();
static void gl_ToggleWireframe();
static void gl_RendererInit();
static void gl_ChangeRendererIfNecessary();
static gl_Texture gl_Texture2DCreate(u32 width, u32 height, u32 bpp, void *data);
static void gl_SwapBuffersMain(GLFWwindow *Window);
static void gl_SetWindowMode(window_mode WindowMode);
static void gl_SetFullscreenMode(window_mode WindowMode);


window_mode FullscreenMode[2] = 
{
	[0] = WINDOW_MODE_FULLSCREEN,
	[1] = WINDOW_MODE_FULLSCREEN_BORDERLESS,
};


// This is a dumb hack but it's what we have to do to get around Windows' SwapBuffers driver bug
// This version is basically a no-op
#ifndef GLFW_CUSTOM_SWAP_BUFFERS_IMPLEMENTATION
void glfw_SwapBuffers(GLFWwindow *Window)
{
	glfwSwapBuffers(Window);
}
void glfw_InitForOS(GLFWwindow *Window)
{
	return;
}
#endif

int main(int argc, char **argv)
{
	// Sanity check to make sure our framebuffer is the size that we expect
	Assert(sizeof *render_buffer.pixels == PLATFORM_BYTES_PER_PIXEL);
	
	jsys_Init();
	snd_Init();
	
	glfwInit();
	
	gl_FullscreenWindowIsBorderless = PLATFORM_PREFERRED_BORDERLESS_DEFAULT;
	gl_WindowIsFullscreen = true;
	
	glfwWindowHint(GLFW_DECORATED, gl_FullscreenWindowIsBorderless ? GLFW_FALSE : GLFW_TRUE);
	
	gl_Monitor = glfwGetPrimaryMonitor();
	gl_Vidmode = glfwGetVideoMode(gl_Monitor);
	gl_Window = glfwCreateWindow(gl_Vidmode->width, gl_Vidmode->height, PLATFORM_WINDOW_NAME, gl_FullscreenWindowIsBorderless ? 0 : gl_Monitor, NULL);
	gl_SetWindowMode(FullscreenMode[gl_FullscreenWindowIsBorderless]);
	glfw_InitForOS(gl_Window);
	
	
	// Some sensible screen dimension defaults
	gl_Screen.width =  gl_Vidmode->width;
	gl_Screen.height = gl_Vidmode->height;
	gl_Screen.windowedWidth = gl_Vidmode->width / 2;
	gl_Screen.windowedHeight = gl_Vidmode->height / 2;
	gl_Screen.windowedX = (gl_Screen.width / 2) - (gl_Screen.windowedWidth / 2);
	gl_Screen.windowedY = (gl_Screen.height / 2) - (gl_Screen.windowedHeight / 2);
	Pretty("glfwCreateWindow: %dx%d window created\n", gl_Vidmode->width, gl_Vidmode->height);
	
	// 'Bind' our window
	// Note: glfwSwapInterval MUST be called before glfwMakeContextCurrent for this codebase
	glfwSwapInterval(1);
	glfwMakeContextCurrent(gl_Window);
	
	gl_CallbackInit();
	
	gl_FramebufferInit(&render_buffer, FB_WIDTH_SW, FB_HEIGHT_SW, PLATFORM_BYTES_PER_PIXEL);
	
	gl_SoftwareBuffer = gl_Texture2DCreate(FB_WIDTH_SW, FB_HEIGHT_SW, PLATFORM_BYTES_PER_PIXEL, NULL);
	gl_PauseTexture = gl_Texture2DCreate(4, 4, PLATFORM_BYTES_PER_PIXEL, NULL);
	
	gl_CursorDisable(gl_Window);
	
	gl_PrintDiagnostics();
	
#if PLATFORM_PROFILER
	u32 debug_FramesElapsed = 0;
	double debug_MsElapsed = 0;
#endif
	
	b32 firstMouse = true;
	double t1 = 0;
	double t2 = 0;
	double dtCur = 0;
	double dtPrev = 0;
	double mouseXCurrent = 0;
	double mouseYCurrent = 0;
	double mouseXOld = 0;
	double mouseYOld = 0;
	v2i gl_MouseDp = {0};
	v2i gl_MouseDpOld = {0};
	
	gl_TargetRefreshSeconds = 1.0 / gl_Vidmode->refreshRate;
	
	gl_RendererType = R_HW;
	gl_RendererInit();
	

	//
	// Main loop
	//
	glClearColor(0, 0, 0, 1);
	while(!gl_TimeToQuit)
	{
		t1 = glfwGetTime();
		glfwPollEvents();
		
		gl_ChangeRendererIfNecessary();
		if(gl_RendererType == R_SW)
		{
			glClear(GL_COLOR_BUFFER_BIT);
		}
		
		gl_PollKeys(gl_Window, &gl_Input);
		
		if(gl_Paused)
		{
			// Set the state of some things here and go into a mini game loop
			gl_MouseDpOld = gl_MouseDp;
			gl_WasPaused = true;
			gl_CursorEnable(gl_Window);
			snd_PauseAllSources();
			
			// If software, this is our pixel framebuffer
			// If hardware, this is the cached OpenGL screen framebuffer 
			GLuint textureId;
			
			if(gl_RendererType == R_HW)
			{
				// Copy current framebuffer to texture
				// It seems like glCopyTexImage2D reserves internal memory for the texture for us,
				// so no need to call glTexImage2D beforehand
				textureId = gl_PauseTexture.id;
				glPushAttrib(GL_POLYGON_BIT | GL_PIXEL_MODE_BIT);
				glDisable(GL_BLEND);
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, textureId);
				glReadBuffer(GL_FRONT);
				glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0, gl_Screen.width, gl_Screen.height, 0);
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				
				// Since our texture displays in OpenGL's default NDC coordinates we forego using glOrtho here
				glPushMatrix();
				glLoadIdentity();
			}
			else
			{
				textureId = gl_SoftwareBuffer.id;
			}
			
			
			while(gl_Paused)
			{
				glClear(GL_COLOR_BUFFER_BIT);
				if(gl_TimeToQuit)
				{
					goto the_end;
				}
				gl_Texture2DDrawFullscreen(textureId);
				gl_PauseGraphicDraw();
				glfw_SwapBuffers(gl_Window);
				glfwWaitEvents();
				continue;
			}
			
			if(gl_RendererType == R_HW)
			{
				glDisable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glPopAttrib();
				glPopMatrix();
			}
			
			gl_CursorDisable(gl_Window);
			snd_ResumeAllSources();
			glfw_SwapBuffers(gl_Window);
			continue;
		}
		
		// Note: We poll the mouse manually to get a better feel of the transition per-frame
		// Note2: Hey it actually worked!
		
		// @Hack to avoid crazy mouse warping at the beginning of the game
		// We can warp the mouse through GLFW's native api if we really wanted to eliminate this branch
		if(firstMouse)
		{
			firstMouse = false;
			glfwGetCursorPos(gl_Window, &mouseXCurrent, &mouseYCurrent);
		}
		else
		{
			glfwGetCursorPos(gl_Window, &mouseXCurrent, &mouseYCurrent);
			gl_MouseDp.x = mouseXCurrent - mouseXOld;
			gl_MouseDp.y = mouseYCurrent - mouseYOld;
		}
		
		if(gl_WasPaused)
		{
			gl_Input.mouse_dp = gl_MouseDpOld;
			gl_WasPaused = false;
		}
		else
		{
			gl_Input.mouse_dp = gl_MouseDp;
		}
		
		double dtFrame;
		double absoluteFrameDeltaSec;
		double epsilon = gl_TargetRefreshSeconds + 0.0005;
		if(dtPrev < epsilon || dtCur < epsilon)
		{
			absoluteFrameDeltaSec = fabs(dtPrev - dtCur);
			if(absoluteFrameDeltaSec < gl_TargetRefreshSeconds) dtFrame = gl_TargetRefreshSeconds;
			else dtFrame = dtPrev;
		}
		else
		{
			dtFrame = dtPrev;
		}
		gl_UpdateAndRender(&render_buffer, &gl_SoftwareBuffer, dtFrame);
		
		// This is a big hack :)
		// Since we are not tracking key repeats we need to clear the changed flag manually each frame
		// The game only acts on keys if the changed flag was in a different state on the previous frame
#define PLATFORM_CLEAR_KEY(breakoutKey) \
gl_Input.buttons[(breakoutKey)].changed = false; \
		
		PLATFORM_CLEAR_KEY(BUTTON_LEFT);
		PLATFORM_CLEAR_KEY(BUTTON_RIGHT);
		PLATFORM_CLEAR_KEY(BUTTON_UP);
		PLATFORM_CLEAR_KEY(BUTTON_DOWN);
		PLATFORM_CLEAR_KEY(BUTTON_ESC);
		PLATFORM_CLEAR_KEY(BUTTON_CFG);
		PLATFORM_CLEAR_KEY(BUTTON_LMB);
		mouseXOld = mouseXCurrent;
		mouseYOld = mouseYCurrent;
		
		glfw_SwapBuffers(gl_Window);
		
		t2 = glfwGetTime();
		dtPrev = dtCur;
		dtCur = (t2 - t1);
		
#if PLATFORM_PROFILER
		debug_MsElapsed += (dtPrev);
		debug_FramesElapsed++;
		if(debug_FramesElapsed == 240)
		{
			printf("%.4f ms per frame\n", (debug_MsElapsed / 240.0) * 1000.0);
			debug_FramesElapsed = 0;
			debug_MsElapsed = 0;
		}
#endif
	}
	the_end:
	free(render_buffer.pixels);
	snd_Shutdown();
	glfwTerminate();
	return 0;
}


internal void os_free_file(String s)
{
	free(s.data);
}

internal String os_read_entire_file(char *file_path)
{
	String S = {0};
	FILE *F = {0};
	s64 filesize;
	F = fopen(file_path, "ab+");
	Pretty("os_read_entire_file: Opening file '%s'\n", file_path);
	Assert(F != NULL);
	fseek(F, 0, SEEK_END);
	filesize = ftell(F);
	Assert(filesize != -1);
	rewind(F);
	if(filesize > 0)
	{
		s64 bytesRead;
		S.data = calloc(filesize, sizeof *S.data);
		Assert(S.data);
		bytesRead = fread(S.data, 1, filesize, F);
		Pretty("os_read_entire_file: %"PRIi64" bytes read for file '%s'\n", bytesRead, file_path);
	}
	S.size = filesize;
	Pretty("os_read_entire_file: %"PRIi64" bytes allocated for '%s'\n", filesize, file_path);
	fclose(F);
	return S;
}

internal String os_read_save_file()
{
	return os_read_entire_file("data/save.brk");
}

internal String os_read_config_file()
{
	Pretty("os_read_config_file: Preparing to open config file '%s'\n", "config.txt");
	return os_read_entire_file("config.txt");
}


#define save_file_error \
Dowhile(PrettyError("os_read_save_file: Unable to write to save file! The game will still function but progress will not be saved\n"); \
goto save_end;)
static b32
os_write_save_file(String data)
{
	// Note on file saves: The game modifies the save contents during runtime (in a buffer)
	// When writing an updated save buffer we want to create a new file with the modified contents,
	// NOT append to the existing file (if one exists)
	
	FILE *TempSave = {0}; /* Write the save data to a temp file first */
	int rc;
	s64 bytesWritten;
	b32 result = true; /* We assume a successful write */
	const char *savefileName = "data/save.brk";
	const char *tempfileName = "data/save.brk.tmp";
	const char *colormodeSet = "\033[0;31m"; // @Hardcoded red color VT escape
	const char *boldSet = "\033[1m";
	TempSave =  fopen(tempfileName, "wb+");
	if(TempSave)
	{
		bytesWritten = fwrite(data.data, 1, data.size, TempSave);
		if(bytesWritten != data.size)
		{
			fclose(TempSave);
			save_file_error;
		}
	}
	else
	{
		save_file_error;
	}
	fclose(TempSave);
#ifndef _WIN32
	rc = rename(tempfileName, savefileName);
#else
	if(!MoveFileEx(tempfileName, savefileName, MOVEFILE_REPLACE_EXISTING))
	{
		rc = GetLastError();
	}
#endif /* !_WIN32 */
	if(rc == 0)
	{
		Pretty("os_write_save_file: %"PRIi64" bytes written to '%s'\n", bytesWritten, savefileName);
	}
	else
	{
		result = false;
		perror("rename");
		Pretty("%sUnable to rename temporary save file\n"
			   "The game was successfully saved under the name %s'%s'%s\n"
			   "Please navigate to its path and rename the file to %s'%s'%s to access your save file on the next launch of the game\n",
			   colormodeSet,
			   boldSet,tempfileName,colormodeSet,
			   boldSet,savefileName,colormodeSet);
	}
	save_end:
	return result;
}

String os_get_pak_data()
{
	return os_read_entire_file("data/assets.pak");
}

internal void
os_toggle_fullscreen()
{
	// Empty
}

#define US_FACTOR 1E6
// @Hardcoded We operate in microseconds rather than seconds because last_counter truncates our time...
f32 os_seconds_elapsed(u64 last_counter)
{
	f32 tNow;
	tNow = glfwGetTime() * US_FACTOR;
	return (f32)(tNow - last_counter) / US_FACTOR;
}

u64 os_get_perf_counter()
{
	return glfwGetTime() * US_FACTOR;
}

// Note: We manually clear the changed flag in main() with PLATFORM_CLEAR_KEY
#define GLKD(glKey, breakoutKey) case (glKey): \
gl_Input.buttons[(breakoutKey)].is_down = true; \
gl_Input.buttons[(breakoutKey)].changed = true; \
break

#define GLKU(glKey, breakoutKey) case (glKey): \
gl_Input.buttons[(breakoutKey)].is_down = false; \
gl_Input.buttons[(breakoutKey)].changed = true; \
break

void gl_CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod)
{
	// Some convenience controls
	
	// Quit
	if((key == GLFW_KEY_Q && action == GLFW_PRESS)
	   || (key == GLFW_KEY_F4 && action == GLFW_PRESS && mod == GLFW_MOD_ALT))
	{
		gl_TimeToQuit = true;
		return;
	}
	
	// Pause
	if((key == GLFW_KEY_P || key == GLFW_KEY_SPACE) && action == GLFW_PRESS)
	{
		gl_Paused = !gl_Paused;
	}
	
	// Switch renderers
	if(key == GLFW_KEY_1 && action == GLFW_PRESS)
	{
		gl_RendererType = R_HW;
		Con_Print("GL renderer");
	}
	
	if(key == GLFW_KEY_2 && action == GLFW_PRESS)
	{
		gl_RendererType = R_SW;
		Con_Print("Software renderer");
	}
	
	// Toggle fullscreen
	if((key == GLFW_KEY_ENTER && action == GLFW_PRESS && mod == GLFW_MOD_ALT)
	   || (key == GLFW_KEY_F11 && action == GLFW_PRESS)
	   || (key == GLFW_KEY_F && action == GLFW_PRESS))
	{
		window_mode Mode;
		if(gl_WindowIsFullscreen)
		{
			Mode = WINDOW_MODE_WINDOWED;
		}
		else
		{
			Mode = gl_FullscreenWindowIsBorderless ? WINDOW_MODE_FULLSCREEN_BORDERLESS : WINDOW_MODE_FULLSCREEN;
		}
		
		gl_SetWindowMode(Mode);
	}
	
	// Clear platform profiler (platform_profiler.c)
	if(key == GLFW_KEY_R && action == GLFW_PRESS)
	{
		platform_TimedBlockClearAll();
	}
	if(key == GLFW_KEY_W && action == GLFW_PRESS)
	{
		platform_TimedBlockList();
	}
	
	if(key == GLFW_KEY_V && action == GLFW_PRESS)
	{
		gl_ToggleWireframe();
	}
	
	if(key == GLFW_KEY_3 && action == GLFW_PRESS)
	{
		gl_SetFullscreenMode(WINDOW_MODE_FULLSCREEN_BORDERLESS);
		if(gl_WindowIsFullscreen)
		{
			gl_SetWindowMode(WINDOW_MODE_FULLSCREEN_BORDERLESS);
		}
		Con_Print("Fullscreen mode Borderless");
	}
	if(key == GLFW_KEY_4 && action == GLFW_PRESS)
	{
		gl_SetFullscreenMode(WINDOW_MODE_FULLSCREEN);
		if(gl_WindowIsFullscreen)
		{
			gl_SetWindowMode(WINDOW_MODE_FULLSCREEN);
		}
		Con_Print("Fullscreen mode Full");
	}
	
	// Provide input data needed for update_game
	switch(action)
	{
		case GLFW_PRESS:
		{
			switch(key)
			{
				GLKD(GLFW_KEY_LEFT, BUTTON_LEFT);
				GLKD(GLFW_KEY_RIGHT, BUTTON_RIGHT);
				GLKD(GLFW_KEY_UP, BUTTON_UP);
				GLKD(GLFW_KEY_DOWN, BUTTON_DOWN);
				GLKD(GLFW_KEY_ESCAPE, BUTTON_ESC);
				GLKD(GLFW_KEY_L, BUTTON_CFG);
			}
		}
		break;
		case GLFW_RELEASE:
		{
			switch(key)
			{
				GLKU(GLFW_KEY_LEFT, BUTTON_LEFT);
				GLKU(GLFW_KEY_RIGHT, BUTTON_RIGHT);
				GLKU(GLFW_KEY_UP, BUTTON_UP);
				GLKU(GLFW_KEY_DOWN, BUTTON_DOWN);
				GLKU(GLFW_KEY_ESCAPE, BUTTON_ESC);
				GLKU(GLFW_KEY_L, BUTTON_CFG);
			}
		}
		break;
	}
}

void gl_CbResize(GLFWwindow *Window, int x, int y)
{
	glViewport(0, 0, x, y);
	gl_Screen.width = x;
	gl_Screen.height = y;
	if(!gl_WindowIsFullscreen)
	{
		gl_Screen.windowedWidth = x;
		gl_Screen.windowedHeight = y;
	}
	
	// This is probably wasteful to call on each resize, but I cannot find a way to detect window resolution changes in glfw without
	// calling the native platform api
	
	const GLFWvidmode *TempVidMode;
	TempVidMode = glfwGetVideoMode(gl_Monitor);
	gl_TargetRefreshSeconds = 1.0 / TempVidMode->refreshRate;
}

void gl_CbMouseButton(GLFWwindow *Window, int button, int action, int mod)
{
	if(button == GLFW_MOUSE_BUTTON_LEFT)
	{
		if(action == GLFW_PRESS)
		{
			gl_Input.buttons[(BUTTON_LMB)].is_down = true;
			gl_Input.buttons[(BUTTON_LMB)].changed = true;
		}
		else if (action == GLFW_RELEASE)
		{
			gl_Input.buttons[(BUTTON_LMB)].is_down = false;
			gl_Input.buttons[(BUTTON_LMB)].changed = true;
		}
	}
}

void gl_PauseGraphicDraw()
{
	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_TEXTURE_2D);
	glPushMatrix();
	glLoadIdentity();
	glBegin(GL_QUADS);
	double half = 0;
	double w = 0.025;
	double h = 0.1;
	double w2 = w * 2;
	
	// Left side
	// Top left
	glColor3f(1,1,1);
	glVertex2f(half - w2, half - h);
	
	// Top right
	glColor3f(0,1,1);
	glVertex2f(half - w, half - h);
	
	// Bottom right
	glColor3f(0,0,1);
	glVertex2f(half - w, half + h);
	
	// Bottom left
	glVertex2f(half - w2, half + h);
	
	
	// Right side
	// Top left
	glColor3f(1,1,1);
	glVertex2f(half + w2, half - h);
	
	// Top right
	glColor3f(0,1,1);
	glVertex2f(half + w, half - h);
	
	// Bottom right
	glColor3f(0,0,1);
	glVertex2f(half + w, half + h);
	
	// Bottom left
	glVertex2f(half + w2, half + h);
	
	glEnd();
	glPopMatrix();
	glPopAttrib();
}

void gl_CursorEnable(GLFWwindow *Window)
{
	glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void gl_CursorDisable(GLFWwindow *Window)
{
	glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void gl_FramebufferInit(Render_Buffer *Buffer, u32 width, u32 height, u32 bpp)
{
	Assert(sizeof *Buffer->pixels == PLATFORM_BYTES_PER_PIXEL);
	Assert(bpp == PLATFORM_BYTES_PER_PIXEL);
	Buffer->pixels = NULL;
	Buffer->pixels = malloc(width * height * bpp);
	Assert(Buffer->pixels != NULL);
	Buffer->width = width;
	Buffer->height = height;
	Pretty("gl_FramebufferInit: Initialized framebuffer -- %ux%u, %u BPP\n", width, height, PLATFORM_BYTES_PER_PIXEL);
}

void gl_CbWinClose(GLFWwindow *Window)
{
	gl_TimeToQuit = true;
}

gl_Texture gl_Texture2DCreate(u32 width, u32 height, u32 bpp, void *data)
{
	gl_Texture Texture;
	
	GLuint id;
	
	glEnable(GL_TEXTURE_2D);
	
	glGenTextures(1, &id);
	
	glBindTexture(GL_TEXTURE_2D, id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, PLATFORM_PREFERRED_PIXEL_LAYOUT, PLATFORM_PREFERRED_PIXEL_TYPE, data);
	
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	Texture.id = id;
	Texture.width = width;
	Texture.height = height;
	Texture.data = data;
	Texture.bpp = bpp;
	Texture.size = width * height * bpp;
	
	return Texture;
}

void gl_Texture2DDrawFullscreen(GLuint id)
{
	glBindTexture(GL_TEXTURE_2D, id);
	
	glColor4f(1, 1, 1, 1);
	
	glBegin(GL_QUADS);
	// Tex coord is mapped from 0, 1. X=0 is the left of the texture, Y=1 is the top of the texture,
	// Vertex is mapped from -1, 1. X=-1 is the left of the screen, Y=-1 is the bottom of the screen
	
	glTexCoord2f(0 ,0);
	glVertex2f(-1, -1);
	
	glTexCoord2f(0, 1);
	glVertex2f(-1, 1);
	
	glTexCoord2f(1, 1);
	glVertex2f(1, 1);
	
	glTexCoord2f(1, 0);
	glVertex2f(1, -1);
	
	glEnd();
}

void gl_UpdateAndRender(Render_Buffer *Buffer, gl_Texture *Texture, f32 dt)
{
	begin_profiler();
	
	// Core update function provided by the game
	/**********************/
	platform_TimedBlockBegin(TB_GAME);
	update_game(&gl_Input, dt);
	platform_TimedBlockEnd(TB_GAME);
	snd_Update();
	/**********************/
	
	render_profiler(dt);
	
	if(gl_RendererType == R_SW)
	{
		platform_TimedBlockBegin(TB_MEMCPY);
		
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Texture->width, Texture->height, 0, 
					 PLATFORM_PREFERRED_PIXEL_LAYOUT, PLATFORM_PREFERRED_PIXEL_TYPE, Buffer->pixels);
		
		platform_TimedBlockEnd(TB_MEMCPY);
		
		gl_Texture2DDrawFullscreen(Texture->id);
	}
}

void gl_CallbackInit()
{
	glfwSetFramebufferSizeCallback(gl_Window, gl_CbResize);
	glfwSetKeyCallback(gl_Window, gl_CbKeys);
	glfwSetMouseButtonCallback(gl_Window, gl_CbMouseButton);
	glfwSetWindowCloseCallback(gl_Window, gl_CbWinClose);
}

void gl_PollKeys(GLFWwindow *Win, Input *input)
{
	if(glfwGetKey(Win, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		input->buttons[(BUTTON_DOWN)].is_down = true;
		input->buttons[(BUTTON_DOWN)].changed = true;
	}
}

void gl_PrintDiagnostics()
{
	printf("=====================================================================\n");
	// These only return valid values after calling glfwMakeContextCurrent
	gl_PrintEnumString(GL_VENDOR);
	gl_PrintEnumString(GL_RENDERER);
	gl_PrintEnumString(GL_VERSION);
	printf("Fullscreen: %s\n", (gl_WindowIsFullscreen) ? "Yes" : "No");
	printf("=====================================================================\n");
}

void gl_ToggleWireframe()
{
	static b32 wireframeEnabled = 0;
	
	if(gl_Paused) return;
	
	wireframeEnabled = !wireframeEnabled;
	
	if(wireframeEnabled)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

void gl_RendererInit()
{
	// From r_common.c
	// Forces recalculation of aspect multiplier
	buffer_was_changed = 1;
	
	clear_profiler();
	
	if(gl_RendererType == R_HW)
	{
		glDisable(GL_TEXTURE_2D);
		// Note to self: Disable this when using glVertex... to draw primitives. Fun debugging this one
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		glLoadIdentity();
		glOrtho(0, FB_WIDTH_HW, 0, FB_HEIGHT_HW, 0, 1);
		
		R_EnableHardwareRenderer();
		
		render_buffer.width = FB_WIDTH_HW;
		render_buffer.height = FB_HEIGHT_HW;
	}
	else
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, gl_SoftwareBuffer.id);
		
		glDisable(GL_BLEND);
		
		glLoadIdentity();
		
		R_EnableSoftwareRenderer();
		
		render_buffer.width = FB_WIDTH_SW;
		render_buffer.height = FB_HEIGHT_SW;
		
		//clear_screen(0x00000000);
		glClearColor(0, 0, 0, 1);
	}
}

void gl_ChangeRendererIfNecessary()
{
	static int FirstCall = 1;
	static renderer_type LastRenderer;
	
	if(FirstCall)
	{
		LastRenderer = gl_RendererType;
		FirstCall = 0;
	}
	else
	{
		if(LastRenderer != gl_RendererType)
		{
			gl_RendererInit();
		}
		LastRenderer = gl_RendererType;
	}
}

u32 platform_allocate_texture(u32 width, u32 height, void *memory)
{
	u32 textureId;
	glEnable(GL_TEXTURE_2D);
	
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
				 PLATFORM_PREFERRED_PIXEL_LAYOUT, PLATFORM_PREFERRED_PIXEL_TYPE, memory);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	
	return textureId;
}

void gl_SetWindowMode(window_mode WindowMode)
{
	switch(WindowMode)
	{
		case WINDOW_MODE_WINDOWED:
		{
			glfwSetWindowAttrib(gl_Window, GLFW_DECORATED, GLFW_TRUE);
			glfwSetWindowMonitor(gl_Window, NULL, gl_Screen.windowedX, gl_Screen.windowedY, gl_Screen.windowedWidth, gl_Screen.windowedHeight, GLFW_DONT_CARE);
			gl_WindowIsFullscreen = false;
		}
		break;
		case WINDOW_MODE_FULLSCREEN_BORDERLESS:
		case WINDOW_MODE_FULLSCREEN:
		{
			if(!gl_WindowIsFullscreen)
			{
				glfwGetWindowPos(gl_Window, &gl_Screen.windowedX, &gl_Screen.windowedY);
			}
			gl_WindowIsFullscreen = true;
			GLFWmonitor *Monitor = gl_Monitor;//glfwGetPrimaryMonitor();
			gl_Vidmode = glfwGetVideoMode(Monitor);
			if(WindowMode == WINDOW_MODE_FULLSCREEN_BORDERLESS)
			{
				// @Hack: We offset the topleft corner by -1 so that the Windows compositor is fooled into using DwmFlush for vsync
				// On other operating systems this is inconsequential
				glfwSetWindowAttrib(gl_Window, GLFW_DECORATED, GLFW_FALSE);
#ifdef _WIN32
				glfwSetWindowMonitor(gl_Window, 0, -1, -1, gl_Vidmode->width + 1, gl_Vidmode->height + 1, GLFW_DONT_CARE);
#else
				glfwSetWindowMonitor(gl_Window, 0, 0, 0, gl_Vidmode->width + 0, gl_Vidmode->height + 0, GLFW_DONT_CARE);
#endif
			}
			else
			{
				glfwSetWindowAttrib(gl_Window, GLFW_DECORATED, GLFW_TRUE);
				glfwSetWindowMonitor(gl_Window, Monitor, 0, 0, gl_Vidmode->width, gl_Vidmode->height, GLFW_DONT_CARE);
			}
			
		}
		break;
		default:
		Assert(0 && "Invalid Window Mode!");
	}
}

void gl_SetFullscreenMode(window_mode WindowMode)
{
	switch(WindowMode)
	{
		case WINDOW_MODE_FULLSCREEN: gl_FullscreenWindowIsBorderless = false; break;
		case WINDOW_MODE_FULLSCREEN_BORDERLESS: gl_FullscreenWindowIsBorderless = true; break;
		default: break;
	}
}
