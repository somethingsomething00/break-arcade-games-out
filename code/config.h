// X11
//------------------------------------
// X11 shared memory extension.
// It's on by default because it provides a smoother gameplay experience.
// Define as 0 to disable.
#ifndef X_USE_SHM
#define X_USE_SHM 1
#endif

// Use GLX for vertical sync
// Since X11 doesn't have a standard way to query for vertical retrace on its own,
// we need to use GLX for this.
// By default, we just call the kernel sleep to simulate a vblank,
// but this will occasionally cause tearing because we do not always line up with the hardware timings.
// Enabling GLX will get rid of tearing.
// Define as 1 to enable.
#ifndef X_USE_GLX_FOR_VSYNC
#define X_USE_GLX_FOR_VSYNC 1
#endif


// SDL
//------------------------------------
// Disable runtime error checking for calls made by SDL.
// By default, we check every function call made by SDL for errors.
// On error, the program crashes and provides a diagnostic message.
// Define as 1 to disable.
#ifndef SDL_NOCHECK
#define SDL_NOCHECK 0
#endif


// Linux / Windows
//------------------------------------
// Use to profile the duration of sections of code.
// At the moment, linux and windows are supported.
// See platform_profiler.c for more details.
// Define as 1 to enable
#ifndef PLATFORM_PROFILER
#define PLATFORM_PROFILER 0
#endif


// Game
//------------------------------------
// Use the in-game profiler.
// If porting this code, you must call render_profiler() in your code to actually draw it.
// See profiler.c for details.
// Define as 1 to enable.
#ifndef PROFILER
#define PROFILER 0
#endif

// Enable `developer` mode.
// Up for invincibility.
// Down to destroy blocks.
// Right to advance to the next level.
// Define as 1 to enable.
#ifndef DEVELOPMENT
#define DEVELOPMENT 0
#endif


// Audio (SDL and OpenAL only)
//------------------------------------
// The X11 and OpenGL versions use OpenAL as the audio backend.
// The SDL2 version uses SDL2 Mixer.
// The backends are automatically determined in common_audio.c.
// The Windows GDI version uses DirectSound, which is always enabled.

// If you do not have the audio library.
// for the version you are compiling for, set the flag to 0.
// The program will still compile, but without audio support.
// See common_audio.c and common_audio_dummy.h.
// Define as 1 to enable audio for all versions.
#ifndef AUDIO_ENABLED
#define AUDIO_ENABLED 1
#endif


// Some OpenGL implementations on Windows cause glfwSwapBuffers to use 100% of the cpu during vsync.
// We solve this issue by using Windows' native DwmFlush to perform the vsync.
// On by default, but not necessary if you see that you have no issues with cpu usage.
// Try both methods and see how you fare.
// See win32_gl_platform.c
// Define as 1 to enable.
#ifndef GLFW_WIN32_USE_DWM_FOR_VSYNC
#define GLFW_WIN32_USE_DWM_FOR_VSYNC 1
#endif


// Intrinsics
//------------------------------------
// Use x86 processor intrinsics for some routines.
// At the moment, the only thing we do is a fast memset using rep stosd.
// See common_intrinsics.c for supported routines.
// See clear_screen in r_software.c
// Define as 1 to enable.
#ifndef PLATFORM_USE_INTRINSICS
#define PLATFORM_USE_INTRINSICS 1
#endif


// SIMD
//------------------------------------
// Use simd equivalents instead of the native x86 routines.
// See common_intrinsics.c.
// Define as 1 to enable.
#ifndef PLATFORM_USE_SIMD
#define PLATFORM_USE_SIMD 0
#endif
