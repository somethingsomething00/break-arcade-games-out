#ifndef _X_SHM_C_
#define _X_SHM_C_
void x_DoublebufferCreate(doublebuffer *DbReturn, Display *Dpy, XWindowAttributes *WindowAttributes, u32 width, u32 height)
{
	DbReturn->Front.SegmentInfo.shmid = shmget(IPC_PRIVATE, width * height * sizeof *DbReturn->Front.Fb.data, IPC_CREAT | 0777);
	DbReturn->Back.SegmentInfo.shmid = shmget(IPC_PRIVATE, width * height * sizeof *DbReturn->Back.Fb.data, IPC_CREAT | 0777);
	Assert(DbReturn->Front.SegmentInfo.shmid != -1);
	Assert(DbReturn->Back.SegmentInfo.shmid != -1);

	DbReturn->Front.Fb.data = shmat(DbReturn->Front.SegmentInfo.shmid, 0, 0);
	DbReturn->Back.Fb.data = shmat(DbReturn->Back.SegmentInfo.shmid, 0, 0);
	Assert(DbReturn->Front.Fb.data != (void *)-1);
	Assert(DbReturn->Back.Fb.data != (void *)-1);


	Assert(DbReturn->Front.Image = XShmCreateImage(Dpy, WindowAttributes->visual,
				WindowAttributes->depth, ZPixmap, NULL, &DbReturn->Front.SegmentInfo, width, height));
	DbReturn->Front.Image->data = (char *)DbReturn->Front.Fb.data;

	Assert(DbReturn->Back.Image = XShmCreateImage(Dpy, WindowAttributes->visual,
				WindowAttributes->depth, ZPixmap, NULL, &DbReturn->Back.SegmentInfo, width, height));
	DbReturn->Back.Image->data = (char *)DbReturn->Back.Fb.data;


	DbReturn->Front.SegmentInfo.shmaddr = (char *)DbReturn->Front.Fb.data;
	DbReturn->Back.SegmentInfo.shmaddr = (char *)DbReturn->Back.Fb.data;
	Assert(XShmAttach(Dpy, &DbReturn->Front.SegmentInfo));
	Assert(XShmAttach(Dpy, &DbReturn->Back.SegmentInfo));

	DbReturn->Front.Fb.width = width;
	DbReturn->Front.Fb.height = height;
	DbReturn->Front.Fb.subwidth = width;
	DbReturn->Front.Fb.subheight = height;

	DbReturn->Back.Fb.width = width;
	DbReturn->Back.Fb.height = height;
	DbReturn->Back.Fb.subwidth = width;
	DbReturn->Back.Fb.subheight = height;
}

void x_SwapBuffers(doublebuffer *Db)
{
	XImage *tmpImage;
	framebuffer tmpFb;

	// Note: The explicit copy is intentional.
	// Since the SegmentInfo members are stack allocated, their addresses must be preserved.
	// We don't need to swap the SegmentInfo members either.
	tmpImage = Db->Front.Image;
	Db->Front.Image = Db->Back.Image;
	Db->Back.Image = tmpImage;

	tmpFb = Db->Front.Fb;
	Db->Front.Fb = Db->Back.Fb;
	Db->Back.Fb = tmpFb;
}

void x_DoublebufferCleanup(doublebuffer *Db, Display *Dpy)
{
	XShmDetach(Dpy, &Db->Front.SegmentInfo);
	XShmDetach(Dpy, &Db->Back.SegmentInfo);
	XDestroyImage(Db->Front.Image);
	XDestroyImage(Db->Back.Image);
	shmctl(Db->Front.SegmentInfo.shmid, IPC_RMID, 0);
	shmctl(Db->Back.SegmentInfo.shmid, IPC_RMID, 0);
}

void x_DoublebufferResetOnResize(doublebuffer *Db)
{
	// @Hack
	// This is probably not as portable because we're using the struct directly.

	// The alternative was to free the XImage struct and recreate both buffers with XShmCreateImage
	// This was causing massive issues because the memory was still being transfered over during the
	// free / reallocation of the backbuffer image and the application was crashing
	// Modifying the width/height fields directly seems  to satisfy X server's requirements
	// but who knows if a bug creeps in along the way

	Db->Front.Image->width = Db->Front.Fb.subwidth;
	Db->Front.Image->height = Db->Front.Fb.subheight;
	Db->Front.Image->bytes_per_line = Db->Front.Fb.subwidth * sizeof *Db->Front.Fb.data;

	Db->Back.Image->width = Db->Back.Fb.subwidth;
	Db->Back.Image->height = Db->Back.Fb.subheight;
	Db->Back.Image->bytes_per_line = Db->Back.Fb.subwidth * sizeof *Db->Back.Fb.data;
}
#endif /* _X_SHM_C_ */
