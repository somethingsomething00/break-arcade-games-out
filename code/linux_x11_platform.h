#ifndef _LINUX_X11_PLATFORM_H_
#define _LINUX_X11_PLATFORM_H_

// Standard
//--------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


// Linux
//--------------------------------------------------------------------------------
#include <sys/mman.h>


// X11
//--------------------------------------------------------------------------------
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xrandr.h>
// XKBlib reserves the word internal
#undef internal
#include <X11/XKBlib.h>



/**********************************
* Platform types
**********************************/
typedef u32 framebuffer_t;
typedef u32 process_mask_t;

typedef struct
{
	u32 width;
	u32 height;
	u32 subwidth;
	u32 subheight;
	framebuffer_t *data;
} framebuffer;

typedef struct
{
	u32 width;
	u32 height;

	u32 widthWindowed;
	u32 heightWindowed;

	u32 xpos;
	u32 ypos;

	u32 xposWindowed;
	u32 yposWindowed;

	u32 widthNative;
	u32 heightNative;

	u32 fullscreenWidth;
	u32 fullscreenHeight;

	u32 borderWidthCur;
	u32 borderWidthOld;

	short refreshRate;
} screeninfo;

typedef struct
{
	int width;
	int height;

	int widthNative;
	int heightNative;
	// We won't always need these, but they're here anyway

	int refreshRateInHz;
} xrandr_screen_info;

typedef u32 samplecache_t;
typedef struct
{
	// Note: Feel free to change these to f64 types if needed
	// We don't need the precision so they are u32s for better performance
	samplecache_t *xs;
	samplecache_t *ys;
} samplecache;

typedef struct
{
	s32 xCur;
	s32 yCur;
	s32 xOld;
	s32 yOld;
	s32 yTotal;
	s32 xTotal;
	s32 dx;
	s32 dy;
} mouse;

typedef struct
{
	// The primary memory store
	u8 *Memory;

	// Pointers to the main memory store
	// Distributed by the program
	void *FramebufferGame;
	void *FramebufferPlatform;
	void *SampleCacheXs;
	void *SampleCacheYs;
	u64 totalSize;
	u64 used;
} game_memory;

enum
{
	PLATFORM_CURSOR_VISIBLE,
	PLATFORM_CURSOR_INVISIBLE,
	PLATFORM_CURSOR_TOTAL,
};

typedef struct
{
	b32 isDown;
	b32 isHeld;
} key;

#define PLATFORM_MAX_KEYCODES 255
// X11 (guarantees?) that the max value returned by XDisplayKeycodes is no more than 255
// This is also defined in X11/extensions/XKB.h as XKBXkbMaxLegalKeyCode 255
typedef struct
{
	key Key[PLATFORM_MAX_KEYCODES];
} keys;


#endif /* _LINUX_X11_PLATFORM_H_ */
