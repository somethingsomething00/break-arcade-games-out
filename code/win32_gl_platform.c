#ifndef _WIN32_GL_PLATFORM_C_
#define _WIN32_GL_PLATFORM_C_
#define WIN32_THIRDPARTY_SOUND 1
#include <windows.h>
#include <dwmapi.h>

#define GLFW_EXPOSE_NATIVE_WIN32
#include "config.h"
#include "common_gl_platform.h"
#include "win32_gl_platform.h"
#include <GLFW/glfw3native.h>

// Unfortunately we need this platform specific bit of code in here
// due to some OpenGL driver issues.
// In fullscreen windows, glfwSwapBuffers 'may' cause the CPU to spinlock, resulting in 100% cpu usage for that core.
// This is not the fault of glfw, but rather most likely a faulty driver implementation involving the SwapBuffers function.

// We circumvent this by:
// - Setting the context monitor to NULL.
// - Turning decorations off.
// - In the case where we want a borderless full screen:
//   - Resizing window to be 1 pixel larger than the monitor resolution in both directions,
//     and offsetting its top left position by -1 pixel in both directions.
// - Otherwise just detecting the fullscreen state
// - Calling DwmFlush and SwapBuffers ourselves.

// However, this means that Windows versions before Vista will not compile becasue dwm did not exist yet.
// (As far as I know!)
// At the time of writing, glfw3.dll calls DwmFlush for `windowed' mode as well, but we don't want to rely on library behaviour.
// Big thanks to the glfw3 source tree for some pointers on the implementation.

// In short:
// Fullscreen = SwapBuffers
// Fullscreen + Borderless =  DwmFlush / SwapBuffers
// Windowed = DwmFlush / SwapBuffers

// See config.h
#if GLFW_WIN32_USE_DWM_FOR_VSYNC

#define GLFW_CUSTOM_SWAP_BUFFERS_IMPLEMENTATION
HDC DeviceContext = NULL;
int SwapInterval = 0;

#define glfwSwapInterval(count) do{glfwSwapInterval((count)); SwapInterval = (count);}while(0)

void glfw_InitForOS(GLFWwindow *Window)
{
	HWND Wnd = glfwGetWin32Window(Window);
	DeviceContext = GetDC(Wnd);
}

extern b32 gl_FullscreenWindowIsBorderless;
extern b32 gl_WindowIsFullscreen;
void glfw_SwapBuffers(GLFWwindow *Window)
{
	if((gl_FullscreenWindowIsBorderless && gl_WindowIsFullscreen) || !gl_WindowIsFullscreen)
	{
		for(int i = 0; i < SwapInterval; i++)
		{
			DwmFlush();
		}
		SwapBuffers(DeviceContext);
	}
	else
	{
		SwapBuffers(DeviceContext);
	}
}

#endif /* GLFW_WIN32_USE_DWM_FOR_VSYNC */

#include "common_gl_platform.c"


#endif /* _WIN32_GL_PLATFORM_C_ */
