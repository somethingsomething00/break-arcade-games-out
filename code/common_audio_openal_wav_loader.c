// Reference:
// http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html

// Note: Even though this file name refers OpenAL,
// it really is a generic WAV loader.
// The only library specific things are the OpenAL types.
// It can be repurposed to work for other APIs.

#pragma pack(push, 1)

typedef struct
{
	u32 chunk_id;
	u32 chunk_size;
	u32 wave_id;
} wav_riff_chunk;

typedef struct
{
	u32 chunk_id;
	u32 chunk_size;
	u16 format_tag;
	u16 num_channels;
	u32 samples_per_sec;
	u8 _ignore[6];
	u16 bits_per_sample;
} wav_fmt_chunk;

typedef struct
{
	u32 chunk_id;
	u32 chunk_size;
} wav_data_chunk;

typedef struct
{
	wav_riff_chunk riff;
	wav_fmt_chunk fmt;
} wav_header;

#pragma pack(pop)

int ValidateChunkId(const u32 want, const u32 have)
{
	return !(have == want);
}

#define snd_PackRiffIdLE(a, b, c, d) ((a) | ((b) << 8) | ((c) << 16) | ((d) << 24))
#define snd_PackRiffId(a, b, c, d) (snd_PackRiffIdLE(a, b, c, d))

// #define snd_PackRiffIdBE(a, b, c, d) ((a << 24) | (b << 16) | (c << 8) | (d << 0))

// Intended as a drop-in replacement for alutLoadMemoryFromFileImage
// Note: We don't parse all header formats yet.
void *snd_LoadMemoryFromFileImage(const void *memory, size_t length, ALenum *format, ALsizei *size, f32 *frequency)
{
	wav_header WavHeader;
	const u8 *pMemory = memory;
	u8 *pHeader = (u8 *)&WavHeader;
	void *result = NULL;

#ifndef WAVE_FORMAT_PCM
	const u32 WAVE_FORMAT_PCM = 0x1;
#endif

	snd_Assert(length > sizeof WavHeader);

	for(size_t i = 0; i < sizeof WavHeader; i++)
	{
		pHeader[i] = pMemory[i];
	}

	const u32 RIFF = snd_PackRiffId('R', 'I', 'F', 'F');
	const u32 WAVE = snd_PackRiffId('W', 'A', 'V', 'E');
	const u32 FMT = snd_PackRiffId('f', 'm', 't', ' ');
	const u32 DATA = snd_PackRiffId('d', 'a', 't', 'a');

	// Make sure these constants are present
	int chunkstate = 0;
	chunkstate |= ValidateChunkId(RIFF, WavHeader.riff.chunk_id);
	chunkstate |= ValidateChunkId(WAVE, WavHeader.riff.wave_id);
	chunkstate |= ValidateChunkId(FMT, WavHeader.fmt.chunk_id);

	snd_Assert(chunkstate == 0 && "Invalid WAVE header!");

	// We are assuming that the RIFF header and fmt header are consecutive in memory!

	// Note: We don't support other formats yet
	snd_Assert(WavHeader.fmt.format_tag == WAVE_FORMAT_PCM);
	snd_Assert(WavHeader.fmt.bits_per_sample == 16);

	if(WavHeader.fmt.num_channels == 2)
	{
		*format = AL_FORMAT_STEREO16;
	}
	else if(WavHeader.fmt.num_channels == 1)
	{
		*format = AL_FORMAT_MONO16;
	}
	else
	{
		snd_Assert(!"Unsupported WAV format");
	}

	// The data chunk is not guaranteed to follow the fmt chunk
	// After we get the fmt chunk, we have to parse the rest of the memory for the
	// "data" magic number

	size_t after_fmt_chunk =
		8 // skip chunk_id and size
		+ WavHeader.fmt.chunk_size
		+  ((char *)&WavHeader.fmt - (char *)&WavHeader);

	u8 *p = (u8 *)(memory) + after_fmt_chunk;
	size_t c = after_fmt_chunk;
	int data_chunk_present = 0;

	// We used to assume 4 byte alignment to detect the data chunk, but now we step through each byte
	while(c < length)
	{
		u32 id = *(u32 *)p;
		if(ValidateChunkId(DATA, id) == 0)
		{
			data_chunk_present = 1;
			break;
		}
		p++;
		c += 1;
	}

	if(!data_chunk_present)
	{
		snd_Log("Data chunk for WAV file is corrputed!\n");
		snd_Log("Here are the known details for this file\n");

		snd_Log("Size in memory(bytes): %zu\n", length);
		snd_Log("Frequency:             %d\n", WavHeader.fmt.samples_per_sec);
		snd_Log("Bits per sample:       %d\n", WavHeader.fmt.bits_per_sample);
		snd_Log("Channels:              %d\n", WavHeader.fmt.num_channels);

		snd_Assert(0);
	}

	wav_data_chunk *DataChunk = (wav_data_chunk *)p;
	const u8 *copy_from = p + sizeof *DataChunk;
	u8 padding_byte = (DataChunk->chunk_size & 1);
	u32 data_size = DataChunk->chunk_size + padding_byte;

	result = malloc(data_size);
	if(result)
	{
		memcpy(result, copy_from, data_size);
	}

	*size = data_size;
	*frequency = (f32)WavHeader.fmt.samples_per_sec;

	return result;
}
