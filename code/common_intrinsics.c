// NAME: common_intrinsics.c
// TIME OF CREATION: 2022-04-25 17:54:09
// AUTHOR:
// DESCRIPTION: This file includes some optimized SIMD routine equivalents to the regular C function versions found in the game
// 				Where builtin processor instructions are faster, those are used in preference to the SIMD versions
// 				You can define PLATFORM_USE_SIMD=1 to prefer the SIMD variants

// The naming convetion is:
// intrin_<original_function_name>

// Note: These intrinsics have been tested on both Linux and Windows.
// As of writing, all functions work as intended (2022-05).

#ifdef _WIN32
#include <windows.h>
#include <intrin.h>
#else
// Reduce compile times if we don't need this header
 #if PLATFORM_USE_SIMD
  #include <immintrin.h>
 #endif 
#endif /* _WIN32 */

#ifdef __always_inline
#define LOCAL_INLINE __always_inline
#else
#define LOCAL_INLINE static inline
#endif

// On x86_64 CPUs with the ERMS cpuid bit, this is at least twice as fast as an sse2 memset implementation
// ERMS: Enhanced Rep Movsb (also applied to stos)
// Copies word to ptr, (bytes/4) times.

#ifdef _WIN32

// Windows conveniently provides us with a compiler intrinsic
LOCAL_INLINE void stos32_impl(void *ptr, u32 word, size_t bytes)
{
	__stosd(ptr, word, bytes / 4);
}

#else

// Inline assembly compatible with GNU assembler (as)
LOCAL_INLINE void stos32_impl(void *ptr, u32 word, size_t bytes)
{
    // Ripped straight out of clang's intrin.h
	bytes /= 4;
    __asm__ __volatile__("cld\n"
                         "rep stosl\n"
                         : "+D"(ptr), "+c"(bytes)
                         : "a"(word)
                         : "memory");
}

#endif /* _WIN32 */

LOCAL_INLINE void intrin_clear_screen(Render_Buffer *Buffer, const u32 color)
{
#if !PLATFORM_USE_SIMD
	stos32_impl(Buffer->pixels, color, Buffer->width * Buffer->height * 4);
#else

	// Assumes a framebuffer with dimensions divisible by 4
	const __m128i color128 = _mm_set1_epi32(color);
	__m128i *pixels128 = (__m128i *)Buffer->pixels;
	const u32 pixelCount = Buffer->width * Buffer->height;
	const u32 iterations = pixelCount / 4;

	for(u32 i = 0; i < iterations; i += 4)
	{
		_mm_store_si128(&pixels128[0], color128);
		_mm_store_si128(&pixels128[1], color128);
		_mm_store_si128(&pixels128[2], color128);
		_mm_store_si128(&pixels128[3], color128);

		pixels128 += 4;
	}
#endif
}
