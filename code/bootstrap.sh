#!/bin/sh
# To customize the build file, see build_linux.c
# After making your changes, run this again

cc=gcc
echo "Compiling build.c"
"$cc" build.c -o build -lpthread && echo "build binary is now compiled. Run ./build to compile the project!" || echo "Compilation Error!"

