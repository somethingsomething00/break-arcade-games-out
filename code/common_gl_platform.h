#ifndef _COMMON_GL_PLATFORM_H_
#define _COMMON_GL_PLATFORM_H_

// Standard
//--------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


// GL
//--------------------------------------------------------------------------------
#include <GL/gl.h>
#include <GLFW/glfw3.h>


// Local
//--------------------------------------------------------------------------------
#include "types.h"


/**********************************
* Platform types
**********************************/
typedef struct
{
	u32 width;
	u32 height;
	
	s32 windowedWidth;
	s32 windowedHeight;
	s32 windowedX;
	s32 windowedY;
} gl_Screen_t;

typedef struct
{
	u32 *data;
	GLuint id;
	u32 width;
	u32 height;
	u32 bpp;
	u32 size;
} gl_Texture;

typedef enum
{
	WINDOW_MODE_WINDOWED,
	WINDOW_MODE_FULLSCREEN_BORDERLESS,
	WINDOW_MODE_FULLSCREEN
} window_mode;

#endif /* _COMMON_GL_PLATFORM_H_ */
