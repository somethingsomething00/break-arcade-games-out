// Note: This code is now shared between Linux and Windows builds.

// NAME: common_audio.c
// TIME OF CREATION: 2022-04-20 20:49:24
// AUTHOR:
// DESCRIPTION: General audio interface
// 				At the moment, abstracts OpenAL or SDL_Mixer

// Unused
// For compatability with the main game
// Always included
int next_playing_sound = 0;
int immutable_sound_count = 0;
f32 *player_visual_p_x_ptr = NULL;

#if AUDIO_ENABLED
#include <stdio.h>
#include <stdlib.h>


#define snd_Log(...) \
do { printf("%s:%d: ", __FILE__, __LINE__); printf(__VA_ARGS__); fflush(stdout); } while(0)

#define snd_Print(...) \
do { printf(__VA_ARGS__); fflush(stdout); } while(0)

#define snd_Assert(cond) \
do { if(!(cond)) {fprintf(stderr, "%s:%d: snd_Assert: Assertion failed for expression '%s'\n", __FILE__, __LINE__, #cond); exit(1);} } while(0)

char *snd_HumanReadableSoundId(int id);


// @Cleanup: Is there a foolproof way to detect SDL_Mixer?
#if USE_SDL_SOUND
	#include "common_audio_sdl2.c"
#else


#ifdef _WIN32
	#include "win32_audio_openal_v2.c"
#else
	#include "linux_audio_openal_v2.c"

#endif /* _WIN32 */
#endif /* USE_SDL_SOUND */

#include "common_audio.h"


/**********************************
* Common
**********************************/
#define hrsi(i_) case (i_): return #i_
char *
snd_HumanReadableSoundId(int id)
{
	switch(id)
	{
		hrsi(S_MENU_MUSIC);
		hrsi(S_MAIN_MUSIC);
		hrsi(S_HIT_1);
		hrsi(S_HIT_2);
		hrsi(S_HIT_3);
		hrsi(S_HIT_4);
		hrsi(S_HIT_5);
		hrsi(S_HIT_6);
		hrsi(S_HIT_7);
		hrsi(S_HIT_8);
		hrsi(S_HIT_9);
		hrsi(S_HIT_10);
		hrsi(S_HIT_11);
		hrsi(S_HIT_12);
		hrsi(S_HIT_13);
		hrsi(S_HIT_14);
		hrsi(S_HIT_15);
		hrsi(S_HIT_16);
		hrsi(S_GAME_OVER);
		hrsi(S_FORCEFIELD);
		hrsi(S_FIREWORKS);
		hrsi(S_SPRING);
		hrsi(S_START_GAME);
		hrsi(S_LOSE_LIFE);
		hrsi(S_REDIRECT);
		hrsi(S_BALL);
		hrsi(S_COMET_BEGIN);
		hrsi(S_COMET_LOOP);
		hrsi(S_OLD_SOUND);
		hrsi(S_POWERUP);
		hrsi(S_POWERDOWN);
		hrsi(S_INTERFACE);
		hrsi(S_PLAYER_WALL);
		hrsi(S_LOAD_GAME);
		hrsi(S_BRICK_1);
		hrsi(S_BRICK_2);
		hrsi(S_BRICK_3);
		hrsi(S_BRICK_4);
		hrsi(S_BRICK_5);
		hrsi(S_SINE);
		default:
		snd_Log("Unhandled sound id '%d' in snd_HumanReadableSoundId\n", id);
		snd_Assert(0);
	}
	return NULL;
}
#else

#include "common_audio_dummy.h"

#endif /* AUDIO_ENABLED */
