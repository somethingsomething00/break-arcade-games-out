#ifndef _BUILD_H_
#define _BUILD_H_
#include <stdio.h>
#include <fcntl.h>
#include <stdbool.h>
#include <assert.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

#if defined (_WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <process.h>
#include <direct.h>

#else

#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <libgen.h> // For basename
#endif /* _WIN32 */

#if defined (_WIN32)
#define PLATFORM_PATH_MAX (MAX_PATH)
#define PLATFORM_PATH_SEP "\\"
#else
#include <limits.h>
#define PLATFORM_PATH_MAX (PATH_MAX)
#define PLATFORM_PATH_SEP "/"
#endif /* _WIN32 */


#define PLATFORM_PATH_SEP_LEN ((sizeof (PLATFORM_PATH_SEP) / sizeof (PLATFORM_PATH_SEP[0])) - 1)

#define array_count(a_) (sizeof(a_) / sizeof(a_)[0])

typedef enum
{
	CPY_CLOBBER = 0x1,
	CPY_NO_CLOBBER = 0x2,
	CPY_MAKE_EXECUTABLE = 0x4,
} copy_flag_t;

typedef enum
{
	FC_CLOBBER = 0x1,
	FC_NO_CLOBBER = 0x2,
} file_create_t;


#define PUSHD_MAX 16
typedef struct
{
	char dirs[PUSHD_MAX][PLATFORM_PATH_MAX];
	int count;
} dir_stack_t;
dir_stack_t dir_stack = {0};

#define CMDS_MAX 64
// How many user arguments do we accept for each target?
typedef struct
{
	int argc;
	const char *argv[CMDS_MAX];
} command_buffer;



// This function prototype is for setting up your compiler arguments
#define BUILD_FUNC(name) void name(command_buffer *buffer, const char *target_name)
typedef BUILD_FUNC((buildfunc));

// This function prototype is for running post-build operations
#define POST_FUNC(name) void name(const char *target_name)
typedef POST_FUNC((post_build_func));

typedef struct
{
	const char *name;
	buildfunc *build_func;
	post_build_func *post_func;
	command_buffer commands;
} target;

#define TARGETS_MAX 8
target targets[TARGETS_MAX];
unsigned int target_count = 0;


int opt_verbose = 1;
// Toggle verbose printing of messages
// Controls behaviour of printv

int opt_commands_show = 1;
// Display active compiler switches during compilation
// In general, display all arguments passed to the target
// when the build is run.
// Controls behaviour of commands_show



void printv(const char *fmt, ...)
{
	if(opt_verbose)
	{
		va_list vl;
		va_start(vl, fmt);
		vfprintf(stdout, fmt, vl);
		va_end(vl);
		fflush(stdout);
	}
}

// Error handler for functions returning an int
// By libc convention, negative returns are errors
int libc_calli(int ret, const char *func, const char *info)
{
	if(ret < 0)
	{
		int ec = errno;
		if(ec)
		{
			const char *errstring = strerror(ec);
			fprintf(stderr, "%s: %s: %s\n", func, errstring, info);
			exit(1);
		}
	}
	return ret;
}

// Error handler for functions returning a pointer
// By libc convention, NULL pointers are errors
void *libc_callv(void *mem, const char *func, const char *info)
{
	if(!mem)
	{
		int ec = errno;
		const char *errstring = strerror(ec);
		fprintf(stderr, "%s: %s: %s\n", func, errstring, info);
		exit(1);
	}
	return mem;
}

// Just for clang-cl.
// It doesn't understand the flag -Fe: <filename> (note the space) to specify the output file
// So we have to fix it to -Fe:<filename>
char *concat2(const char *s1, const char *s2)
{
	char *result;
	int len1, len2;
	len1 = strlen(s1);
	len2 = strlen(s2);
	int len_total = len1 + len2;
	result = libc_callv(malloc(len_total + 1), "malloc", "malloc");
	memcpy(result, s1, len1);
	memcpy(&result[len1], s2, len2);
	result[len_total] = 0;
	return result;
}

void pushd(const char *dir)
{
	assert(dir_stack.count < PUSHD_MAX);
	getcwd(dir_stack.dirs[dir_stack.count], PLATFORM_PATH_MAX);
	dir_stack.count++;
	libc_calli(chdir(dir), "chdir", dir);
}
// @user
void popd()
{
	if(dir_stack.count--)
	{
		char *dir = dir_stack.dirs[dir_stack.count];
		libc_calli(chdir(dir), "chdir", dir);
	}
}

#if defined (_WIN32)
int impl_mkdir(const char *dir)
{
	return mkdir(dir);
}
#else
int impl_mkdir(const char *dir)
{
	return mkdir(dir,
				 S_IRUSR 	| S_IWUSR 	| S_IXUSR
				 | S_IRGRP 				| S_IXGRP
				 | S_IROTH 				| S_IXOTH);
}
#endif

#define mkdirs(...) impl_mkdirs(__VA_ARGS__, NULL)
void impl_mkdirs(const char *s, ...)
{
	va_list vl;
	char path[PLATFORM_PATH_MAX];
	char *dir;
	size_t bytes_copied = 0;

	dir = (char *)s;
	va_start(vl, s);

	while(dir && bytes_copied < PLATFORM_PATH_MAX)
	{
		int len = strlen(dir);
		memcpy(&path[bytes_copied], dir, len);
		bytes_copied += len;
		memcpy(&path[bytes_copied], PLATFORM_PATH_SEP, PLATFORM_PATH_SEP_LEN);
		bytes_copied += PLATFORM_PATH_SEP_LEN;
		path[bytes_copied] = 0;

		int rc = impl_mkdir(path);
		if(rc != 0)
		{
			int ec = errno;
			if(ec == EEXIST)
			{
				goto next_arg;
			}
			else
			{
				// Error message
				printv("Tried to make dir '%s' but failed\n", path);
				printv("%s\n", strerror(ec));
				exit(1);
			}
		}
		next_arg:
		dir = va_arg(vl, char *);
	}
	va_end(vl);
	assert(bytes_copied > 0);
}



#if defined (_WIN32)
#define S_IFREG _S_IFREG
#define S_IFDIR _S_IFDIR
#endif /* _WIN32 */
bool file_exists(const char *path, bool *is_dir)
{
	if(is_dir) *is_dir = false;
	bool result = false;
	struct stat fileinfo;
	int rc = stat(path, &fileinfo);
	if(rc == 0)
	{
		if(fileinfo.st_mode & (S_IFREG | S_IFDIR))
		{
			if(fileinfo.st_mode & S_IFDIR && is_dir) *is_dir = true;
			result = true;
		}
	}
	return result;
}
// todo: Undef?

// @user
size_t filesize_get(int fd, const char *path)
{
	struct stat fileinfo;
	libc_calli(fstat(fd, &fileinfo), "fstat", path);
	return fileinfo.st_size;
}

// @user
// Return just the filename, without the directory prefix
#if defined (_WIN32)
#undef strndup
char *strndup(char *s, size_t limit)
{
	char *result = NULL;
	int len = strlen(s);
	if(len)
	{
		int to_alloc = ((len < limit) ? len : limit);
		result = malloc(to_alloc + 1);
		memcpy(result, s, to_alloc);
		result[to_alloc] = 0;
	}
	return result;
}
char *filename_get(const char *filename)
{
	char *base_name = NULL;

	WIN32_FIND_DATA Data;
	HANDLE result = FindFirstFile(filename, &Data);
	if(result) base_name = strndup(Data.cFileName, PLATFORM_PATH_MAX);

	return base_name;
}
#else
char *filename_get(const char *filename)
{
	// From the linux manpage:
	// Both dirname() and basename() may modify the contents of path,
	// so it may be desirable to pass a copy when calling one of these functions.

	char *name_copy = strdup(filename);
	char *result = basename(name_copy);
	return result;
}
#endif

#if defined (_WIN32)
void file_make_executable(int fd)
{
	// Does not apply to Windows
	return;
}

void file_copy_permissions(int fd_from, int fd_to)
{
	return;
}

#else

void file_make_executable(int fd)
{
	struct stat fileinfo;
	libc_calli(fstat(fd, &fileinfo), "fstat", 0);
	libc_calli(fchmod(fd, fileinfo.st_mode | S_IXUSR | S_IXGRP | S_IXOTH), "fchmod", "setting executable bits");
}

void file_copy_permissions(int fd_from, int fd_to)
{
	struct stat fileinfo;
	libc_calli(fstat(fd_from, &fileinfo), "fstat", 0);
	libc_calli(fchmod(fd_to, fileinfo.st_mode), "fchmod", "copying file permissions");
}

#endif /* _WIN32 */

#define make_path(...) impl_make_path(__VA_ARGS__, NULL)
char *impl_make_path(const char *s, ...)
{
	char *result = libc_callv(malloc(PLATFORM_PATH_MAX), "malloc", "make_path");
	va_list vl;
	va_start(vl, s);
	size_t bytes_copied = 0;

	char *path = (char *)s;
	while(path && bytes_copied < PLATFORM_PATH_MAX)
	{
		int len = strlen(path);
		memcpy(&result[bytes_copied], path, len);
		bytes_copied += len;
		memcpy(&result[bytes_copied], PLATFORM_PATH_SEP, PLATFORM_PATH_SEP_LEN);
		bytes_copied += PLATFORM_PATH_SEP_LEN;
		result[bytes_copied] = 0;

		path = va_arg(vl, char *);
	}
	va_end(vl);

	// Remove trailing slash
	// The memset is overkill here but it allows for generalization
	if(bytes_copied > PLATFORM_PATH_SEP_LEN)
	{
		memset(&result[bytes_copied - PLATFORM_PATH_SEP_LEN], 0, PLATFORM_PATH_SEP_LEN);
	}
	return result;
}





void copy_file(const char *src, const char *dst, copy_flag_t copy_flag)
{
	FILE *fsrc= libc_callv(fopen(src, "rb"), "fopen", src);
	bool is_dir;
	const char *dstpath = dst;
	// const char *relative_dst_dir = dst;
	if(file_exists(dst, &is_dir))
	{
		if(is_dir)
		{
			const char *dir = dst;
			char *srcname = filename_get(src);
			dstpath = make_path(dst, srcname);

			// Check again since we appended directories
			if(copy_flag & CPY_NO_CLOBBER && file_exists(dstpath, NULL))
			{
				printv("copy_file: File exists: '%s' Skipping...\n", dstpath);
				goto end;
			}
		}
		else
		{
			printv("copy_file: File exists: '%s' Skipping...\n", dst);
			goto end;

		}
	}

	FILE *fdst = libc_callv(fopen(dstpath, "wb+"), "fopen", dstpath);
	size_t filesize = filesize_get(fileno(fsrc), src);
	char *srcbuffer = libc_callv(malloc(filesize), "malloc", "malloc");

	int bytes_read = fread(srcbuffer, 1, filesize, fsrc);
	assert(bytes_read == filesize);

	int bytes_written = fwrite(srcbuffer, 1, filesize, fdst);
	assert(bytes_written == filesize);
	free(srcbuffer);

	file_copy_permissions(fileno(fsrc), fileno(fdst));

	if(copy_flag & CPY_MAKE_EXECUTABLE)
	{
		file_make_executable(fileno(fdst));
	}

	printv("copy_file: %s -> %s\n", src, dstpath);

	fclose(fdst);
	end:
	fclose(fsrc);

}

void move_file(const char *src, const char *dst)
{
	const char *dstpath = dst;
	bool is_dir;
	if(file_exists(dst, &is_dir))
	{
		if(is_dir)
		{
			char *srcname = filename_get(src);
			dstpath = make_path(dst, srcname);
		}
	}

	// On Windows, rename fails by default if the file already exists, so we use the Windows specific MoveFileEx
#ifdef _WIN32
	if(!MoveFileEx(src, dstpath, MOVEFILE_REPLACE_EXISTING))
	{
		libc_calli(-1, "MoveFileEx", dstpath);
	}
#else
	libc_calli(rename(src, dstpath), "rename", dstpath);
#endif

	printv("move_file: %s -> %s\n", src, dstpath);
}


void file_create_from_memory(const char *dst, const char *memory, size_t size, bool overwrite_if_exists)
{
	if(file_exists(dst, NULL) && !overwrite_if_exists)
	{
		printv("file_create_from_memory: File exists: '%s' Skipping...\n", dst);
		return;
	}
	FILE *fdst = libc_callv(fopen(dst, "wb+"), "fopen", dst);
	size_t bytes_written = fwrite(memory, 1, size, fdst);
	assert(bytes_written == size);
	fclose(fdst);
	
	printv("file_create_from_memory: -> %s\n", dst);
}

void copy_executable(const char *src, const char *dst)
{
	copy_file(src, dst, CPY_CLOBBER);
}

void commands_show(command_buffer *buffer)
{
	if(opt_commands_show)
	{
		for(int i = 0; i < buffer->argc; i++)
		{
			printv("%s ", buffer->argv[i]);
		}
		printv("\n");
	}
}

// A wrapper around system() with some basic safety checks
int execute_shell_command(char *command)
{
	char *command_to_execute = command;
	if(!command)
	{
		printv("execute_shell_command: NULL command was passed!");
		return -1;
	}

	// Note: What do we do if a Windows user uses the Linux subsystem?
#ifndef _WIN32
	// Commands that start with the DASH character '-' must be given a space, otherwise the
	// shell may intepreted it as an option
	// This is specific to UNIX-like shells like sh, bash, zsh,...
	// where the system command invokes as execlp("sh", "sh", "-c", command, NULL);
	if(command[0] == '-')
	{
		int len = strlen(command);
		command_to_execute = malloc(len + 2);
		memcpy(&command_to_execute[1], command, len);
		command_to_execute[0] = ' ';
		command_to_execute[len] = 0;
	}
#endif

	int rc = system(command_to_execute);
	return rc;
}

#if defined (_WIN32)
void process_spawn(const char **commands)
{
	// Note: This does not do what we want in a multithreaded scenario
	// If one thread fails, the rest will continue executing their spawned processes
	// However, we want to terminate all child processes spawned by this program
	// In Linux, this is accomplished by calling killpg(0, SIGINT);
	// We will probably have to use CreateProcess and save some handles to get the same behaviour
	int rc = libc_calli(spawnvp(_P_WAIT, commands[0], commands), "spawnvp", "Could not spawn child process");
	if(rc != 0)
	{
		printv("ERROR: child process exited with exit code %d\n", rc);
		exit(rc);
	}
}

#else
void process_spawn(const char **commands)
{
	pid_t cpid = fork();
	if(cpid == -1)
	{
		libc_calli(cpid, "fork", "Could not execute child process");
	}
	//Child
	else if(cpid == 0)
	{
		libc_calli(execvp(commands[0], (char * const *)commands), "execvp", "Could not spawn process");
		_exit(0);
	}
	else
	{
		int child_status;
		waitpid(cpid, &child_status, 0);
		if(WIFEXITED(child_status))
		{
			int exit_status = WEXITSTATUS(child_status);
			if(exit_status != 0)
			{
				printv("ERROR: child process exited with exit code %d\n", exit_status);
				killpg(0, SIGINT);
			}

		}
	}
}

#endif /* _WIN32 */

// build_func must be a valid function pointer
// post_func is optional
void target_add(const char *name, buildfunc *build_func, post_build_func *post_func)
{
	if(target_count < TARGETS_MAX)
	{
		targets[target_count].name = name;
		targets[target_count].build_func = build_func;
		targets[target_count].post_func = post_func;
		target_count++;
	}
}

#define command_add(...) impl_command_add(buffer, __VA_ARGS__, NULL)
void impl_command_add(command_buffer *buffer, const char *cmd, ...)
{
	assert(buffer->argc < CMDS_MAX);
	const char *arg;
	va_list vl;
	arg = cmd;
	va_start(vl, cmd);
	while(arg && buffer->argc < CMDS_MAX - 1) // 1 space for null terminated array
	{
		buffer->argv[buffer->argc] = strdup(arg);
		arg = va_arg(vl, const char *);
		buffer->argc++;
	}
	buffer->argv[buffer->argc] = 0;
}

#define command_add_from_stack_array(array) impl_command_add_from_stack_array(buffer, array_count((array)), (array))
void impl_command_add_from_stack_array(command_buffer *buffer, const size_t cmd_count, const char **cmds)
{
	assert(buffer->argc < CMDS_MAX);
	for(size_t i = 0; (i < cmd_count && buffer->argc < CMDS_MAX); i++)
	{
		buffer->argv[buffer->argc++] = strdup(cmds[i]);
	}
	buffer->argv[buffer->argc] = 0;
}

// @temp
void iterate_buffer(command_buffer *buffer)
{
	printf("argc: %d\n", buffer->argc);
	for(int i = 0; i < buffer->argc; i++)
	{
		printf("[%d] %s\n", i, buffer->argv[i]);
	}
}


void build_run()
{

}
// Default cmd.exe does not support ansi color codes
//#if !defined (_WIN32)
#define PRINT_IN_GREEN(body) do{printf("\033[1;32m"); fflush(stdout); do{body;}while(0); \
printf("\033[0m"); \
fflush(stdout);\
printf("\033[0m");}while(0)

//#else
//#define PRINT_IN_GREEN(body) do {body;} while(0)
//#endif /* !_WIN32 */

void build_single_target(target *t)
{
	command_buffer *buffer = &t->commands;
	process_spawn(buffer->argv);
}


typedef struct
{
	int argc;
	char **argv;
} args;

void target_copy_commandline_arguments(target *t, args *a)
{
	if(!a) return;
	if(a->argc < 2) return;
	command_buffer *buffer = &t->commands;
	for(int arg = 1; arg < a->argc; arg++)
	{
		command_add(a->argv[arg]);
	}
}

#ifdef _WIN32
// Windows pthreads wrappers
#define PTHREAD_MUTEX_INITIALIZER {0}
typedef struct
{
	HANDLE *Handle;
} pthread_mutex_t;

typedef HANDLE pthread_t;
typedef void*(*posix_thread_proc)(void *data);
typedef DWORD(*win32_thread_proc)(void *data);

//mutex
void pthread_mutex_init(pthread_mutex_t *Mutex, void *Ignore0)
{
	Mutex->Handle = CreateMutex(0, FALSE, 0);
}


void pthread_mutex_lock(pthread_mutex_t *Mutex)
{
	WaitForSingleObject(Mutex->Handle, INFINITE);
}

void pthread_mutex_unlock(pthread_mutex_t *Mutex)
{
	ReleaseMutex(Mutex->Handle);
}

// pthread
int pthread_create(pthread_t *Thread, void *Ignore0, posix_thread_proc thread_proc, void *data)
{
	*Thread = CreateThread(0, 0, (win32_thread_proc)thread_proc, data, 0, 0);
	return (*Thread != NULL);
}


void pthread_join(pthread_t Thread, void *Ignore0)
{
	WaitForSingleObject(Thread, INFINITE);
}


void pthread_exit(void *ExitStatus)
{
	intptr_t status = (intptr_t)ExitStatus;
	ExitThread(status);
}


#endif /* _WIN32 */

int global_thread_target_idx = 0;
pthread_mutex_t global_target_lock = {0};
void *build_threadproc(void *data)
{
	for(;;)
	{
		int local_target_idx;
		pthread_mutex_lock(&global_target_lock);
		local_target_idx = global_thread_target_idx;
		if(local_target_idx < target_count)
		{
			global_thread_target_idx++;

			// This part does not need to actually need to be in the mutex
			// as far as data races go.
			// We are just doing this to get synchronized output

			//----------------------------------------------------------
			target *t = &targets[local_target_idx];
			PRINT_IN_GREEN(printv("Building target: %s\n", t->name));
			t->build_func(&t->commands, t->name);
			args *args = data;
			target_copy_commandline_arguments(t, args);
			commands_show(&t->commands);
			//----------------------------------------------------------

			pthread_mutex_unlock(&global_target_lock);

			//----------------------------------------------------------

			// Placeholder for the above code.
			// If printing or calling t->build_func is taking a long time,
			// it's safe to move it here

			//----------------------------------------------------------

			build_single_target(t);
			PRINT_IN_GREEN(printv("Built target: %s\n", t->name));
		}
		else
		{
			pthread_mutex_unlock(&global_target_lock);
			pthread_exit(NULL);
		}
	}
}
#ifdef _WIN32
int thread_count_get()
{
	SYSTEM_INFO SystemInfo;
	SystemInfo.dwNumberOfProcessors = 0;
	GetSystemInfo(&SystemInfo);
	return SystemInfo.dwNumberOfProcessors;
}
#else
int thread_count_get()
{
	return libc_calli(sysconf(_SC_NPROCESSORS_ONLN), "sysconf", "unable to get a thread count");
}
#endif /*_WIN32 */


#define THREADS_MAX 64
void build_run_all_threaded(int argc, char **argv)
{
	// We could subdivide the jobs instead of using a mutex
	global_thread_target_idx = 0;
	pthread_t threads[THREADS_MAX];
	int thread_count = thread_count_get();
	assert(thread_count > 0);
	int active_threads = (thread_count < THREADS_MAX) ? thread_count : THREADS_MAX;

	pthread_mutex_init(&global_target_lock, NULL);

	for(int i  = 0; i < active_threads; i++)
	{
		args A = {argc, argv};
		pthread_create(&threads[i], NULL, build_threadproc, &A);
	}

	for(int i  = 0; i < active_threads; i++)
	{
		pthread_join(threads[i], NULL);
	}
}


void build_run_all(int argc, char **argv)
{
	unsigned int i = 0;

	// Just so we don't check for this condition every time
	if(argc > 1)
	{
		for(i = 0; i < target_count; i++)
		{
			command_buffer *buffer = &targets[i].commands;
			targets[i].build_func(buffer, targets[i].name);
			for(int arg = 1; arg < argc; arg++)
			{
				command_add(argv[arg]);
			}
			commands_show(buffer);
			process_spawn(buffer->argv);
		}
	}
	else
	{
		for(i = 0; i < target_count; i++)
		{
			command_buffer *buffer = &targets[i].commands;
			PRINT_IN_GREEN(printv("Building target: %s\n", targets[i].name));
			targets[i].build_func(buffer, targets[i].name);
			commands_show(buffer);
			process_spawn(buffer->argv);
			PRINT_IN_GREEN(printv("Built target: %s\n\n", targets[i].name));
		}
	}
}

void post_run_all()
{
	unsigned int i = 0;
	for(i = 0; i < target_count; i++)
	{
		if(targets[i].post_func)
		{
			targets[i].post_func(targets[i].name);
		}
	}
}

#endif /* _BUILD_H_ */
