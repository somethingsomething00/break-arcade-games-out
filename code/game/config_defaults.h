#ifndef _CONFIG_DEFAULTS_H_
#define _CONFIG_DEFAULTS_H_

#define mouse_sensitivity_default 1.0f
#define mouse_sensitivity_min 0.1f
#define mouse_sensitivity_max 20.0f
#define add_screenshake_default add_screenshake_impl

#endif /* _CONFIG_DEFAULTS_H_ */
