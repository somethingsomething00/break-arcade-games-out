#include <GL/gl.h>

// OpenGL renderer
// Contains all functions related to drawing rectangles for the game

f32 calculate_aspect_multipler();
void R_EnableHardwareRenderer();

typedef struct
{
	f32 b;
	f32 g;
	f32 r;
} gl_v3;

f32 gl_ColorChannelToFloat(u8 channel)
{
	return (f32)channel / 255.0f;
}

gl_v3 gl_PixelUnpackToV3(u32 color)
{
	// BGR
	gl_v3 result =
	{
		.b = gl_ColorChannelToFloat((color >> 0) & 0xFF),
		.g = gl_ColorChannelToFloat((color >> 8) & 0xFF),
		.r = gl_ColorChannelToFloat((color >> 16) & 0xFF),
	};

	return result;
}


// Wrapper
void gl_DrawRect(int x0, int y0, int x1, int y1, u32 color, f32 alpha)
{
	gl_v3 c = gl_PixelUnpackToV3(color);
	glColor4f(c.r, c.g, c.b, alpha);
	
	glBegin(GL_TRIANGLES);

	// Bottom triangle
	glVertex2i(x0, y0);
	glVertex2i(x0, y1);
	glVertex2i(x1, y1);


	// Top triangle
	glVertex2i(x0, y0);
	glVertex2i(x1, y1);
	glVertex2i(x1, y0);

	glEnd();
}

void gl_DrawRotatedRect(int x0, int y0, int x1, int y1, u32 color, f32 alpha, f32 angle)
{
	f32 x = x0;
	f32 y = y0;
	f32 w = x1 - x0;
	f32 h = y1 - y0;
	f32 tx = x + (w / 2);
	f32 ty = y + (h / 2);

	// We are drawing quads with their origin at the top left
	// To rotate around the centre, we translate it back by half its
	// width and height so it's centered

	glPushMatrix();
	glTranslatef(tx, ty, 0);
	glRotatef(angle, 0, 0, 1.0);
	glTranslatef(-tx, -ty, 0);

	gl_DrawRect(
				x0, y0,
				x1, y1,
				color,
				alpha);

	glPopMatrix();
}

void gl_DrawBitmap(Bitmap *bitmap, int x0, int y0, int x1, int y1)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, bitmap->texture_id);

	glColor4f(1, 1, 1, 1);

	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2i(x0, y0);

	glTexCoord2f(0, 1);
	glVertex2i(x0, y1);

	glTexCoord2f(1, 1);
	glVertex2i(x1, y1);

	glTexCoord2f(1, 0);
	glVertex2i(x1, y0);

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}

// Note: Angle is expected in degrees
void gl_DrawRotatedRectWrapper(v2 p, v2 half_size, f32 angle, u32 color, f32 alpha)
{
	p = sub_v2(p, cam_p);
	f32 aspect_multiplier = calculate_aspect_multipler();

	half_size.x *= aspect_multiplier * cam_scale;
	half_size.y *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	int x0 = (int)(p.x-half_size.x);
	int y0 = (int)(p.y-half_size.y);
	int x1 = (int)(p.x+half_size.x);
	int y1 = (int)(p.y+half_size.y);

	// y0 += (half_size.y / 2);
	// y1 += (half_size.y / 2);

	gl_DrawRotatedRect(
					   x0, y0,
					   x1, y1,
					   color,
					   alpha,
					   angle);
}

F_RECT_OPAQUE(impl_gl_DrawRect)
{
	gl_DrawRect(x0, y0, x1, y1, color, 1.0f);
}

F_RECT_TRANSPARENT(impl_gl_DrawRectTransparent)
{
	gl_DrawRect(x0, y0, x1, y1, color, alpha);
}

F_RECT_OPAQUE_ROT(impl_gl_DrawRotatedRect)
{
	begin_profiling(PROFILING_DRAW_ROTATED);
	gl_DrawRotatedRectWrapper(p, half_size, angle, color, 1.0f);
	end_profiling(PROFILING_DRAW_ROTATED);
}

F_RECT_TRANSPARENT_ROT(impl_gl_DrawRotatedRectTransparent)
{
	begin_profiling(PROFILING_DRAW_ROTATED);
	gl_DrawRotatedRectWrapper(p, half_size, angle, color, alpha);
	end_profiling(PROFILING_DRAW_ROTATED);
}

F_CLEAR_SCREEN(impl_gl_ClearScreen)
{
    begin_profiling(PROFILING_DRAW_BACKGROUND);

	gl_v3 c = gl_PixelUnpackToV3(color);
	glClearColor(c.r, c.g, c.b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

    end_profiling(PROFILING_DRAW_BACKGROUND);
}

F_CLEAR_ARENA(impl_gl_ClearArenaScreen)
{
	impl_gl_ClearScreen(color);
}


F_BITMAP(impl_gl_DrawBitmap)
{
	begin_profiling(PROFILING_DRAW_BITMAP);

	p = sub_v2(p, cam_p);

	f32 aspect_multiplier = calculate_aspect_multipler();

	half_size.x *= aspect_multiplier * cam_scale;
	half_size.y *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	int x0 = (int)(p.x-half_size.x);
	int y0 = (int)(p.y-half_size.y);
	int x1 = (int)(p.x+half_size.x);
	int y1 = (int)(p.y+half_size.y);


	x0 = clamp(0, x0, render_buffer.width);
	x1 = clamp(0, x1, render_buffer.width);
	y0 = clamp(0, y0, render_buffer.height);
	y1 = clamp(0, y1, render_buffer.height);

	gl_DrawBitmap(bitmap, x0, y0, x1, y1);

	end_profiling(PROFILING_DRAW_BITMAP);
}

void R_EnableHardwareRenderer()
{
	clear_screen = impl_gl_ClearScreen;
	clear_arena_screen = impl_gl_ClearArenaScreen;

	draw_rect_in_pixels = impl_gl_DrawRect;
	draw_rect_in_pixels_transparent = impl_gl_DrawRectTransparent;

	draw_rotated_rect = impl_gl_DrawRotatedRect;
	draw_transparent_rotated_rect = impl_gl_DrawRotatedRectTransparent;

	draw_bitmap = impl_gl_DrawBitmap;
}
