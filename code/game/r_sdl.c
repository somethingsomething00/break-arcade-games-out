#include <SDL2/SDL.h>

// SDL2 renderer
// Contains all functions related to drawing rectangles for the game

extern SDL_Renderer *sdl_Renderer;
f32 calculate_aspect_multipler();

typedef struct
{
	u8 r;
	u8 g;
	u8 b;
	u8 a;
} sdl_color4;


// We will use a texture as our quad
// Since SDL2 doesn't allow rotated rects by default, we will overcome this limitation by using SDL_RenderCopyEx with our desired rotation applied
SDL_Texture *Quad;

// Bitmaps
// We reserve the 0th bitmap as empty
#define BITMAP_COUNT (ArrayCount(bitmaps))
SDL_Texture *Bitmaps[BITMAP_COUNT + 1];
int bitmapsAllocated = 0;


// Init bitmap texture memory
void R_Init()
{
#define QUAD_WIDTH 4
#define QUAD_HEIGHT 4

	static u32 QuadData[QUAD_WIDTH * QUAD_HEIGHT];
	Quad = SDL_CreateTexture(sdl_Renderer, PLATFORM_PREFERRED_PIXEL_FORMAT, SDL_TEXTUREACCESS_STATIC, QUAD_WIDTH, QUAD_HEIGHT);
	SDL_SetTextureBlendMode(Quad, SDL_BLENDMODE_BLEND);

	for(int i = 0; i < QUAD_WIDTH * QUAD_HEIGHT; i++)
	{
		QuadData[i] = 0xFFFFFFFF;
	}

	int pitch = QUAD_WIDTH * sizeof *QuadData;
	SDL_UpdateTexture(Quad, NULL, QuadData, pitch);

	// Create 0th bitmap
	SDL_Texture *DummyTexture = SDL_CreateTexture(sdl_Renderer, PLATFORM_PREFERRED_PIXEL_FORMAT, SDL_TEXTUREACCESS_STATIC, QUAD_WIDTH, QUAD_HEIGHT);

	SDL_UpdateTexture(DummyTexture, NULL, QuadData, pitch);
	Bitmaps[bitmapsAllocated++] = DummyTexture;

#undef QUAD_WIDTH
#undef QUAD_HEIGHT
}

u32 R_AllocateTexture(u32 width, u32 height, void *memory)
{
	u32 textureId;

	assert(bitmapsAllocated < (BITMAP_COUNT) + 1);

	SDL_Texture *Bitmap = SDL_CreateTexture(sdl_Renderer, PLATFORM_PREFERRED_PIXEL_FORMAT, SDL_TEXTUREACCESS_STATIC, width, height);
	
	SDL_SetTextureBlendMode(Bitmap, SDL_BLENDMODE_BLEND);
	SDL_SetTextureScaleMode(Bitmap, SDL_ScaleModeNearest);

	u32 pitch = width * PLATFORM_BYTES_PER_PIXEL;
	SDL_UpdateTexture(Bitmap, NULL, memory, pitch);

	textureId = bitmapsAllocated;
	Bitmaps[bitmapsAllocated] = Bitmap;

	bitmapsAllocated += 1;

	return textureId;
}

SDL_Texture *R_FetchTexture(u32 texture_id)
{
	SDL_Texture *Result = NULL;

	if(texture_id > bitmapsAllocated) Result = Bitmaps[0];
	else Result = Bitmaps[texture_id];

	return Result;
}

SDL_Rect R_MakeRect(int x0, int y0, int x1, int y1)
{
	int topx = x0;
	int topy = y0;
	int w = x1 - x0;
	int h = y1 - y0;

	SDL_Rect Rect = 
	{
		topx, topy, w, h
	};

	return Rect;
}

sdl_color4 sdl_PixelUnpack(u32 color)
{
	sdl_color4 Result = 
	{
		.r = (u8)((color >> 16) & 0xFF),
		.g = (u8)((color >> 8) & 0xFF),
		.b = (u8)((color >> 0) & 0xFF),
	};

	return Result;
}

u8 sdl_AlphaFromNormalized(float alpha)
{
	if(alpha < 0.0f) alpha  = 0.0f;
	else if (alpha > 1.0f) alpha = 1.0f;

	u32 a = alpha * 255.0f;

	return a;
}

void sdl_DrawRect(int x0, int y0, int x1, int y1, u32 color, float alpha)
{
	SDL_Rect Rect = R_MakeRect(x0, y0, x1, y1);

	sdl_color4 Color = sdl_PixelUnpack(color);
	u32 a = (255.0f * alpha);
	Color.a = a;
	SDL_SetRenderDrawColor(sdl_Renderer, Color.r, Color.g, Color.b, Color.a);
	SDL_RenderFillRect(sdl_Renderer, &Rect);
}

// Angle is in degrees
void sdl_DrawRotatedRect(int x0, int y0, int x1, int y1, f32 angle, u32 color, float alpha)
{
	SDL_Rect DestRect = R_MakeRect(x0, y0, x1, y1);

	sdl_color4 Color = sdl_PixelUnpack(color);
	u32 a = (255.0f * alpha);
	if(a > 255) a = 255;
	Color.a = a;
	
	SDL_SetTextureColorMod(Quad, Color.r, Color.g, Color.b);
	SDL_SetTextureAlphaMod(Quad, Color.a);
	SDL_RenderCopyEx(sdl_Renderer, Quad, NULL, &DestRect, angle, NULL, 0);
}

void sdl_DrawRotatedRectWrapper(v2 p, v2 half_size, f32 angle, u32 color, f32 alpha)
{
	p = sub_v2(p, cam_p);
	f32 aspect_multiplier = calculate_aspect_multipler();

	half_size.x *= aspect_multiplier * cam_scale;
	half_size.y *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	int x0 = (int)(p.x-half_size.x);
	int y0 = (int)(p.y-half_size.y);
	int x1 = (int)(p.x+half_size.x);
	int y1 = (int)(p.y+half_size.y);

	if(alpha < 0.0f) alpha = 0.0f;

	sdl_DrawRotatedRect(
			   x0, y0,
			   x1, y1,
			   angle,
			   color,
			   alpha);
}

void sdl_DrawBitmap(int x0, int y0, int x1, int y1, f32 alpha, u32 texture_id)
{
	u32 a = alpha * 255.0f;

	// Redundant since we check this in draw_bitmap but just in case
	if(a > 255) a = 255;

	SDL_Rect DestRect = R_MakeRect(x0, y0, x1, y1);

	SDL_Texture *Texture = R_FetchTexture(texture_id);
	SDL_SetTextureAlphaMod(Texture, a);
	SDL_RenderCopyEx(sdl_Renderer, Texture, NULL, &DestRect, 0, NULL, 0);
}


F_RECT_OPAQUE(impl_sdl_DrawRect)
{
	sdl_DrawRect(x0, y0, x1, y1, color, 1.0f);
}

F_RECT_TRANSPARENT(impl_sdl_DrawRectTransparent)
{
	sdl_DrawRect(x0, y0, x1, y1, color, alpha);
}

F_RECT_OPAQUE_ROT(impl_sdl_DrawRotatedRect)
{
	begin_profiling(PROFILING_DRAW_ROTATED);
	sdl_DrawRotatedRectWrapper(p, half_size, angle, color, 1.0f);
	end_profiling(PROFILING_DRAW_ROTATED);
}

F_RECT_TRANSPARENT_ROT(impl_sdl_DrawRotatedRectTransparent)
{
	begin_profiling(PROFILING_DRAW_ROTATED);
	sdl_DrawRotatedRectWrapper(p, half_size, angle, color, alpha);
	end_profiling(PROFILING_DRAW_ROTATED);
}

F_CLEAR_SCREEN(impl_sdl_ClearScreen)
{
	begin_profiling(PROFILING_DRAW_BACKGROUND);

	sdl_color4 c = sdl_PixelUnpack(color);
	SDL_SetRenderDrawColor(sdl_Renderer, c.r, c.g, c.b, 255);
	SDL_RenderClear(sdl_Renderer);

	end_profiling(PROFILING_DRAW_BACKGROUND);
}

F_CLEAR_ARENA(impl_sdl_ClearArenaScreen)
{
	impl_sdl_ClearScreen(color);
}

F_BITMAP(impl_sdl_DrawBitmap)
{
	begin_profiling(PROFILING_DRAW_BITMAP);

	p = sub_v2(p, cam_p);

	f32 aspect_multiplier = calculate_aspect_multipler();

	half_size.x *= aspect_multiplier * cam_scale;
	half_size.y *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	int x0 = (int)(p.x-half_size.x);
	int y0 = (int)(p.y-half_size.y);
	int x1 = (int)(p.x+half_size.x);
	int y1 = (int)(p.y+half_size.y);

	x0 = clamp(0, x0, render_buffer.width);
	x1 = clamp(0, x1, render_buffer.width);
	y0 = clamp(0, y0, render_buffer.height);
	y1 = clamp(0, y1, render_buffer.height);

	if(alpha_multiplier < 0) alpha_multiplier = 0;
	if(alpha_multiplier > 1.0) alpha_multiplier = 1.0;

	sdl_DrawBitmap(x0, y0, x1, y1, alpha_multiplier, bitmap->texture_id);

	end_profiling(PROFILING_DRAW_BITMAP);
}

void R_EnableHardwareRenderer()
{
	clear_screen = impl_sdl_ClearScreen;
	clear_arena_screen = impl_sdl_ClearArenaScreen;

	draw_rect_in_pixels = impl_sdl_DrawRect;
	draw_rect_in_pixels_transparent = impl_sdl_DrawRectTransparent;

	draw_rotated_rect = impl_sdl_DrawRotatedRect;
	draw_transparent_rotated_rect = impl_sdl_DrawRotatedRectTransparent;

	draw_bitmap = impl_sdl_DrawBitmap;
}
