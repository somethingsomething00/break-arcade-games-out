global_variable v2 cam_p, cam_dp;
global_variable f32 cam_scale = 0.01f;
global_variable b32 buffer_was_changed = 1;

#include "r_impl.c"
#include "r_software.c"

#if GL_USE_RENDERER_HW
	#include "r_gl.c"
#endif
#if SDL_USE_RENDERER
	#include "r_sdl.c"
#endif

#include "r_common.c"
