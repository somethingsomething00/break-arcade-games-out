// Software renderer
// Contains all functions related to drawing rectangles for the game


f32
calculate_aspect_multipler();

void 
R_EnableSoftwareRenderer();

void
sw_clear_screen(const u32 color) {
    begin_profiling(PROFILING_DRAW_BACKGROUND);


	// @New 64 bit copy and unrolled loop
	// Old behavior: Pixel was copied 32 bits at a time (1 pixel's worth)
	// New behaviour: We do a 64 bit copy (2 pixels worth) and on top of that unroll the loop 4 times
	// On both optimized and unoptimized build this runs ~2.8 times faster
	// Also unrolled the 32 bit copy 4 times if doing 32 bit copies on a particular platform is faster
	// Note that this requires that the framebuffer dimensions supplied by the platform layer are evenly divisible by 8

	// Finally, we now support a simd version of the copy.
	// The appropriate define has to be used to include it in the compiled code.

#if PLATFORM_USE_INTRINSICS
	intrin_clear_screen(&render_buffer, color);
#else

	u32 *pixel;
	int x;
	int y;
#if 1
	const u64 color64 = (color | (u64)color << 32);
	u64 *pixel64 = (u64 *)render_buffer.pixels;
	for (y = 0; y < render_buffer.height; y++) {
		for (x = 0; x < render_buffer.width / 8; x++) {
			pixel64[0] = color64;
			pixel64[1] = color64;
			pixel64[2] = color64;
			pixel64[3] = color64;
			pixel64 += 4;
		}
	}
	if(render_buffer.width & 7) {

		for(x = 0, pixel = (u32 *)pixel64; x < (render_buffer.width & 7); x++) {
			*pixel++ = color;
		}
	}

#else
	// For 32 bit platforms
	pixel = (u32 *)render_buffer.pixels;
	for (y = 0; y < render_buffer.height; y++) {
		for (x = 0; x < render_buffer.width / 4; x++) {
			pixel[0] = color;
			pixel[1] = color;
			pixel[2] = color;
			pixel[3] = color;
			pixel += 4;
		}
	}
	if(render_buffer.width & 7) {

		for(x = 0; x < (render_buffer.width & 7); x++) {
			*pixel++ = color;
		}
	}
#endif



#endif /* PLATFORM_USE_SIMD */
	end_profiling(PROFILING_DRAW_BACKGROUND);
}


internal void
sw_draw_rect_in_pixels(int x0, int y0, int x1, int y1, u32 color) {

	x0 = clamp(0, x0, render_buffer.width);
	x1 = clamp(0, x1, render_buffer.width);
	y0 = clamp(0, y0, render_buffer.height);
	y1 = clamp(0, y1, render_buffer.height);

	u32 *row = &render_buffer.pixels[x0 + render_buffer.width*y0];
	u32 *pixel = row;
	int stride = render_buffer.width;


	for (int y = y0; y < y1; y++) {
		for (int x = x0; x < x1; x++) {
			*pixel++ = color;
		}

		row += stride;
		pixel = row;
	}
}


internal void
sw_draw_rect_in_pixels_transparent(int x0, int y0, int x1, int y1, u32 color, f32 alpha) {

	alpha = clampf(0.f, alpha, 1.f);

	x0 = clamp(0, x0, render_buffer.width);
	x1 = clamp(0, x1, render_buffer.width);
	y0 = clamp(0, y0, render_buffer.height);
	y1 = clamp(0, y1, render_buffer.height);

	u32 *row = render_buffer.pixels + x0 + render_buffer.width*y0;
	u32 *pixel = row;
	int stride = render_buffer.width;
	u32 p;


	for (int y = y0; y < y1; y++) {
		for (int x = x0; x < x1; x++) {
			p = *pixel;
			*pixel++ = lerp_color(p, alpha, color); //@Speed
		}
		row += stride;
		pixel = row;
	}
}


internal void
sw_clear_arena_screen(v2 p, f32 left_most, f32 right_most, f32 half_size_y, u32 color) {
#if 0
	begin_profiling(PROFILING_DRAW_BACKGROUND);
	f32 aspect_multiplier = calculate_aspect_multipler();
	p = sub_v2(p, cam_p);

	half_size_y *= aspect_multiplier * cam_scale;
	left_most *= aspect_multiplier * cam_scale;
	right_most *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	int x0 = (int)(p.x+left_most);
	// int y0 = (int)(p.y-half_size_y);
	int x1 = (int)(p.x+right_most);
	int y1 = (int)(p.y+half_size_y);

	// @Optimize: Clearing the main arena screen could easily be done in simd
	draw_rect_in_pixels(x0, 0, x1, y1, color);
	end_profiling(PROFILING_DRAW_BACKGROUND);
#else
	// @New: Given the current arena size (where it covers most of the screen), it's faster to clear the entire screen with the arena colour rather than call the more specific draw_rect_in_pixels().
	// The arena walls are then drawn over in draw_arena_rects
	sw_clear_screen(color);
#endif
}


internal void
sw_draw_transparent_rotated_rect(v2 p, v2 half_size, f32 angle, u32 color, f32 alpha) { //In degrees
	begin_profiling(PROFILING_DRAW_ROTATED);

	alpha = clampf(0, alpha, 1);
	p = sub_v2(p, cam_p);
	angle = deg_to_rad(angle);

	f32 cos = cosf(angle);
	f32 sin = sinf(angle);

	v2 x_axis = (v2){cos, sin};
	v2 y_axis = (v2){-sin, cos};

	m2 rotation = (m2){ //@Speed @Clenaup: Maybe do a single matrix multiplication?
		x_axis.x, y_axis.x,
		x_axis.y, y_axis.y,
	};

	Rect2 rect = make_rect_center_half_size((v2){0, 0}, half_size);

	for (int i = 0; i < 4; i++)
		rect.p[i] = mul_m2_v2(rotation, rect.p[i]);


	// Change to pixels

	f32 aspect_multiplier = calculate_aspect_multipler();
	f32 s = aspect_multiplier * cam_scale;

	m2 world_to_pixels_scale_transform = (m2){
		s, 0,
		0, s,
	};

	v2 position_v = (v2){(f32)render_buffer.width * .5f,
		(f32)render_buffer.height * .5f};

	v2i min_bound = (v2i){render_buffer.width, render_buffer.height};
	v2i max_bound = (v2i){0, 0};


	for (int i = 0; i < 4; i++) {
		rect.p[i] = add_v2(p, rect.p[i]);
		rect.p[i] = mul_m2_v2(world_to_pixels_scale_transform, rect.p[i]);
		rect.p[i] = add_v2(rect.p[i], position_v);

		int x_t = trunc_f32(rect.p[i].x);
		int y_t = trunc_f32(rect.p[i].y);
		int x_c = ceil_f32(rect.p[i].x);
		int y_c = ceil_f32(rect.p[i].y);

		if (x_t < min_bound.x) min_bound.x = x_t;
		if (x_c > max_bound.x) max_bound.x = x_c;
		if (y_t < min_bound.y) min_bound.y = y_t;
		if (y_c > max_bound.y) max_bound.y = y_c;
	}

	min_bound.x = clamp(0, min_bound.x, render_buffer.width);
	max_bound.x = clamp(0, max_bound.x, render_buffer.width);
	min_bound.y = clamp(0, min_bound.y, render_buffer.height);
	max_bound.y = clamp(0, max_bound.y, render_buffer.height);


	// In pixels

	v2 axis_1 = sub_v2(rect.p[1], rect.p[0]);
	v2 axis_2 = sub_v2(rect.p[3], rect.p[0]);
	f32 axis_1_len = len_v2(axis_1);
	f32 axis_2_len = len_v2(axis_2);
	axis_1 = normalize(axis_1);
	axis_2 = normalize(axis_2);

	f32 b_r = (f32)((color & 0xff0000) >> 16);
	f32 b_g = (f32)((color & 0xff00) >> 8);
	f32 b_b = (f32)(color & 0xff);

	f32 source_r = alpha*b_r;
	f32 source_g = alpha*b_g;
	f32 source_b = alpha*b_b;
	f32 inv_alpha = 1-alpha;

	u32 *row = render_buffer.pixels + min_bound.x+1 + render_buffer.width*min_bound.y+1;
	u32 *pixel = row;
	int stride = render_buffer.width;


	for (int y = min_bound.y+1; y < max_bound.y; y++) {
		for (int x = min_bound.x+1; x < max_bound.x; x++) {

			f32 pixel_p_rel_x = (f32)x - rect.p[0].x;
			f32 pixel_p_rel_y = (f32)y - rect.p[0].y;

			f32 proj_1 = pixel_p_rel_x*axis_1.x + pixel_p_rel_y*axis_1.y;
			f32 proj_2 = pixel_p_rel_x*axis_2.x + pixel_p_rel_y*axis_2.y;

			if (proj_1 >= 0 &&
				proj_1 <= axis_1_len &&
				proj_2 >= 0 &&
				proj_2 <=  axis_2_len) {

				f32 a_r = (f32)((*pixel & 0xff0000) >> 16);
				f32 a_g = (f32)((*pixel & 0xff00) >> 8);
				f32 a_b = (f32)(*pixel & 0xff);

				u8 r = (u8)(inv_alpha*a_r + source_r);
				u8 g = (u8)(inv_alpha*a_g + source_g);
				u8 b = (u8)(inv_alpha*a_b + source_b);

				u32 pixel_color = b | (g << 8) | (r << 16);
				*pixel = pixel_color;
			}

			pixel++;
		}
		row += stride;
		pixel = row;
	}


#if 0
	for (int y = min_bound.y; y < max_bound.y; y++) {
		u32 *pixel = render_buffer.pixels + min_bound.x + render_buffer.width*y;

		int x1 = (int)(((f32)y - b)/a); // a = axis.y/axis.x; b is dor x = 0, where is y

		for (int x = x1; x < x2; x++) {
			*pixel = lerp_color(*pixel, alpha, color); //@Speed
			pixel++;
		}

		/*
		int min_x = ?
			int max_x = ?*/
	}
#endif

	end_profiling(PROFILING_DRAW_ROTATED);
}


internal void
sw_draw_rotated_rect(v2 p, v2 half_size, f32 angle, u32 color) { //In degrees
	begin_profiling(PROFILING_DRAW_ROTATED);

	angle = deg_to_rad(angle);
	p = sub_v2(p, cam_p);

	f32 cos = cosf(angle);
	f32 sin = sinf(angle);

	v2 x_axis = (v2){cos, sin};
	v2 y_axis = (v2){-sin, cos};

	m2 rotation = (m2){ //@Speed @Clenaup: Maybe do a single matrix multiplication?
		x_axis.x, y_axis.x,
		x_axis.y, y_axis.y,
	};

	Rect2 rect = make_rect_center_half_size((v2){0, 0}, half_size);

	for (int i = 0; i < 4; i++)
		rect.p[i] = mul_m2_v2(rotation, rect.p[i]);


	// Change to pixels

	f32 aspect_multiplier = calculate_aspect_multipler();
	f32 s = aspect_multiplier * cam_scale;

	m2 world_to_pixels_scale_transform = (m2){
		s, 0,
		0, s,
	};

	v2 position_v = (v2){(f32)render_buffer.width * .5f,
		(f32)render_buffer.height * .5f};

	v2i min_bound = (v2i){render_buffer.width, render_buffer.height};
	v2i max_bound = (v2i){0, 0};


	for (int i = 0; i < 4; i++) {
		rect.p[i] = add_v2(p, rect.p[i]);
		rect.p[i] = mul_m2_v2(world_to_pixels_scale_transform, rect.p[i]);
		rect.p[i] = add_v2(rect.p[i], position_v);

		int x_t = trunc_f32(rect.p[i].x);
		int y_t = trunc_f32(rect.p[i].y);
		int x_c = ceil_f32(rect.p[i].x);
		int y_c = ceil_f32(rect.p[i].y);

		if (x_t < min_bound.x) min_bound.x = x_t;
		if (x_c > max_bound.x) max_bound.x = x_c;
		if (y_t < min_bound.y) min_bound.y = y_t;
		if (y_c > max_bound.y) max_bound.y = y_c;
	}

	min_bound.x = clamp(0, min_bound.x, render_buffer.width);
	max_bound.x = clamp(0, max_bound.x, render_buffer.width);
	min_bound.y = clamp(0, min_bound.y, render_buffer.height);
	max_bound.y = clamp(0, max_bound.y, render_buffer.height);


	// In pixels

	v2 axis_1 = sub_v2(rect.p[1], rect.p[0]);
	v2 axis_2 = sub_v2(rect.p[3], rect.p[0]);
	f32 axis_1_len = len_v2(axis_1);
	f32 axis_2_len = len_v2(axis_2);
	axis_1 = normalize(axis_1);
	axis_2 = normalize(axis_2);

	u32 *row = render_buffer.pixels + min_bound.x+1 + render_buffer.width*min_bound.y+1;
	u32 *pixel = row;
	int stride = render_buffer.width;
	for (int y = min_bound.y+1; y < max_bound.y; y++) {
		for (int x = min_bound.x+1; x < max_bound.x; x++) {

			f32 pixel_p_rel_x = (f32)x - rect.p[0].x;
			f32 pixel_p_rel_y = (f32)y - rect.p[0].y;

			f32 proj_1 = pixel_p_rel_x*axis_1.x + pixel_p_rel_y*axis_1.y;
			f32 proj_2 = pixel_p_rel_x*axis_2.x + pixel_p_rel_y*axis_2.y;

			if (proj_1 >= 0 &&
				proj_1 <= axis_1_len &&
				proj_2 >= 0 &&
				proj_2 <=  axis_2_len) {
				*pixel = color;
			}

			pixel++;
		}
		row += stride;
		pixel = row;
	}


#if 0
	for (int y = min_bound.y; y < max_bound.y; y++) {
		u32 *pixel = render_buffer.pixels + min_bound.x + render_buffer.width*y;

		int x1 = (int)(((f32)y - b)/a); // a = axis.y/axis.x; b is dor x = 0, where is y

		for (int x = x1; x < x2; x++) {
			*pixel = lerp_color(*pixel, alpha, color); //@Speed
			pixel++;
		}

		/*
		int min_x = ?
		int max_x = ?*/
	}
#endif

	end_profiling(PROFILING_DRAW_ROTATED);
}


internal void
sw_draw_bitmap(Bitmap *bitmap, v2 p, v2 half_size, f32 alpha_multiplier) {

	begin_profiling(PROFILING_DRAW_BITMAP);

	p = sub_v2(p, cam_p);
	alpha_multiplier = clampf(0.f, alpha_multiplier, 1.f)/255.f;

	f32 aspect_multiplier = calculate_aspect_multipler();

	half_size.x *= aspect_multiplier * cam_scale;
	half_size.y *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	int x0 = (int)(p.x-half_size.x);
	int y0 = (int)(p.y-half_size.y);
	int x1 = (int)(p.x+half_size.x);
	int y1 = (int)(p.y+half_size.y);


	// In Pixels

	x0 = clamp(0, x0, render_buffer.width);
	x1 = clamp(0, x1, render_buffer.width);
	y0 = clamp(0, y0, render_buffer.height);
	y1 = clamp(0, y1, render_buffer.height);

	f32 range_x = (f32)(x1 - x0);
	f32 range_y = (f32)(y1 - y0);

	// our UV fetch is wrong when the bitmap goes partially outside the area (it gets stretched not considering the

	u32 *row = render_buffer.pixels + x0 + render_buffer.width*y0;
	u32 *pixel = row;
	int stride = render_buffer.width;
	for (int y = y0; y < y1; y++) {

		f32 v = ((f32)y-y0)/range_y;
		int uv_stride = (int)(v*(f32)bitmap->height)*bitmap->width;
		u32 *source_pixels = bitmap->pixels + uv_stride;
		for (int x = x0; x < x1; x++) {

			f32 u = ((f32)x-x0)/range_x;

			int pixel_x = (int)(u*(f32)bitmap->width);

			u32 bitmap_color = *(source_pixels + pixel_x);

			f32 alpha = (f32)((bitmap_color & 0xff000000) >> 24)*alpha_multiplier;
			assert(alpha >= 0.f && alpha <= 255.f);

			// color lerp with alpha
			f32 a_r = (f32)((*pixel & 0xff0000) >> 16);
			f32 a_g = (f32)((*pixel & 0xff00) >> 8);
			f32 a_b = (f32)(*pixel & 0xff);

			f32 b_r = (f32)((bitmap_color & 0xff0000) >> 16);
			f32 b_g = (f32)((bitmap_color & 0xff00) >> 8);
			f32 b_b = (f32)(bitmap_color & 0xff);

			u8 r = (u8)((1-alpha)*a_r + alpha*b_r);
			u8 g = (u8)((1-alpha)*a_g + alpha*b_g);
			u8 b = (u8)((1-alpha)*a_b + alpha*b_b);

			u32 pixel_color = b | (g << 8) | (r << 16);
			*pixel++ = pixel_color;
		}
		row += stride;
		pixel = row;
	}

	end_profiling(PROFILING_DRAW_BITMAP);
}

#ifndef USE_SW_RENDERER_NO_FP

void R_EnableSoftwareRenderer()
{
	clear_screen = sw_clear_screen;
	clear_arena_screen = sw_clear_arena_screen;

	draw_rect_in_pixels = sw_draw_rect_in_pixels;
	draw_rect_in_pixels_transparent = sw_draw_rect_in_pixels_transparent;

	draw_rotated_rect = sw_draw_rotated_rect;
	draw_transparent_rotated_rect = sw_draw_transparent_rotated_rect;

	draw_bitmap = sw_draw_bitmap;
}

#endif /* !USE_SW_RENDERER_NO_FP */


#ifdef USE_SW_RENDERER_NO_FP

#define R_EnableSoftwareRenderer(...)

// We don't want the function pointer overhead for our X11 platform layer, so we redefine our function calls
// so that they work in the main game code by prefixing `sw_` in front of the function calls

#define draw_rect_in_pixels 				sw_draw_rect_in_pixels
#define draw_rect_in_pixels_transparent  	sw_draw_rect_in_pixels_transparent

#define draw_rotated_rect 					sw_draw_rotated_rect
#define draw_transparent_rotated_rect  		sw_draw_transparent_rotated_rect

#define clear_screen 						sw_clear_screen
#define clear_arena_screen  				sw_clear_arena_screen

#define draw_bitmap 						sw_draw_bitmap

#endif /* USE_SW_RENDERER_NO_FP */
