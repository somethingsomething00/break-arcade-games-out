// Compatability for the win32_gdi file
// jsys_ThreadSync is called in game.c, but it's not needed
// when building the win32_gdi_platform.c file.

#ifndef _COMPAT_H_

void jsys_ThreadSync()
{
	return;
}

#define Assert(cond) assert(cond)

#define _COMPAT_H_

#endif /* _COMPAT_H_ */
