// NAME: r_common.c
// TIME OF CREATION: 2022-05-11 04:42:03
// AUTHOR:
// DESCRIPTION: Common functions for all renderers.
// 				Utilises functions in either r_software.c or r_gl.c to draw rectangles



f32
calculate_aspect_multipler() {
	// @Hardcoded: Aspect ratio is fixed to 16:9
	// This causes issues when rending the game and text

	// Is having a static faster, or slower, than a u32 to float conversion?
	static f32 aspect_multiplier;
	if(buffer_was_changed) {
		if ((f32)render_buffer.width / (f32)render_buffer.height < 1.77f)
			aspect_multiplier = (f32)render_buffer.width / 1.77f;
		else
			aspect_multiplier = (f32)render_buffer.height;
		buffer_was_changed = 0;
	}
	return aspect_multiplier;
}


internal v2
pixels_dp_to_world(v2i pixels_coord) {

	f32 aspect_multiplier = calculate_aspect_multipler();

	v2 result;
	result.x = (f32)pixels_coord.x;
	result.y = (f32)pixels_coord.y;

	result.x /= aspect_multiplier;
	result.x /= cam_scale;

	result.y /= aspect_multiplier;
	result.y /= cam_scale;

	return result;
}

void
draw_rect(v2 p, v2 half_size, u32 color) {

	begin_profiling(PROFILING_DRAW_RECT);

	p = sub_v2(p, cam_p);
	f32 aspect_multiplier = calculate_aspect_multipler();

	half_size.x *= aspect_multiplier * cam_scale;
	half_size.y *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	int x0 = (int)(p.x-half_size.x);
	int y0 = (int)(p.y-half_size.y);
	int x1 = (int)(p.x+half_size.x);
	int y1 = (int)(p.y+half_size.y);

	draw_rect_in_pixels(x0, y0, x1, y1, color);

	end_profiling(PROFILING_DRAW_RECT);
}

void
draw_transparent_rect(v2 p, v2 half_size, u32 color, f32 alpha) {

	alpha = clampf(0, alpha, 1);

	begin_profiling(PROFILING_DRAW_RECT);

	p = sub_v2(p, cam_p);
	f32 aspect_multiplier = calculate_aspect_multipler();

	half_size.x *= aspect_multiplier * cam_scale;
	half_size.y *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	int x0 = (int)(p.x-half_size.x);
	int y0 = (int)(p.y-half_size.y);
	int x1 = (int)(p.x+half_size.x);
	int y1 = (int)(p.y+half_size.y);

	draw_rect_in_pixels_transparent(x0, y0, x1, y1, color, alpha);

	end_profiling(PROFILING_DRAW_RECT);
}


void
draw_arena_rects(v2 p, f32 left_most, f32 right_most, f32 half_size_y, u32 color) {

	f32 aspect_multiplier = calculate_aspect_multipler();
	p = sub_v2(p, cam_p);

	half_size_y *= aspect_multiplier * cam_scale;
	left_most   *= aspect_multiplier * cam_scale;
	right_most  *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	int x0 = (int)(p.x+left_most);
	// int y0 = (int)(p.y-half_size_y);
	int x1 = (int)(p.x+right_most);
	int y1 = (int)(p.y+half_size_y);

	draw_rect_in_pixels(0, 0, x0, render_buffer.height, color);
	draw_rect_in_pixels(x1, 0, render_buffer.width, render_buffer.height, color);

	draw_rect_in_pixels(x0, y1, x1, render_buffer.height, color);
}


f32
draw_number(int number, v2 p, f32 size, u32 color, int number_of_leading_zeros, b32 centered_text) {

	f32 square_size = size / 5.f;
	f32 half_square_size = size / 10.f;

	b32 draw_minus = false;
	if (number < 0) {
		number *= -1;
		draw_minus = true;
	}

	if (centered_text) {

		f32 last_move = 0.f;
		int digit = number % 10;
		int n = number;
		int zeros = number_of_leading_zeros;
		while (n || zeros > 0) {
			zeros--;
			if (digit == 1) last_move = square_size;
			else last_move = square_size*2.f;

			p.x += last_move;

			n /= 10;
			digit = n % 10;
		}
		p.x -= last_move+square_size;
	}

	int digit = number % 10;

	while (number || number_of_leading_zeros > 0) {
		number_of_leading_zeros--;

		switch(digit) {
			case 0: {
				draw_rect((v2){p.x-square_size, p.y},     (v2){half_square_size, 2.5f*square_size}, color);
				draw_rect((v2){p.x+square_size, p.y},     (v2){half_square_size, 2.5f*square_size}, color);
				draw_rect((v2){p.x, p.y+square_size*2.f}, (v2){half_square_size, half_square_size}, color);
				draw_rect((v2){p.x, p.y-square_size*2.f}, (v2){half_square_size, half_square_size}, color);
				p.x -= square_size*4.f;
			} break;

			case 1: {
				draw_rect((v2){p.x+square_size, p.y},     (v2){half_square_size, 2.5f*square_size}, color);
				p.x -= square_size*2.f;
			} break;

			case 2: {
				draw_rect((v2){p.x, p.y+square_size*2.f}, (v2){1.5f*square_size, half_square_size}, color);
				draw_rect((v2){p.x, p.y},                 (v2){1.5f*square_size, half_square_size}, color);
				draw_rect((v2){p.x, p.y-square_size*2.f}, (v2){1.5f*square_size, half_square_size}, color);
				draw_rect((v2){p.x+square_size, p.y+square_size}, (v2){half_square_size, half_square_size}, color);
				draw_rect((v2){p.x-square_size, p.y-square_size}, (v2){half_square_size, half_square_size}, color);
				p.x -= square_size*4.f;
			} break;

			case 3: {
				draw_rect((v2){p.x-half_square_size, p.y+square_size*2.f}, (v2){square_size, half_square_size}, color);
				draw_rect((v2){p.x-half_square_size, p.y},                 (v2){square_size, half_square_size}, color);
				draw_rect((v2){p.x-half_square_size, p.y-square_size*2.f}, (v2){square_size, half_square_size}, color);
				draw_rect((v2){p.x+square_size, p.y}, (v2){half_square_size, 2.5f*square_size}, color);
				p.x -= square_size*4.f;
			} break;

			case 4: {
				draw_rect((v2){p.x+square_size, p.y},             (v2){half_square_size, 2.5f*square_size}, color);
				draw_rect((v2){p.x-square_size, p.y+square_size}, (v2){half_square_size, 1.5f*square_size}, color);
				draw_rect((v2){p.x, p.y},                         (v2){half_square_size, half_square_size}, color);
				p.x -= square_size*4.f;
			} break;

			case 5: {
				draw_rect((v2){p.x, p.y+square_size*2.f}, (v2){1.5f*square_size, half_square_size}, color);
				draw_rect((v2){p.x, p.y},                 (v2){1.5f*square_size, half_square_size}, color);
				draw_rect((v2){p.x, p.y-square_size*2.f}, (v2){1.5f*square_size, half_square_size}, color);
				draw_rect((v2){p.x-square_size, p.y+square_size}, (v2){half_square_size, half_square_size}, color);
				draw_rect((v2){p.x+square_size, p.y-square_size}, (v2){half_square_size, half_square_size}, color);
				p.x -= square_size*4.f;
			} break;

			case 6: {
				draw_rect((v2){p.x+half_square_size, p.y+square_size*2.f}, (v2){square_size, half_square_size}, color);
				draw_rect((v2){p.x+half_square_size, p.y},                 (v2){square_size, half_square_size}, color);
				draw_rect((v2){p.x+half_square_size, p.y-square_size*2.f}, (v2){square_size, half_square_size}, color);
				draw_rect((v2){p.x-square_size, p.y}, (v2){half_square_size, 2.5f*square_size}, color);
				draw_rect((v2){p.x+square_size, p.y-square_size},          (v2){half_square_size, half_square_size}, color);
				p.x -= square_size*4.f;
			} break;

			case 7: {
				draw_rect((v2){p.x+square_size, p.y},             (v2){half_square_size, 2.5f*square_size}, color);
				draw_rect((v2){p.x-half_square_size, p.y+square_size*2.f}, (v2){square_size, half_square_size}, color);
				p.x -= square_size*4.f;
			} break;

			case 8: {
				draw_rect((v2){p.x-square_size, p.y},     (v2){half_square_size, 2.5f*square_size}, color);
				draw_rect((v2){p.x+square_size, p.y},     (v2){half_square_size, 2.5f*square_size}, color);
				draw_rect((v2){p.x, p.y+square_size*2.f}, (v2){half_square_size, half_square_size}, color);
				draw_rect((v2){p.x, p.y-square_size*2.f}, (v2){half_square_size, half_square_size}, color);
				draw_rect((v2){p.x, p.y},                         (v2){half_square_size, half_square_size}, color);
				p.x -= square_size*4.f;
			} break;

			case 9: {
				draw_rect((v2){p.x-half_square_size, p.y+square_size*2.f}, (v2){square_size, half_square_size}, color);
				draw_rect((v2){p.x-half_square_size, p.y},                 (v2){square_size, half_square_size}, color);
				draw_rect((v2){p.x-half_square_size, p.y-square_size*2.f}, (v2){square_size, half_square_size}, color);
				draw_rect((v2){p.x+square_size, p.y},                      (v2){half_square_size, 2.5f*square_size}, color);
				draw_rect((v2){p.x-square_size, p.y+square_size},          (v2){half_square_size, half_square_size}, color);
				p.x -= square_size*4.f;
			} break;

			invalid_default_case;
		}

		number /= 10;
		digit = number % 10;
	}

	if (draw_minus) draw_rect((v2){p.x, p.y}, (v2){2.f*half_square_size, half_square_size}, color);

	return p.x;

}


void
draw_f32(f32 num, v2 p, f32 size, b32 color) {

	int whole = (int)num;
	int frac = (int)(100.f*(absf(num)-whole));

	p.x = draw_number(frac, p, size, color, 2, false);
	draw_rect(add_v2(p, (v2){.5f, -1.f}), (v2){.25f, .25f}, color);
	p.x -= 1.f;
	draw_number(whole, p, size, color, 1, false);
}


#if DEVELOPMENT
internal void print_int(int a);
#endif

internal void
draw_rect_subpixel(v2 p, v2 half_size, u32 color) {

	f32 aspect_multiplier = calculate_aspect_multipler();
	p = sub_v2(p, cam_p);

	half_size.x *= aspect_multiplier * cam_scale;
	half_size.y *= aspect_multiplier * cam_scale;

	p.x *= aspect_multiplier * cam_scale;
	p.y *= aspect_multiplier * cam_scale;

	p.x += (f32)render_buffer.width * .5f;
	p.y += (f32)render_buffer.height * .5f;

	f32 x0f = p.x-half_size.x +.5f;
	int x0 = (int)(x0f);
	f32 x0_alpha = x0f - (f32)x0;

	f32 y0f = p.y-half_size.y +.5f;
	int y0 = (int)(y0f);
	f32 y0_alpha = y0f - (f32)y0;

	f32 x1f = p.x+half_size.x +.5f;
	int x1 = (int)(x1f);
	f32 x1_alpha = x1f - (f32)x1;

	f32 y1f = p.y+half_size.y +.5f;
	int y1 = (int)(y1f);
	f32 y1_alpha = y1f - (f32)y1;

	draw_rect_in_pixels_transparent(x0, y0+1, x0+1, y1, color, 1.f-x0_alpha);
	draw_rect_in_pixels_transparent(x1, y0+1, x1+1, y1, color, x1_alpha);

	draw_rect_in_pixels_transparent(x0+1, y0, x1, y0+1, color, 1.f-y0_alpha);
	draw_rect_in_pixels_transparent(x0+1, y1, x1, y1+1, color, y1_alpha);

	draw_rect_in_pixels(x0+1, y0+1, x1, y1, color);
}

////////////
// Text

#define TEXT_HEIGHT 7
int letter_spacings[] = {4, 4, 4, 4, 4, 4, 4, 4, 3, 4, 4, 4, 5, 5, 4, 4, 5, 4, 4, 3, 4, 5, 5, 5, 5, 4, 1, 4};
char *letter_table[][TEXT_HEIGHT] = {
	" 00",
	"0  0",
	"0  0",
	"0000",
	"0  0",
	"0  0",
	"0  0",

	"000",
	"0  0",
	"0  0",
	"000",
	"0  0",
	"0  0",
	"000",

	" 000",
	"0",
	"0",
	"0",
	"0",
	"0",
	" 000",

	"000",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"000",

	"0000",
	"0",
	"0",
	"000",
	"0",
	"0",
	"0000",

	"0000",
	"0",
	"0",
	"000",
	"0",
	"0",
	"0",

	" 000",
	"0",
	"0",
	"0 00",
	"0  0",
	"0  0",
	" 000",

	"0  0",
	"0  0",
	"0  0",
	"0000",
	"0  0",
	"0  0",
	"0  0",

	"000",
	" 0",
	" 0",
	" 0",
	" 0",
	" 0",
	"000",

	" 000",
	"   0",
	"   0",
	"   0",
	"0  0",
	"0  0",
	" 000",

	"0  0",
	"0  0",
	"0 0",
	"00",
	"0 0",
	"0  0",
	"0  0",

	"0",
	"0",
	"0",
	"0",
	"0",
	"0",
	"0000",

	"00 00",
	"0 0 0",
	"0 0 0",
	"0   0",
	"0   0",
	"0   0",
	"0   0",

	"00  0",
	"0 0 0",
	"0 0 0",
	"0 0 0",
	"0 0 0",
	"0 0 0",
	"0  00",

	"0000",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0000",

	" 000",
	"0  0",
	"0  0",
	"000",
	"0",
	"0",
	"0",

	" 000 ",
	"0   0",
	"0   0",
	"0   0",
	"0 0 0",
	"0  0 ",
	" 00 0",

	"000",
	"0  0",
	"0  0",
	"000",
	"0  0",
	"0  0",
	"0  0",

	" 000",
	"0",
	"0 ",
	" 00",
	"   0",
	"   0",
	"000 ",

	"000",
	" 0",
	" 0",
	" 0",
	" 0",
	" 0",
	" 0",

	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	"0  0",
	" 00",

	"0   0",
	"0   0",
	"0   0",
	"0   0",
	"0   0",
	" 0 0",
	"  0",

	"0   0 ",
	"0   0",
	"0   0",
	"0 0 0",
	"0 0 0",
	"0 0 0",
	" 0 0 ",

	"0   0",
	"0   0",
	" 0 0",
	"  0",
	" 0 0",
	"0   0",
	"0   0",

	"0   0",
	"0   0",
	" 0 0",
	"  0",
	"  0",
	"  0",
	"  0",

	"0000",
	"   0",
	"  0",
	" 0",
	"0",
	"0",
	"0000",

	"",
	"",
	"",
	"",
	"",
	"",
	"0",

	"   0",
	"  0",
	"  0",
	" 0",
	" 0",
	"0",
	"0",


};

int
get_letter_index(char c) {
	// @New: Automatically apply uppercase
	// Old behaviour: Program would segfault if letter was lowercase due to out of bounds array access in letter_index

	if(c >= 'a' && c <= 'z') c -= 32; 
	if (c == '.') return 'Z'-'A'+1;
	if (c == '/') return 'Z'-'A'+2;
	return c-'A';
}

enum {
	TEXT_ALIGN_LEFT,
	TEXT_ALIGN_RIGHT,
	TEXT_ALIGN_CENTER,
};

// This is not 100%
internal f32
get_word_align_offset(int text_align, char *word, f32 size) {
	assert(*word);
	if (text_align == TEXT_ALIGN_LEFT) return 0.f;

	f32 result = 0.f;
	f32 block_offset_x = size*1.6f*2.f + size*.8f; //@Copy'n'Paste

	for (char *at = word; *at && *at != '\\'; at++) {
		if (*at == ' ') {
			result += block_offset_x*4 + size*2.f;
			continue;
		}

		int letter_index = get_letter_index(*at);
		result += block_offset_x*letter_spacings[letter_index] + size*2.f;
	}

	result -=  size*2.f;

	if (text_align == TEXT_ALIGN_RIGHT) return -result;
	else if (text_align == TEXT_ALIGN_CENTER) return -result*.5f;

	invalid_code_path;
	return 0.f;
}


internal void
draw_text(char *text, v2 p, f32 size, u32 *colors, int color_count, int first_color, int text_align) {

	int next_color = first_color;
	u32 color = colors[next_color];

	f32 first_x = p.x;
	p.x += get_word_align_offset(text_align, text, size);

	f32 original_x = p.x;
	f32 original_y = p.y;
	v2 half_size = {size*1.6f, size};
	f32 block_offset_x = size*1.6f*2.f + size*.8f; //@Copy'n'Paste


	for (char *letter_at = text; *letter_at; letter_at++) {
		if (*letter_at == ' ') {
			p.x += block_offset_x*4 + size*2.f;
			original_x = p.x;
			continue;
		
		}
		else if (*letter_at >= '0' && *letter_at <= '9') {
			// @Hack: We ignore numbers for now
			continue;
		}
		else if (*letter_at == '\\') {
			p.x = first_x;
			p.x += get_word_align_offset(text_align, letter_at+1, size);
			original_x = p.x;
			p.y -= size*(TEXT_HEIGHT+2)*2.5f;
			original_y = p.y;
			continue;
		}

		int letter_index = get_letter_index(*letter_at);
		char **letter = &letter_table[letter_index][0];


		for (int i = 0; i < TEXT_HEIGHT; i++) {
			char *at = letter[i];
			while(*at) {
				if (*at++ != ' ') {
					draw_rect(p, half_size, color);
				}
				p.x += block_offset_x;
			}
			p.y -= size*2.f + size*.4f;
			if (i != TEXT_HEIGHT-1) p.x = original_x;

			if (++next_color >= color_count) next_color = 0;
			color = colors[next_color];
		}

		p.x = original_x;
		p.y = original_y;
		p.x += block_offset_x*letter_spacings[letter_index] + size*2.f;
		original_x = p.x;

		next_color = first_color;
		color = colors[next_color];
	}
}
