typedef struct
{
	char buf[64];
	f32 life;
} con_msg;

con_msg Con_Messages[4] = {0};
int Con_messagesUsed = 0;

void Con_Print(const char *s, ...)
{
	if(Con_messagesUsed >= array_count(Con_Messages)) Con_messagesUsed = 0;

	con_msg *Msg = &Con_Messages[Con_messagesUsed++];
	va_list vl;
	va_start(vl, s);
	int written = vsnprintf(Msg->buf, array_count(Msg->buf) - 1, s, vl);
	Msg->buf[written] = 0;
	va_end(vl);

	Msg->life = 3.0f;
}

void Con_DisplayMessages(f32 dt)
{
	int yBegin = 44;
	int dyAdvance = 4;
	// int yEnd = yBegin + (dyAdvance * Con_messagesUsed);
	int dy = yBegin;

	u32 color = 0xFFFFFFFF;
	for(int i = 0; i < array_count(Con_Messages); i++)
	{
		con_msg *Msg = &Con_Messages[i];

		if(Msg->life > 0.0f)
		{
			draw_text(Msg->buf, (v2){-84, dy}, .115f, &color, 1, 0, TEXT_ALIGN_LEFT);
			Msg->life -= dt;
			dy -= dyAdvance;
			
			if(Msg->life <= 0)
			{
				Con_messagesUsed = max(Con_messagesUsed -1, 0);
				Msg->life = 0;
			}
		}
	}
}

#if DEVELOPMENT

enum {
    MESSAGE_INT,
    MESSAGE_F32,
    MESSAGE_V2I,
    MESSAGE_V2,
} typedef Message_Kind;

struct {
    Message_Kind kind;

    union {
        int int_val;
        f32 f32_val;
        v2i v2i_val;
        v2 v2_val;
    };

    f32 timer;
} typedef Message;

Message messages[32];
int current_message;

internal void
print_int(int number) {
    Message *message = messages + current_message++;
    if (current_message >= array_count(messages)) current_message = 0;

    message->kind = MESSAGE_INT;
    message->int_val = number;
    message->timer = 2.f;
}

internal void
print_f32(f32 number) {
    Message *message = messages + current_message++;
    if (current_message >= array_count(messages)) current_message = 0;

    message->kind = MESSAGE_F32;
    message->f32_val = number;
    message->timer = 2.f;
}


internal void
print_v2i(v2i number) {
    Message *message = messages + current_message++;
    if (current_message >= array_count(messages)) current_message = 0;

    message->kind = MESSAGE_V2I;
    message->v2i_val = number;
    message->timer = 2.f;
}


internal void
print_v2(v2 number) {
    Message *message = messages + current_message++;
    if (current_message >= array_count(messages)) current_message = 0;

    message->kind = MESSAGE_V2;
    message->v2_val = number;
    message->timer = 2.f;
}


internal void
draw_messages(f32 dt) {
    f32 original_x = -76;
    v2 p = {original_x, 40};

    for (int i = 0 ; i < array_count(messages); i++) {
        Message *message = messages + i;
        if (message->timer <= 0.f) continue;

        message-> timer -= dt;
        switch(message->kind) {
            case MESSAGE_INT: {
                draw_number(message->int_val, p, 2.5f, 0xffffff, 1, false);
            } break;

            case MESSAGE_F32: {
                draw_f32(message->f32_val, p, 2.5f, 0xffffff);
            } break;

            case MESSAGE_V2I: {
                draw_number(message->v2i_val.x, p, 2.5f, 0xffffff, 1, false);
                draw_number(message->v2i_val.y, add_v2(p, (v2){10, 0}), 2.5f, 0xffffff, 1, false);
            } break;

            case MESSAGE_V2: {
                draw_f32(message->v2_val.x, p, 2.5f, 0xffffff);
                draw_f32(message->v2_val.y, add_v2(p, (v2){10, 0}), 2.5f, 0xffffff);
            } break;

            invalid_default_case;
        }

        p.x = original_x;
        p.y -= 3.f;
    }
}

#else

#define draw_messages(...)
#define print_int(...)
#define print_f32(...)

#endif
