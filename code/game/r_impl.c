/* 
   Contains function pointer signature declarations for draw calls used in the game
   This file was written to facilitate switching between renderers at runtime

   As of writing, we have implemented an OpenGL hardware renderer, an SDL renderer, and the original
   software renderer

   The OpenGL and SDL platform layers can switch between their respective renderers and the
   software renderer

   The X11 platform layer defaults to using the software renderer exclusively
*/

#define F_RECT_OPAQUE(name)				void name(int x0, int y0, int x1, int y1, u32 color)
#define F_RECT_TRANSPARENT(name) 		void name(int x0, int y0, int x1, int y1, u32 color, f32 alpha)


#define F_RECT_OPAQUE_ROT(name) 		void name(v2 p, v2 half_size, f32 angle, u32 color)
#define F_RECT_TRANSPARENT_ROT(name) 	void name(v2 p, v2 half_size, f32 angle, u32 color, f32 alpha)


#define F_CLEAR_SCREEN(name) 			void name(const u32 color)
#define F_CLEAR_ARENA(name) 			void name(v2 p, f32 left_most, f32 right_most, f32 half_size_y, u32 color)

#define F_BITMAP(name) 					void name(Bitmap *bitmap, v2 p, v2 half_size, f32 alpha_multiplier) 

// Follows the convetion of pfn_<name> as a function pointer type
// Example: typedef void (*pfn_clear_screen)(u32 color);

#define FP_DECLARE(name, sig) typedef sig((*pfn_##name)); \
	pfn_##name name

FP_DECLARE(clear_arena_screen, F_CLEAR_ARENA);
FP_DECLARE(clear_screen, F_CLEAR_SCREEN);

FP_DECLARE(draw_rect_in_pixels, F_RECT_OPAQUE);
FP_DECLARE(draw_rect_in_pixels_transparent, F_RECT_TRANSPARENT);

FP_DECLARE(draw_rotated_rect, F_RECT_OPAQUE_ROT);
FP_DECLARE(draw_transparent_rotated_rect, F_RECT_TRANSPARENT_ROT);

FP_DECLARE(draw_bitmap, F_BITMAP);

typedef enum
{
	R_SW,
	R_HW,
} renderer_type;
