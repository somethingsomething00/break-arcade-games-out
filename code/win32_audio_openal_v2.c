// NAME: win32_audio_openal_v2.c
// TIME OF CREATION: 2022-05-04 17:41:09
// AUTHOR:
// DESCRIPTION: OpenAL implementation for Windows audio
// 				Uses OpenAL32.dll, and links with OpenAL32.lib.
//
// 				Uses stb_vorbis to decode OGG files.

// 				Linux and Windows audio code is now unified when using OpenAL




#ifndef _WIN32_AUDIO_OPENAL_V2_C_
#define _WIN32_AUDIO_OPENAL_V2_C_

#include "common_audio_openal.c"

#endif /* _WIN32_AUDIO_OPENAL_V2_C_ */
