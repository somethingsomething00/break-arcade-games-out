#ifndef _WINDOWS_PLATFORM_PROFILER_C_
#define _WINDOWS_PLATFORM_PROFILER_C_

static LARGE_INTEGER platform_GlobalPerformanceFrequency = {0};

double platform_GetTimerFrequency()
{
	// Note: We cache the result of QueryPerformanceFrequency
	// This avoids a divide by zero bug, however writing the function this way probably prevents the compiler from inlining this call
	static int isInitialized = 0;
	if(!isInitialized)
	{
        QueryPerformanceFrequency(&platform_GlobalPerformanceFrequency);
		isInitialized = 1;
	}
	return platform_GlobalPerformanceFrequency.QuadPart;
}

u64 platform_GetTime()
{
	LARGE_INTEGER result;
	QueryPerformanceCounter(&result);
	return result.QuadPart;
}


// We include intrin.h in win32_platform_profiler.h to access rdtsc
u64 GetCycles()
{
	return __rdtsc();
}

#endif /* _WINDOWS_PLATFORM_PROFILER_C_ */
