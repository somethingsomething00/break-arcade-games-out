#ifndef _JSYS_H_
#define _JSYS_H_

// We will allow up to 64 threads (if the host machine has this many)
// We detect the available thread count at runtime and adjust accordingly
#define JSYS_NUM_POSSIBLE_THREADS 64

typedef struct
{
	void *data1;
	void *data2;
	void *data3;
	void *data4;
} job_info;

#define JSYS_JOB_FUNC(funcname) void funcname(job_info JobInfo)
typedef JSYS_JOB_FUNC((pJobFunc));

typedef struct
{
	job_info JobInfo;
	pJobFunc *Proc;
} queue_info;

typedef struct queue_node queue_node;
struct queue_node
{
	queue_info *QueueInfo;
	queue_node *Next;
};


/**********************************
* Public interface
**********************************/
void jsys_Init();
void jsys_AddJob(job_info JobInfo, pJobFunc *JobProc);
void jsys_ThreadSync();



#endif /* _JSYS_H_ */
