// This file is now shared between Linux and Windows builds

// @Temp To get around SDL includes
#define USE_SDL_SOUND 1

#define PLATFORM_BYTES_PER_PIXEL 4

#define PLATFORM_PREFERRED_PIXEL_FORMAT \
SDL_PIXELFORMAT_BGRA32

#define ArrayCount(arr_) \
(sizeof(arr_) / sizeof(arr_)[0])


/**********************************
* Config
**********************************/
#define SDL_USE_RENDERER 1
// Must be defined as 1 to include the SDL2 renderer in r_renderer.c

#include "config.h"


// Original game code with slight modifications
//------------------------------------------------------------------------------
#include "game.h"
#include "utils.c"
#include "maths.c"
#include "string.c"
#include "platform_common.c"


// Globals that were not predeclared anywhere as extern
global_variable Render_Buffer render_buffer;
global_variable f32 current_time;
global_variable b32 lock_fps = true;

// Compiler specific intrinsic
// Unused on linux
#ifdef _WIN32
#define interlocked_compare_exchange(a, b, c) InterlockedCompareExchange((volatile long*)a, b, c)
#endif

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#define STBI_NO_STDIO
#define STBI_NO_FAILURE_STRINGS
#define STBI_ASSERT assert
#include "stb_image.h"

// Use x86 intrinsics for certain routines
#include "common_intrinsics.c"

// Job system for OGG loading (it is general purpose though)
#include "common_jsys.h"
#include "common_jsys.c"

#include "cooker_common.c"
#include "ogg_importer.h"
#include "common_audio.c"
#include "asset_loader.c"
#include "profiler.c"
#include "r_renderer.c"
#include "console.c"
#include "game.c"
//------------------------------------------------------------------------------


// Begin platform code (SDL2)
//--------------------------------------------------------------------------------
/**********************************
* Platfrom macros
**********************************/
#define Dowhile(body) do {body} while(0)

#undef Assert
#define Assert(cond) \
Dowhile(if(!(cond)) {fprintf(stderr, "%s:%d: Assertion failed for expression '%s'\n", __FILE__, __LINE__, #cond); exit(1);})

#define Pretty(...) \
Dowhile(printf("%s:%d: ", __FILE__, __LINE__); printf(__VA_ARGS__);)

#define PrettyError(...) \
Dowhile(printf("%s:%d: ", __FILE__, __LINE__); printf("\033[31m"); printf(__VA_ARGS__); printf("\033[0m");)


// Size for the game framebuffer, which it will draw to
// Should be 16:9 optimally, as this is what the game expects

// From testing, it looks like if we create an SDL renderer that happens to implicitly use the OpenGL backend,
// it uses glTexImage2D to copy the framebuffer. 
// Being a prohibitively slow function, we keep these sizes at 1x for the software side
// even though the software renderer itself can handle more.
// Maybe we should've went with SDL_Surface?
#define FB_WIDTH_SW (1280 * 1)
#define FB_HEIGHT_SW (720 * 1)

#define FB_WIDTH_HW (1280 * 1)
#define FB_HEIGHT_HW (720 * 1)


#define PLATFORM_WINDOW_CREATION_FLAGS \
(SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_ALLOW_HIGHDPI)

#define PLATFORM_RENDERER_CREATION_FLAGS \
(SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE)


/**********************************
* Platform includes
**********************************/
#include "common_sdl2_platform.h"
#include "platform_profiler.h"
#include "platform_profiler.c"


/**********************************
* Platform globals
**********************************/
#define PLATFORM_POLL_EVENTS 0x1
#define PLATFORM_WAIT_EVENTS 0x2
typedef int platform_EventProcessMask;

// Display stuff
static SDL_Window *sdl_Window;
static SDL_DisplayMode sdl_DisplayMode;
SDL_Renderer *sdl_Renderer;
static SDL_Texture *sdl_HardwareBuffer;
static SDL_Texture *sdl_SoftwareBuffer;
static SDL_Texture *sdl_RenderTarget;
renderer_type sdl_RendererType;

static u32 sdl_Windowmode[] =
{
	[PLATFORM_WINDOWED] = 0, // This minimizes the game on traditional desktops
	[PLATFORM_FULLSCREEN] = SDL_WINDOW_FULLSCREEN_DESKTOP,
};
static u32 sdl_FullscreenModeIndex;
static u32 sdl_FirstFullscreenToggle = 1;

// Input
static Input sdl_Input;
static mouse sdl_Mouse;

// Timing
static u64 sdl_GlobalPerformanceFrequency;

// Bools
static b32 sdl_TimeToQuit;
static b32 sdl_IsPaused;

// Keysym
keys sdl_Keys = {0};


/**********************************
* Platform functions
**********************************/
// Capture errors returned by sdl
// At the moment we crash on any error
// scp_ -> pointer errors
// scc_ -> int code errors
void *scp_(void *ptr, const char *file, int line, const char *expression);
int scc_(int code, const char *file, int line, const char *expression);
#if !SDL_NOCHECK
#define scp(expr) scp_((expr), __FILE__, __LINE__, #expr)
#define scc(expr) scc_((expr), __FILE__, __LINE__, #expr)
#else
#define scp(expr) (expr)
#define scc(expr) (expr)
#endif /* SDL_NOCHECK */


static void sdl_RenderBufferInit(Render_Buffer *Buffer, u32 width, u32 height, u32 bpp);
static void sdl_UpdateAndRender(Render_Buffer *Buffer, Input *Input, SDL_Renderer *Renderer, SDL_Texture *RenderTarget, f32 dt);
static void sdl_MouseCapture(SDL_Window *Window);
static void sdl_MouseRelease(SDL_Window *Window);
static void sdl_ProcessEvents(Input *Input, mouse *Mouse, keys *Keys, SDL_Event *Event);
static void sdl_GetAndProcessEvents(Input *Input, mouse *Mouse, keys *Keys, platform_EventProcessMask mask);
static void sdl_PauseGraphicDraw(SDL_Renderer *Renderer);
static void sdl_InputInit(Input *Input);
static void sdl_RendererInit();
static void sdl_ChangeRendererIfNecessary();
static void sdl_ToggleFullscreen();

// Our own keysym abstraction to handle single button presses
static void sdl_ProcessKeysm(keys *Keys, Input *Input);
static b32 sdl_KeysymIsDown(const keys *Keys, const SDL_KeyCode Code);
static b32 sdl_KeysymIsUp(const keys *Keys, const SDL_KeyCode Code);
static b32 sdl_KeysymIsHeld(const keys *Keys, const SDL_KeyCode Code);
static void sdl_KeysymSetToHeld(keys *Keys, const SDL_KeyCode Code);
static void sdl_ProcessGameButtonAsSinglePress(keys *Keys, Input *Input, SDL_KeyCode SdlButton, game_button GameButton);
static void sdl_ProcessGameButtonAsHeld(keys *Keys, Input *Input, SDL_KeyCode SdlButton, game_button GameButton);

static u64 platform_GetTicks();
static f32 platform_TicksToSeconds(u64 ticks);
static void *platform_Alloc(u64 bytes);
static void platform_Free(void *ptr);


int main(int argc, char **argv)
{
	const char *videoDriver;
	
	// Note: Required to be initialized to 0 or strange bugs may ensue
	// The game behaves unpredictably if the elapsed time is too large
	f32 elapsedTimeInSeconds = 0;
	
	// Note: t1 and t2 variables are required to be initialized to 0
	u64 t1 = 0;
	u64 t2 = 0;
	
	// Note: 'Ticks' variables are required to be initialized to 0
	u64 oldFrameTicks = 0;
	u64 currentFrameTicks = 0;
	
	s32 mouseXCurrent;
	s32 mouseXOld;
	s32 mouseYCurrent;
	s32 mouseYOld;
	b32 firstMouse = true;
	
	v2i sdl_MouseDp;
	
	SDL_version compiled;
	SDL_version linked;
	
	// Sanity check before we begin
	Assert(sizeof *render_buffer.pixels == PLATFORM_BYTES_PER_PIXEL);
	
	jsys_Init();
	snd_Init();
	
	scc(SDL_Init(SDL_INIT_VIDEO));
	
	SDL_VERSION(&compiled);
	SDL_GetVersion(&linked);
	printf("SDL Version %d.%d.%d (compiled)\n", compiled.major, compiled.minor, compiled.patch);
	printf("SDL Version %d.%d.%d (linked)\n", linked.major, linked.minor, linked.patch);
	
	// This is strange, we first create a 0 size window and only then find out the video mode of the platform
	sdl_Window = scp(SDL_CreateWindow("Break Arcade Games Out [SDL]", 0, 0, 0, 0, PLATFORM_WINDOW_CREATION_FLAGS));
	scc(SDL_GetDisplayMode(0, 0, &sdl_DisplayMode));
	Pretty("Created window: %dx%d\n", sdl_DisplayMode.w, sdl_DisplayMode.h);
	
	sdl_Renderer = scp(SDL_CreateRenderer(sdl_Window, -1, PLATFORM_RENDERER_CREATION_FLAGS));
	SDL_SetRenderDrawBlendMode(sdl_Renderer, SDL_BLENDMODE_BLEND);
	
	
	sdl_SoftwareBuffer = scp(SDL_CreateTexture(sdl_Renderer, PLATFORM_PREFERRED_PIXEL_FORMAT, SDL_TEXTUREACCESS_STREAMING, FB_WIDTH_SW, FB_HEIGHT_SW));
	sdl_HardwareBuffer = scp(SDL_CreateTexture(sdl_Renderer, PLATFORM_PREFERRED_PIXEL_FORMAT, SDL_TEXTUREACCESS_TARGET, FB_WIDTH_HW, FB_HEIGHT_HW));
	
	Pretty("Created software texture: %dx%d, %d BPP\n", FB_WIDTH_SW, FB_HEIGHT_SW, PLATFORM_BYTES_PER_PIXEL);
	Pretty("Created hardware texture: %dx%d, %d BPP\n", FB_WIDTH_HW, FB_HEIGHT_HW, PLATFORM_BYTES_PER_PIXEL);
	
	SDL_SetWindowFullscreen(sdl_Window, SDL_WINDOW_FULLSCREEN_DESKTOP);
	sdl_FullscreenModeIndex = PLATFORM_FULLSCREEN;
	
	sdl_MouseCapture(sdl_Window);
	
	sdl_GlobalPerformanceFrequency = SDL_GetPerformanceFrequency();
	
	sdl_InputInit(&sdl_Input);
	
	// Note: y is required to be initalized to a positive integer
	sdl_MouseDp.x = 0;
	sdl_MouseDp.y = sdl_DisplayMode.w / 2;
	
	sdl_RenderBufferInit(&render_buffer, FB_WIDTH_SW, FB_HEIGHT_SW, PLATFORM_BYTES_PER_PIXEL);
	
	videoDriver = SDL_GetCurrentVideoDriver();
	if(videoDriver)
	{
		printf("SDL driver: %s\n", videoDriver);
	}
	
	R_Init();
	sdl_RendererType = R_HW;
	sdl_RendererInit();
	Assert(sdl_RenderTarget != NULL && "We failed to bind our render target!");
	
	/////////////////////////
	// Main loop
	/////////////////////////
	
	
	while(!sdl_TimeToQuit)
	{
		t1 = platform_GetTicks();
		
		sdl_GetAndProcessEvents(&sdl_Input, &sdl_Mouse, &sdl_Keys, PLATFORM_POLL_EVENTS);
		
		// Must be called `after` sdl_GetAndProcessEvents so we register the state change
		sdl_ChangeRendererIfNecessary(); 
		
		if(sdl_IsPaused)
		{
			sdl_MouseRelease(sdl_Window);
			
			while(sdl_IsPaused)
			{
				if(sdl_TimeToQuit) goto the_end;
				SDL_RenderClear(sdl_Renderer);
				scc(SDL_RenderCopyEx(sdl_Renderer, sdl_RenderTarget, NULL, NULL, 0, NULL, SDL_FLIP_VERTICAL));
				sdl_PauseGraphicDraw(sdl_Renderer);
				SDL_RenderPresent(sdl_Renderer);
				sdl_GetAndProcessEvents(&sdl_Input, &sdl_Mouse, &sdl_Keys, PLATFORM_WAIT_EVENTS);
			}
			sdl_MouseCapture(sdl_Window);
			SDL_RenderPresent(sdl_Renderer);
			continue;
		}
		
		// sdl_GetAndProcessEvents(&sdl_Input, &sdl_Mouse, &sdl_Keys, PLATFORM_POLL_EVENTS);
		mouseXCurrent = sdl_Mouse.xTotal;
		
		// @Hack to avoid mouse warping upon game launch
		if(firstMouse) firstMouse = false;
		else sdl_MouseDp.x = mouseXCurrent - mouseXOld;
		
		mouseXOld = mouseXCurrent;
		sdl_Input.mouse_dp = sdl_MouseDp;
		sdl_UpdateAndRender(&render_buffer, &sdl_Input, sdl_Renderer, sdl_RenderTarget, elapsedTimeInSeconds);
		t2 = platform_GetTicks();
		oldFrameTicks = currentFrameTicks;
		currentFrameTicks = t2 - t1;
		elapsedTimeInSeconds = platform_TicksToSeconds(oldFrameTicks);
	}
	
	the_end:
	SDL_DestroyWindow(sdl_Window);
	SDL_DestroyRenderer(sdl_Renderer);
	// Note: The SDL_DestoryRenderer implicitly frees the render_buffer.pixels since that's what we passed as the texture memory
	// From the manual: "Destroy the rendering context for a window and free associated textures."
	
	// SDL_Quit is commented out on purpose, due to it causing a delay on exiting the program
	// SDL_Quit();
	snd_Shutdown();
	return 0;
}

// os_* functions
// Used internally by the game to load and save files
//--------------------------------------------------------------------------------

static void os_free_file(String s)
{
	free(s.data);
}

static String os_read_entire_file(char *file_path)
{
	String S = {0};
	FILE *F = {0};
	s64 filesize;
	s64 bytesRead;
	F = fopen(file_path, "ab+");
	Pretty("os_read_entire_file: Opening file '%s'\n", file_path);
	Assert(F != NULL);
	fseek(F, 0, SEEK_END);
	filesize = ftell(F);
	Assert(filesize != -1);
	rewind(F);
	if(filesize > 0)
	{
		S.data = calloc(filesize, sizeof *S.data);
		Assert(S.data);
		bytesRead = fread(S.data, 1, filesize, F);
		Pretty("os_read_entire_file: %"PRIi64" bytes read for file '%s'\n", bytesRead, file_path);
	}
	S.size = filesize;
	Pretty("os_read_entire_file: %"PRIi64" bytes allocated for '%s'\n", filesize, file_path);
	fclose(F);
	return S;
}

static String os_read_save_file()
{
	return os_read_entire_file("data/save.brk");
}

static String os_read_config_file()
{
	Pretty("os_read_config_file: Preparing to open config file '%s'\n", "config.txt");
	return os_read_entire_file("config.txt");
}


#define save_file_error \
Dowhile(PrettyError("os_read_save_file: Unable to write to save file! The game will still function but progress will not be saved\n"); \
goto save_end;)
static b32 os_write_save_file(String data)
{
	// Note on file saves: The game modifies the save contents during runtime (in a buffer)
	// When writing an updated save buffer we want to create a new file with the modified contents,
	// NOT append to the existing file (if one exists)
	
	FILE *TempSave = {0}; /* Write the save data to a temp file first */
	int rc;
	s64 bytesWritten;
	b32 result = true; /* We assume a successful write */
	const char *savefileName = "data/save.brk";
	const char *tempfileName = "data/save.brk.tmp";
	const char *colormodeSet = "\033[0;31m"; // @Hardcoded red color VT escape
	const char *boldSet = "\033[1m";
	TempSave =  fopen(tempfileName, "wb+");
	if(TempSave)
	{
		bytesWritten = fwrite(data.data, 1, data.size, TempSave);
		if(bytesWritten != data.size)
		{
			fclose(TempSave);
			save_file_error;
		}
	}
	else
	{
		save_file_error;
	}
	fclose(TempSave);
#ifndef _WIN32
	rc = rename(tempfileName, savefileName);
#else
	if(!MoveFileEx(tempfileName, savefileName, MOVEFILE_REPLACE_EXISTING))
	{
		rc = GetLastError();
	}
#endif /* !_WIN32 */
	if(rc == 0)
	{
		Pretty("os_write_save_file: %"PRIi64" bytes written to '%s'\n", bytesWritten, savefileName);
	}
	else
	{
		result = false;
		perror("rename");
		Pretty("%sUnable to rename temporary save file\n"
			   "The game was successfully saved under the name %s'%s'%s\n"
			   "Please navigate to its path and rename the file to %s'%s'%s to access your save file on the next launch of the game\n",
			   colormodeSet,
			   boldSet,tempfileName,colormodeSet,
			   boldSet,savefileName,colormodeSet);
	}
	save_end:
	return result;
}

String os_get_pak_data()
{
	return os_read_entire_file("data/assets.pak");
}

static void os_toggle_fullscreen()
{
	// Used in config_file_parser.c
	// Despite the function name it only gets called if the windowed_mode string was found in the config
	// Therefore we set this to windowed mode every time regardless
	
	// Note: This is not a typo!
	sdl_FullscreenModeIndex = PLATFORM_FULLSCREEN;
	sdl_ToggleFullscreen();
}

f32 os_seconds_elapsed(u64 last_counter)
{
	u64 tNow;
	tNow = platform_GetTicks();
	return platform_TicksToSeconds((tNow - last_counter));
}

u64 os_get_perf_counter()
{
	return  platform_GetTicks();
}

// Thanks to tsoding for this idea
int scc_(int code, const char *file, int line, const char *expression)
{
	if(code != 0)
	{
		fprintf(stderr, "%s:%d: Int call failed for expression '%s': %s\n", file, line,  expression, SDL_GetError());
		exit(1);
	}
	return code;
}

void *scp_(void *ptr, const char *file, int line, const char *expression)
{
	if(!ptr)
	{
		fprintf(stderr, "%s:%d: Pointer call failed for expression '%s': %s\n", file, line, expression, SDL_GetError());
		exit(1);
	}
	return ptr;
}

// sdl_* functions
//--------------------------------------------------------------------------------

void sdl_RenderBufferInit(Render_Buffer *Buffer, u32 width, u32 height, u32 bpp)
{
	// Note: We request memory for this buffer from SDL_LockTexture
	Buffer->pixels = NULL;
	Buffer->width = width;
	Buffer->height = height;
	Pretty("sdl_RenderBufferInit: Initialized framebuffer %ux%u, %u\n", width, height, bpp);
}

void sdl_UpdateAndRender(Render_Buffer *Buffer, Input *Input, SDL_Renderer *Renderer, SDL_Texture *RenderTarget, f32 dt)
{
	if(sdl_RendererType == R_SW)
	{
		// Note: Lock texture from SDL itself and reserve it for our use
		int pitch;
		scc(SDL_LockTexture(RenderTarget, NULL, (void **)&Buffer->pixels, &pitch));
		Assert(pitch == Buffer->width * PLATFORM_BYTES_PER_PIXEL);
	}
	else
	{
		scc(SDL_SetRenderTarget(Renderer, RenderTarget));
	}
	
	begin_profiler();
	
	// Core update function provided by the game
	/**********************/
	platform_TimedBlockBegin(TB_GAME);
	update_game(Input, dt);
	snd_Update();
	platform_TimedBlockEnd(TB_GAME);
	/**********************/
	
	render_profiler(dt);
	
	if(sdl_RendererType == R_SW)
	{
		// Note: Allow SDL to access texture data for its internal use
		platform_TimedBlockBegin(TB_MEMCPY);
		SDL_UnlockTexture(RenderTarget);
		platform_TimedBlockEnd(TB_MEMCPY);
	}
	else
	{
		scc(SDL_SetRenderTarget(Renderer, NULL));
	}
	
	scc(SDL_RenderCopyEx(sdl_Renderer, RenderTarget, NULL, NULL, 0, NULL, SDL_FLIP_VERTICAL));
	
	SDL_RenderPresent(Renderer);
}

u64 platform_GetTicks()
{
	return SDL_GetPerformanceCounter();
}

f32 platform_TicksToSeconds(u64 ticks)
{
	return (f32)ticks / (f32)sdl_GlobalPerformanceFrequency;
}

void sdl_MouseCapture(SDL_Window *Window)
{
	SDL_ShowCursor(SDL_FALSE);
	SDL_SetWindowGrab(Window, SDL_TRUE);
}

void sdl_MouseRelease(SDL_Window *Window)
{
	SDL_ShowCursor(SDL_TRUE);
	SDL_SetWindowGrab(Window, SDL_FALSE);
}

void sdl_ToggleFullscreen()
{
	sdl_FullscreenModeIndex  = !sdl_FullscreenModeIndex;
	if(sdl_FirstFullscreenToggle)
	{
		// Setting a fullscreen window to windowed for the first time will make its viewport tiny
		// We get around this undesirable effect with this bit logic.
		sdl_FirstFullscreenToggle = 0;
		if(sdl_FullscreenModeIndex == PLATFORM_WINDOWED)
		{
			SDL_DisplayMode DisplayMode = {0};
			SDL_GetWindowDisplayMode(sdl_Window, &DisplayMode);
			
			int w = DisplayMode.w / 2;
			int h = DisplayMode.h / 2;
			
			SDL_SetWindowFullscreen(sdl_Window, sdl_Windowmode[sdl_FullscreenModeIndex]);
			SDL_SetWindowSize(sdl_Window, w, h);
			SDL_SetWindowPosition(sdl_Window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
		}
		
	}
	else
	{
		SDL_SetWindowFullscreen(sdl_Window, sdl_Windowmode[sdl_FullscreenModeIndex]);
	}
}

void sdl_GetAndProcessEvents(Input *Input, mouse *Mouse, keys *Keys, platform_EventProcessMask mask)
{
	// PLATFORM_POLL_EVENTS and PLATFORM_WAIT_EVENTS paths are the same for now,
	// until I can figure out how to block events in SDL without stalling the event queue
	
	// sdl_ProcessKeysm is outside of the PollEvent loop for now, there are some bugs regarding reloading the config
	
	SDL_Event Event;
	if(mask & PLATFORM_POLL_EVENTS)
	{
		while(SDL_PollEvent(&Event))
		{
			sdl_ProcessEvents(Input, Mouse, Keys, &Event);
		}
		sdl_ProcessKeysm(Keys, Input);
	}
	else if(mask & PLATFORM_WAIT_EVENTS)
	{
		while(SDL_PollEvent(&Event))
		{
			sdl_ProcessEvents(Input, Mouse, Keys, &Event);
		}
		sdl_ProcessKeysm(Keys, Input);
	}
}


b32 sdl_KeysymIsDown(const keys *Keys, const SDL_KeyCode Code)
{
	return Keys->Key[SDL_GetScancodeFromKey(Code)].isDown;
	
}

b32 sdl_KeysymIsHeld(const keys *Keys, const SDL_KeyCode Code)
{
	return Keys->Key[SDL_GetScancodeFromKey(Code)].isHeld;
	
}

b32 sdl_KeysymIsUp(const keys *Keys, const SDL_KeyCode Code)
{
	b32 a;
	b32 b;
	a = sdl_KeysymIsDown(Keys, Code);
	b = sdl_KeysymIsHeld(Keys, Code);
	return (!a && !b);
}

void sdl_KeysymSetToHeld(keys *Keys, const SDL_KeyCode Code)
{
	Keys->Key[SDL_GetScancodeFromKey(Code)].isDown = false;
	Keys->Key[SDL_GetScancodeFromKey(Code)].isHeld = true;
}

void sdl_ProcessGameButtonAsSinglePress(keys *Keys, Input *Input, SDL_KeyCode SdlButton, game_button GameButton)
{
	if(sdl_KeysymIsDown(Keys, SdlButton))
	{
		sdl_KeysymSetToHeld(Keys, SdlButton);
		Input->buttons[GameButton].changed = true;
		Input->buttons[GameButton].is_down = true;
		
	}
	else if(sdl_KeysymIsHeld(Keys, SdlButton) || sdl_KeysymIsUp(Keys, SdlButton))
	{
		Input->buttons[GameButton].changed = true;
		Input->buttons[GameButton].is_down = false;
	}
}

void sdl_ProcessGameButtonAsHeld(keys *Keys, Input *Input, SDL_KeyCode SdlButton, game_button GameButton)
{
	if(sdl_KeysymIsDown(Keys, SdlButton) || sdl_KeysymIsHeld(Keys, SdlButton))
	{
		sdl_KeysymSetToHeld(Keys, SdlButton);
		Input->buttons[GameButton].changed = true;
		Input->buttons[GameButton].is_down = true;
	}
	else if(sdl_KeysymIsUp(Keys, SdlButton))
	{
		Input->buttons[GameButton].changed = true;
		Input->buttons[GameButton].is_down = false;
	}
}


// SDL doesn't natively support polling for a single keypress per-frame
// This function allows us to do this by mapping sdl scancodes (not keycodes!)
// to an array and querying their state
// The array is set initially in sdl_ProcessEvents
void sdl_ProcessKeysm(keys *Keys, Input *Input)
{
	// Platform controls
	// Pause
	if(sdl_KeysymIsDown(Keys, SDLK_p))
	{
		sdl_IsPaused = !sdl_IsPaused;
		sdl_KeysymSetToHeld(Keys, SDLK_p);
	}
	if(sdl_KeysymIsDown(Keys, SDLK_SPACE))
	{
		sdl_IsPaused = !sdl_IsPaused;
		sdl_KeysymSetToHeld(Keys, SDLK_SPACE);
	}
	
	// Quit
	if(sdl_KeysymIsDown(Keys, SDLK_q) || sdl_KeysymIsHeld(Keys, SDLK_q))
	{
		sdl_TimeToQuit = 1;
		sdl_KeysymSetToHeld(Keys, SDLK_q);
	}
	
	// Fullscreen
	if(sdl_KeysymIsDown(Keys, SDLK_f))
	{
		sdl_ToggleFullscreen();
		sdl_KeysymSetToHeld(Keys, SDLK_f);
	}
	
	// Clear the platform profiler
	if(sdl_KeysymIsDown(Keys, SDLK_r))
	{
		platform_TimedBlockClearAll();
		sdl_KeysymSetToHeld(Keys, SDLK_r);
	}
	
	// Print out the platform profiler in its current state
	if(sdl_KeysymIsDown(Keys, SDLK_w))
	{
		platform_TimedBlockList();
		sdl_KeysymSetToHeld(Keys, SDLK_w);
	}
	
	// Change between renderers
	if(sdl_KeysymIsDown(Keys, SDLK_1))
	{
		sdl_KeysymSetToHeld(Keys, SDLK_1);
		sdl_RendererType = R_HW;
		
		Con_Print("SDL renderer");
	}
	
	if(sdl_KeysymIsDown(Keys, SDLK_2))
	{
		sdl_KeysymSetToHeld(Keys, SDLK_2);
		sdl_RendererType = R_SW;
		
		Con_Print("Software renderer");
	}
	
	
	
	// Game controls
	
	// These are controls that only occur once per keypress
	sdl_ProcessGameButtonAsSinglePress(Keys, Input, SDLK_l, BUTTON_CFG);
	sdl_ProcessGameButtonAsSinglePress(Keys, Input, SDLK_ESCAPE, BUTTON_ESC);
	sdl_ProcessGameButtonAsSinglePress(Keys, Input, SDLK_RIGHT, BUTTON_RIGHT);
	sdl_ProcessGameButtonAsSinglePress(Keys, Input, SDLK_LEFT, BUTTON_LEFT);
	
	// These are controls occur every frame while the key is held
	sdl_ProcessGameButtonAsHeld(Keys, Input, SDLK_DOWN, BUTTON_DOWN);
	sdl_ProcessGameButtonAsHeld(Keys, Input, SDLK_UP, BUTTON_UP);
}

#define	SDLKU(sdlkey, gamekey) \
case sdlkey: Input->buttons[gamekey].changed = true;\
Input->buttons[gamekey].is_down = false;\

#define	SDLKD(sdlkey, gamekey) \
case sdlkey: Input->buttons[gamekey].changed = true;\
Input->buttons[gamekey].is_down = true;\
break

// Sets the state of our internal keysym, and used for mouse movement
void sdl_ProcessEvents(Input *Input, mouse *Mouse, keys *Keys, SDL_Event *Event)
{
	switch(Event->type)
	{
		case SDL_QUIT:
		sdl_TimeToQuit = 1;
		break;
		
		// Keys
		case SDL_KEYDOWN:
		if(!Keys->Key[Event->key.keysym.scancode].isDown
		   && !Keys->Key[Event->key.keysym.scancode].isHeld)
		{
			Keys->Key[Event->key.keysym.scancode].isDown = true;
		}
		switch(Event->key.keysym.sym)
		{
			// The only key combos we let sdl handle because we don't handle modifiers
			case SDLK_F4:
			if(Event->key.keysym.mod & KMOD_ALT)
			{
				sdl_TimeToQuit = 1;
			}
			break;
			
			case SDLK_RETURN:
			case SDLK_F11:
			if(Event->key.keysym.sym == SDLK_RETURN) 
			{
				if(!(Event->key.keysym.mod & KMOD_ALT))
				{
					break;
				}
			}
			sdl_ToggleFullscreen();
			break;
		}
		break;
		
		case SDL_KEYUP:
		Keys->Key[Event->key.keysym.scancode].isDown = false;
		Keys->Key[Event->key.keysym.scancode].isHeld = false;
		break;
		
		// Mouse
		case SDL_MOUSEBUTTONDOWN:
		switch(Event->button.button)
		{
			SDLKD(SDL_BUTTON_LEFT, BUTTON_LMB);
		}
		break;
		
		case SDL_MOUSEBUTTONUP:
		switch(Event->button.button)
		{
			SDLKU(SDL_BUTTON_LEFT, BUTTON_LMB);
		}
		break;
		
		case SDL_MOUSEMOTION:
		{
			// Taken straight from the X11 code!
			// Set up infinite mouse scrolling
			// SDL_SetRelativeMouseMode was no good for this since it evaluates movement as a percentage of the total screen width
			// This means if the delta was relatively low, the mouse movement would be rounded to 0 for multiple frames
			// until enough movement was accumulated
			static int warped = 0;
			s32 x;
			s32 y;
			int w;
			int h;
			x = Event->motion.x;
			y = Event->motion.y;
			if(!warped)
			{
				Mouse->xOld = Mouse->xCur;
				Mouse->yOld = Mouse->yCur;
				Mouse->xCur = x;
				Mouse->yCur = y;
			}
			else
			{
				warped = 0;
			}
			if(!sdl_IsPaused)
			{
				Mouse->xTotal += (Mouse->xCur - Mouse->xOld);
				SDL_GetRendererOutputSize(sdl_Renderer, &w, &h);
				if(x == 0 || x >= w - 1)
				{
					s32 xTemp;
					xTemp = Mouse->xCur;
					Mouse->xCur = w / 2;
					SDL_WarpMouseInWindow(sdl_Window, w / 2, h / 2);
					Mouse->xOld = (xTemp - Mouse->xOld) + Mouse->xCur;
					warped = 1;
				}
			}
		}
		break;
	}
}
void sdl_PauseGraphicDraw(SDL_Renderer *Renderer)
{
	u32 x1, y1, x2, y2, w, h;
	u32 halfwidth;
	u32 halfheight;
	
	s32 renderWidth;
	s32 renderHeight;
	
	SDL_GetRendererOutputSize(Renderer, &renderWidth, &renderHeight);
	
	halfwidth = renderWidth / 2;
	halfheight = renderHeight / 2;
	w = (u32)((f32)renderWidth * 0.015f);
	h = (u32)((f32)renderHeight * 0.1f);
	
	x1 = halfwidth - (w + (w / 2));
	x2 = halfwidth + (w - (w /2));
	y1 = y2 = (halfheight - (h / 2));
	
	// Old draw routine, a bit boring really!
#if 1
	// x, y, w, h
	const SDL_Rect Rects[2] =
	{
		[0] = {x1, y1, w, h},
		[1] = {x2, y2, w, h},
	};
	
	// SDL_SetRenderDrawColor(Renderer, 160, 0, 0, 0xFF); // RGBA
	SDL_SetRenderDrawColor(Renderer, 120, 255, 255, 255);
	SDL_RenderFillRects(Renderer, Rects, 2);
#else
	// Lerp colour - just for fun
	float xBegin;
	float yBegin;
	float xEnd;
	float yEnd;
	float step = 0.25;
	float steps;
	int stepsInLoop;
	float percent;
	
	int r, g, b;
	
	int bBegin = 255;
	int bEnd = 255;
	
	int gBegin = 0;
	int gEnd = 255;
	
	int rBegin = 0;
	int rEnd = 255;
	
	int bRange;
	int gRange;
	int rRange;
	
	bRange = bEnd - bBegin;
	gRange = gEnd - gBegin;
	rRange = rEnd - rBegin;
	
	b = bBegin;
	g = gBegin;
	r = rBegin;
	
	yBegin = y1;
	yEnd = y1 + h;
	steps = (yEnd - yBegin) / step;
	
	for(yBegin = y1, stepsInLoop = 0; yBegin < yEnd; yBegin += step)
	{
		stepsInLoop++;
		// SDL_SetRenderDrawColor(Renderer, b, g, r, 255);
		SDL_SetRenderDrawColor(Renderer, r, g, b, 255);
		SDL_RenderDrawLineF(Renderer, x1, yBegin, x1 + w, yBegin);
		SDL_RenderDrawLineF(Renderer, x2, yBegin, x2 + w, yBegin);
		percent = (float)stepsInLoop / (float)steps;
		b = ((float)bRange * percent) + bBegin;
		r = ((float)rRange * percent) + rBegin;
		g = ((float)gRange * percent) + gBegin;
	}
#endif
	SDL_SetRenderDrawColor(Renderer, 0, 0, 0, 0xFF);
}


void sdl_InputInit(Input *Input)
{
	Input->mouse_dp.x = 0;
	Input->mouse_dp.y = 1;
	//Note: y cannot be 0! Ensure it is some positive value
}

void sdl_RendererInit()
{
	// From r_common.c
	// Forces recalculation of aspect multiplier
	buffer_was_changed = 1;
	
	clear_profiler();
	
	if(sdl_RendererType == R_HW)
	{
		sdl_RenderTarget = sdl_HardwareBuffer;
		
		render_buffer.width = FB_WIDTH_HW;
		render_buffer.height = FB_HEIGHT_HW;
		
		R_EnableHardwareRenderer();
	}
	else
	{
		sdl_RenderTarget = sdl_SoftwareBuffer;
		
		render_buffer.width = FB_WIDTH_SW;
		render_buffer.height = FB_HEIGHT_SW;
		
		R_EnableSoftwareRenderer();
	}
}

void sdl_ChangeRendererIfNecessary()
{
	static int LastRenderer;
	static int FirstCall = 1;
	
	if(FirstCall)
	{
		FirstCall = 0;
		LastRenderer = sdl_RendererType;
	}
	else
	{
		if(LastRenderer != sdl_RendererType)
		{
			sdl_RendererInit();
		}
		LastRenderer = sdl_RendererType;
	}
}

void *platform_Alloc(u64 bytes)
{
	void *memory = NULL;
	memory = calloc(1, bytes);
	return memory;
}

void platform_Free(void *ptr)
{
	free(ptr);
}

u32 platform_allocate_texture(u32 width, u32 height, void *memory)
{
	return R_AllocateTexture(width, height, memory);
}
