// NAME: common_jsys.c
// TIME OF CREATION: 2022-04-23 02:18:28
// AUTHOR:
// DESCRIPTION: Multithreaded job system

// Initialize the library by calling jsys_Init (must be called before using any other function!)
// Add jobs by calling jsys_AddJob
// Synchronize the main thread with remaining jobs by calling jsys_ThreadSync
// When no new jobs are found, the threads will remain suspended until the end of the program

// All other functions are meant to be internal to the library

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#ifndef _WIN32
#include <unistd.h>
#include <pthread.h>
#else
#include "win32_mini_pthread.c"
#endif /* !_WIN32 */

/**********************************
 * Globals
**********************************/
queue_node *jsys__Head = NULL;
queue_node *jsys__Tail = NULL;

pthread_mutex_t jsys__JobLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t jsys__SyncLock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t jsys__ThreadProcCond = PTHREAD_COND_INITIALIZER;
pthread_t jsys__Threads[JSYS_NUM_POSSIBLE_THREADS] = {0};

volatile int jsys__SyncStatus = 0;
pthread_barrier_t jsys__SyncBarrier;
volatile int jsys__IsInitialized = 0;

/**********************************
 * Implementation
**********************************/
void jsys_Enqueue(queue_info *Info)
{
	queue_node *NewNode = malloc(sizeof *NewNode);
	assert(NewNode);
	queue_info *NewQueueInfo = malloc(sizeof *NewQueueInfo);
	assert(NewQueueInfo);

	*NewQueueInfo = *Info;
	NewNode->QueueInfo = NewQueueInfo;
	if(jsys__Head == NULL)
	{
		jsys__Head = NewNode;
		jsys__Tail = NewNode;
	}
	else
	{
		jsys__Tail->Next = NewNode;
		jsys__Tail = NewNode;
	}
	NewNode->Next = NULL;
}

queue_info *jsys_Dequeue()
{
	queue_node *TempNode;
	queue_info *Result;

	if(!jsys__Head) return NULL;

	Result = jsys__Head->QueueInfo;

	TempNode = jsys__Head->Next;
	free(jsys__Head);
	jsys__Head = NULL;
	jsys__Head = TempNode;
	return Result;
}

void jsys_CheckInit()
{
	if(!jsys__IsInitialized)
	{
		fprintf(stderr, "jsys has not been initialized!. "
				"Please call jsys_Init() before using this library (status of jsys__IsInitialized: %d)\n",
				jsys__IsInitialized);
		exit(1);
	}
}

// This thread will never exit, but it will remain suspended by pthread_cond_wait if there are no more jobs left in the queue
void *ThreadProc(void *Data)
{
	queue_info *QueueInfo;
	for(;;)
	{
		pthread_mutex_lock(&jsys__JobLock);
		QueueInfo = jsys_Dequeue();

		if(!QueueInfo)
		{
			// Check for sync condition
			int local_SyncStatus;
			pthread_mutex_lock(&jsys__SyncLock);
			local_SyncStatus = jsys__SyncStatus;
			pthread_mutex_unlock(&jsys__SyncLock);
			if(local_SyncStatus)
			{
				pthread_mutex_unlock(&jsys__JobLock);
				pthread_barrier_wait(&jsys__SyncBarrier);
				continue;
			}
			pthread_cond_wait(&jsys__ThreadProcCond, &jsys__JobLock);
		}
		pthread_mutex_unlock(&jsys__JobLock);

		if(QueueInfo)
		{
			if(QueueInfo->Proc)
			{
				QueueInfo->Proc(QueueInfo->JobInfo);
				free((void *)QueueInfo);
				// Who should be responsible for freeing this memory?
			}
		}
	}
}

#ifdef _WIN32
int jsys_GetRuntimeThreadCount()
{
	SYSTEM_INFO SystemInfo;
	SystemInfo.dwNumberOfProcessors = 0;
	GetSystemInfo(&SystemInfo);
	return SystemInfo.dwNumberOfProcessors;
}

void jsys_InitInternal()
{
	pthread_mutex_init(&jsys__JobLock);
	pthread_mutex_init(&jsys__SyncLock);
	pthread_cond_init(&jsys__ThreadProcCond);
}

#else

int jsys_GetRuntimeThreadCount()
{
	return sysconf(_SC_NPROCESSORS_ONLN);
}

void jsys_InitInternal()
{
	return;
}
#endif /* _WIN32 */

/**********************************
 * Public interface implementation
 **********************************/
void jsys_Init()
{
	jsys_InitInternal();
	int i;
	int runtimeThreads = jsys_GetRuntimeThreadCount();
	// @Cleanup: Make a way to revert back to single threaded mode
	if(runtimeThreads < 1)
	{
		int ec = errno;
		fprintf(stderr, "Could not lanuch additional threads: %s\n", strerror(ec));
		exit(1);
	}

	int actualThreads = (runtimeThreads < JSYS_NUM_POSSIBLE_THREADS) ? runtimeThreads : JSYS_NUM_POSSIBLE_THREADS;

	pthread_barrier_init(&jsys__SyncBarrier, NULL, actualThreads + 1);
	for(i = 0; i < actualThreads; i++)
	{
		int rc = pthread_create(&jsys__Threads[i], NULL, ThreadProc, 0);
		if(rc != 0)
		{
			int ec = errno;
			fprintf(stderr, "Error creating thread no. %d! (%d requested: %s)\n", i, actualThreads, strerror(ec));
			exit(1);
		}
	}
	printf("jsys_Init: %d threads created (%d requested)\n", actualThreads, runtimeThreads);
	if(runtimeThreads > JSYS_NUM_POSSIBLE_THREADS)
	{
		printf("jsys_Init: NOTE: Your machine can support at least %d runtime threads (currently using %d threads). "
			   "Consider increasing JSYS_NUM_POSSIBLE_THREADS to this amount\n",
			   actualThreads,
			   runtimeThreads);
	}
	jsys__IsInitialized = 1;
}

void jsys_AddJob(job_info JobInfo, pJobFunc *JobProc)
{
	jsys_CheckInit();

	queue_info QueueInfo;
	pthread_mutex_lock(&jsys__JobLock);
	QueueInfo.Proc = JobProc;
	QueueInfo.JobInfo = JobInfo;
	jsys_Enqueue(&QueueInfo);
	pthread_mutex_unlock(&jsys__JobLock);
	pthread_cond_signal(&jsys__ThreadProcCond);
}

void jsys_ThreadSync()
{
	jsys_CheckInit();

	// Do we need to have the jsys__JobLock as well?
	pthread_mutex_lock(&jsys__JobLock);
	pthread_mutex_lock(&jsys__SyncLock);
	jsys__SyncStatus = 1;
	pthread_mutex_unlock(&jsys__SyncLock);
	pthread_mutex_unlock(&jsys__JobLock);

	pthread_cond_broadcast(&jsys__ThreadProcCond);
	pthread_barrier_wait(&jsys__SyncBarrier);


	pthread_mutex_lock(&jsys__SyncLock);
	jsys__SyncStatus = 0;
	pthread_mutex_unlock(&jsys__SyncLock);
}
