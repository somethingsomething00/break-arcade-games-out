#ifndef _LINUX_AUDIO_OPENAL_V2_C_
#define _LINUX_AUDIO_OPENAL_V2_C_


// NAME: linux_audio_openal_v2.c
// TIME OF CREATION: 2022-04-18 18:46:13
// AUTHOR:
// DESCRIPTION: Audio interface for Linux
// 				Uses OpenAL for audio
// 				Uses stb_vorbis to decode ogg files into readable data for OpenAL

// 				Linux and Windows audio code is now unified when using OpenAL

#include "common_audio_openal.c"

#endif /* _LINUX_AUDIO_OPENAL_V2_C_ */
