#if PLATFORM_PROFILER

#ifndef _PLATFORM_PROFILER_C_
#define _PLATFORM_PROFILER_C_

timed_block TimedBlock[TB_TOTAL] = {0};
cycle_block CycleBlock[CB_TOTAL] = {0};

#ifndef _WIN32
#include "linux_platform_profiler.c"
#else
#include "win32_platform_profiler.c"
#endif /* _WIN32 */

void platform_TimedBlockBegin(timed_block_t id)
{
	TimedBlock[id].begin = platform_GetTime();
}

void platform_TimedBlockEnd(timed_block_t id)
{
	u64 begin;
	u64 end;
	TimedBlock[id].end = platform_GetTime();
	begin = TimedBlock[id].begin;
	end = TimedBlock[id].end;

	TimedBlock[id].seconds += ((end - begin) / platform_GetTimerFrequency());
	TimedBlock[id].hits++;
}

void platform_TimedBlockList()
{
	int i;
	for(i = 0; i <  TB_TOTAL; i++)
	{
		printf("[%s] %.4f ms\n", HumanReadableTimedBlock((timed_block_t)i), (TimedBlock[i].seconds / (double)TimedBlock[i].hits)*1000.0);
	}
}

void platform_TimedBlockClearAll()
{
	int i;
	for(i = 0; i <  TB_TOTAL; i++)
	{
		TimedBlock[i].seconds = 0;
		TimedBlock[i].hits = 0;
		TimedBlock[i].begin = 0;
		TimedBlock[i].end  = 0;
	}
}

// Measure cpu cycles
void platform_CycleBlockBegin(cycle_block_t id)
{
	CycleBlock[id].cycles = GetCycles();
}

// Explicitly provide the number of hits timed in the block
void platform_CycleBlockEndHits(cycle_block_t id, u64 hits)
{
	CycleBlock[id].cycles = GetCycles() - CycleBlock[id].cycles;
	CycleBlock[id].hits = hits;
}

void platform_CycleBlockEnd(cycle_block_t id)
{
	CycleBlock[id].cycles = GetCycles() - CycleBlock[id].cycles;
	CycleBlock[id].hits++;
}

void platform_CycleBlockList()
{
	int i;
	for(i = 0; i < CB_TOTAL; i++)
	{
		double cyh;
		// Avoid divide by zero bug
		if(CycleBlock[i].hits)
		{
			cyh = (double)CycleBlock[i].cycles / (double)CycleBlock[i].hits;
		}
		else
		{
			cyh = 0;
		}
		printf("[%s]: Hits: %"PRIu64" cy/h: %.4f\n", HumanReadableCycleBlock(i), CycleBlock[i].hits, cyh);
	}
}

#define HRTB(id) \
case (id): return #id; break;
const char *HumanReadableTimedBlock(timed_block_t id)
{
	switch(id)
	{
		HRTB(TB_GAME);
		HRTB(TB_IMAGE);
		HRTB(TB_MEMCPY);
		HRTB(TB_TEXTURE);
		HRTB(TB_BEFORE_GAME);
		HRTB(TB_CLEAR_SCREEN);
		HRTB(TB_LOAD_SOUND);
		default:
		Assert(!"Unhandled profile id in HumanReadableTimedBlock");
	}
	return NULL;
}

#define HRCB(i) case (i): return #i;
const char *HumanReadableCycleBlock(cycle_block_t id)
{
	switch(id)
	{
		HRCB(CB_MEMCPY);
		HRCB(CB_CLEAR_SCREEN);
		default:
		Assert(!"Unhandled cycle block id in HumanReadableCycleBlock");
	}
	return NULL;
}

#endif /* _PLATFORM_PROFILER_C_ */

#endif /* PROFILER */
