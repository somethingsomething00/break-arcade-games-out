#include <windows.h>

// Limited pthread wrappers using the Windows API
// Do note that this code is probably incredibly buggy,
// do not use for anything but the simplest of things!

// In particular, I sure have my doubts about the
// pthread_barrier_wait, and pthread_cond_broadcast
// implementations.

// Anyhow, this was done purely as an educational excercise and for a bit of fun.

// The function signatures match the pthreads headers,
// so this can be used as a drop-in replacement.
// Some parameters will be unused.

// Implemented:

// pthread_barrier_init
// pthread_barrier_wait

// pthread_cond_init
// pthread_cond_wait
// pthread_cond_signal
// pthread_cond_broadcast

// pthread_mutex_init
// pthread_mutex_lock
// pthread_mutex_unlock

// pthread_create
// pthread_join


// Compatability with pthread.h
// In the POSIX api, these are static initializers that the compiler can normally use at compile-time
// without calling pthread...init()
// For Win32, we will need to initialize these explicitly during runtime.
#define PTHREAD_MUTEX_INITIALIZER {0}
#define PTHREAD_COND_INITIALIZER {0}


typedef struct
{
	HANDLE Semaphore;
	HANDLE Mutex;
	unsigned int TotalCount;
	unsigned int RunningCount;
} pthread_barrier_t;

typedef struct
{
	HANDLE Semaphore;
} pthread_cond_t;

typedef struct
{
	HANDLE Handle;
} pthread_mutex_t;


//------------------------
// Mutex
//------------------------
int pthread_mutex_init(pthread_mutex_t *Mutex)
{
	Mutex->Handle = CreateMutex(0, 0, 0);
	return !(Mutex->Handle == NULL);
}

void pthread_mutex_lock(pthread_mutex_t *Mutex)
{
	WaitForSingleObject(Mutex->Handle, INFINITE);
}

void pthread_mutex_unlock(pthread_mutex_t *Mutex)
{
	ReleaseMutex(Mutex->Handle);
}

#define WIN32_COND_WAIT_MAX_THREADS 64
// @Super hack
// When a semaphore is released using pthread_cond_signal,
// only a single thread that was blocked by pthread_cond_wait
// is chosen to advance per function call.
// The rest remain waiting.

// When calling pthread_cond_broadcast,
// we want to wake up all threads blocked on the wait condition.

// To do that, we just release all of the semaphores at once.
// This is by no means correct, but it gets us the result we are looking for,
// namely, all threads blocked by pthread_cond_wait can wake up and advance.

// This is similar to what we do with pthread_barrier_wait, except in that case
// we actually know the number of semaphores to create at initialization.

// A consequence of this is that only a maximum of WIN32_COND_WAIT_MAX_THREADS
// number of threads can be waiting on this condition variable concurrently,
// otherwise we get strange behaviour.

//------------------------
// Cond
//------------------------
int pthread_cond_init(pthread_cond_t *Cond)
{
	Cond->Semaphore = CreateSemaphore(0, 0, WIN32_COND_WAIT_MAX_THREADS, 0);
	return !(Cond->Semaphore == NULL);
}

void pthread_cond_wait(pthread_cond_t *Cond, pthread_mutex_t *Mutex)
{
	ReleaseMutex(Mutex->Handle);
	WaitForSingleObject(Cond->Semaphore, INFINITE);
}

// Note: This is probably wrong!
void pthread_cond_broadcast(pthread_cond_t *Cond)
{
	ReleaseSemaphore(Cond->Semaphore, WIN32_COND_WAIT_MAX_THREADS, 0);
}

void pthread_cond_signal(pthread_cond_t *Cond)
{
	ReleaseSemaphore(Cond->Semaphore, 1, 0);
}


//------------------------
// Barrier
//------------------------
void pthread_barrier_wait(pthread_barrier_t *Barrier)
{
	WaitForSingleObject(Barrier->Mutex, INFINITE);
	Barrier->RunningCount++;
	if(Barrier->RunningCount == Barrier->TotalCount)
	{
		Barrier->RunningCount = 0;
		ReleaseSemaphore(Barrier->Semaphore, Barrier->TotalCount, 0);

		// Is this part correct?
		ReleaseMutex(Barrier->Mutex);
		return;
	}
	ReleaseMutex(Barrier->Mutex);
	WaitForSingleObject(Barrier->Semaphore, INFINITE);
}

void pthread_barrier_init(pthread_barrier_t *Barrier, void *Ignore0, unsigned int Count)
{
	Barrier->Semaphore = CreateSemaphore(0, 0, Count, 0);
	Barrier->Mutex = CreateMutex(0, 0, 0);
	Barrier->TotalCount = Count;
	Barrier->RunningCount = 0;
}


//------------------------
// Pthread (thread handle)
//------------------------
typedef  HANDLE pthread_t;
typedef DWORD(*pWin32ThreadProc)(void *data);
typedef void *(*pPosixThreadProc)(void *data);

int pthread_create(pthread_t *Thread, void *Ignore0, pPosixThreadProc Proc, void *Data)
{
	*Thread = CreateThread(0, 0, (pWin32ThreadProc)Proc, Data, 0, 0);
	return (*Thread == NULL);
	// pthread_create returns NON-ZERO on failure
}

void pthread_join(pthread_t Thread, void *Ignore0)
{
	WaitForSingleObject(Thread, INFINITE);
}
