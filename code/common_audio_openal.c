// NAME: common_audio_openal.c
// TIME OF CREATION: 2022-05-05 15:03:16
// AUTHOR:
// DESCRIPTION: Audio interface for C programs that wish to use OpenAL as a backend
// 				Uses stb_vorbis to decode OGG files.

// 				Included is an optional dependency for common_jsys.c, which is a multithreaded job system that's used
// 				to speed up loading OGG files.

// 				If the jsys implementation is missing from this source, you can enable the single-threaded version
// 				of snd_AsyncLoadOggFromMemory. There is a function already prepared and ifdefed out.

// 				If you wish to differentiate between the Linux and Windows implementations, this source can be copied
// 				verbatim as a starting point, and included as a separate file for the OS you wish to target.


#ifndef _COMMON_AUDIO_OPENAL_C_
#define _COMMON_AUDIO_OPENAL_C_

#include <AL/al.h>
#include <AL/alc.h>

#include "common_audio_openal_wav_loader.c"

#define SND_DEFAULT_FREQUENCY 44100


// Apparently, OpenAL only supports 256 sound sources?
#define SND_PERSISTENT_SOUND_QUANTITY 32
#define SND_SINGLE_SOUND_QUANTITY 32
#define SND_SOUND_QUANTITY 128
#if SND_SINGLE_SOUND_QUANTITY > SND_PERSISTENT_SOUND_QUANTITY
// #define SND_SOUND_QUANTITY (SND_SINGLE_SOUND_QUANTITY * 2)
#else
// #define SND_SOUND_QUANTITY (SND_PERSISTENT_SOUND_QUANTITY * 2)
#endif

// How many sound sources can be active at any one time?
// New sources will cancel old ones if the old ones
// are not finished playing by the time the index reaches them again

// Persistent (looping) sources will never overwrite each-other,
// provided we keep them below the capacity of SND_PERSISTENT_SOUND_QUANTITY



/**********************************
* Defines
**********************************/
#define snd_PrintEnumString(alenum)                 \
do {                                                \
	const ALchar *alString;                         \
	alString = alGetString((alenum));               \
	if(alString)                                    \
	{                                               \
		snd_Print("%s: %s \n", #alenum, (alString));\
	}                                               \
} while(0)



/**********************************
* Types
**********************************/
typedef struct
{
	// Asset ID as defined in cooker_common.c
	int assetId;

	// OpenAL data
	ALuint al_Buffer;
	ALfloat al_DefaultPitch;
} Loaded_Sound;

typedef struct Playing_Sound Playing_Sound;
struct Playing_Sound
{
    b32 active;

    Loaded_Sound *sound;

    b32 looping;
    f32 volume;
    f32 target_volume;
    f32 speed_multiplier;

	f32 fading_speed;

	// Unused here, for compatability with the main game
    f32 pan;
    f32 *source_x;

	// Unused here, for compatability with the main game
    struct Playing_Sound *synced_sound;

	// OpenAL data
	ALuint al_Source;
};

typedef struct
{
	Playing_Sound Sound[SND_SOUND_QUANTITY];
	Playing_Sound *pSound[SND_SOUND_QUANTITY];
	int pointerIndex;
	int arrayIndex;
} Sound_Collection;

typedef struct
{
    ALCdevice *Device;
    ALCcontext *Context;
} Context;



/**********************************
* Globals
**********************************/
int snd_IsInitialized = 0;

Playing_Sound snd_DummySound  = {0};

Sound_Collection snd_LoopingSounds = {0};
Sound_Collection snd_NonLoopingSounds = {0};

Context snd_Context = {0};

// From asset_loader.c
extern Loaded_Sound sounds[LAST_SOUND-FIRST_SOUND+1];


/**********************************
* Interface
**********************************/
void snd_SourceInit(ALuint sourceId)
{
	alSourcef(sourceId, AL_GAIN, 1.0f);
	alSourcef(sourceId, AL_PITCH, 1.0f);
	alSource3f(sourceId, AL_POSITION, 0, 0, 0);
}


void snd_Init()
{
	ALCcontext *Context;
	ALCdevice *Device;
	int success = 0;

    Device = alcOpenDevice(NULL);
    if(Device)
    {
        Context = alcCreateContext(Device, NULL);
        if(Context)
        {
            success = 1;
            alcMakeContextCurrent(Context);
            snd_Context.Device = Device;
            snd_Context.Context = Context;
        }
    }


	if(!success)
	{
		snd_Log("sndInit: Could not initialize OpenAL! Continuing without sound\n");
		return;
	}


	snd_Log("snd_Init: Initializing OpenAL...\n");
	snd_Print("=====================================================================\n");
	snd_PrintEnumString(AL_VENDOR);
	snd_PrintEnumString(AL_RENDERER);
	snd_PrintEnumString(AL_VERSION);
	snd_Print("=====================================================================\n\n");


	alDistanceModel(AL_LINEAR_DISTANCE);
	// alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);
	alListener3f(AL_POSITION, 0, 0, 0);
	alListener3f(AL_VELOCITY, 0, 0, 0);

	// Init sources
	int i;
	for(i = 0; i < SND_SINGLE_SOUND_QUANTITY; i++)
	{
		alGenSources(1, &snd_NonLoopingSounds.Sound[i].al_Source);
		snd_SourceInit(snd_NonLoopingSounds.Sound[i].al_Source);
	}

	for(i = 0; i < SND_PERSISTENT_SOUND_QUANTITY; i++)
	{
		alGenSources(1, &snd_LoopingSounds.Sound[i].al_Source);
		snd_SourceInit(snd_LoopingSounds.Sound[i].al_Source);
	}


	snd_IsInitialized = 1;
}

void snd_Shutdown()
{
	snd_Log("snd_Shutdown: Deleting sources and buffers...\n");

	// sounds are from asset_loader.c
	int i;
	Loaded_Sound NoLoadedSound = {0};
	Playing_Sound NoPlayingSound = {0};
	for(i = 0; i < LAST_SOUND - FIRST_SOUND + 1; i++)
	{
		Loaded_Sound *S = &sounds[i];
		alDeleteBuffers(1, &S->al_Buffer);
		*S = NoLoadedSound;
	}

	for(int i = 0; i < SND_SINGLE_SOUND_QUANTITY; i++)
	{
		alDeleteSources(1, &snd_NonLoopingSounds.Sound[i].al_Source);
		snd_NonLoopingSounds.Sound[i] = NoPlayingSound;
	}

	for(int i = 0; i < SND_PERSISTENT_SOUND_QUANTITY; i++)
	{
		alDeleteSources(1, &snd_LoopingSounds.Sound[i].al_Source);
		snd_LoopingSounds.Sound[i] = NoPlayingSound;
	}

	snd_IsInitialized = 0;
}

void snd_SetVolume(Playing_Sound *Sound, f32 volume)
{
	volume = max(0, volume);
	Sound->volume = volume;
	Sound->target_volume = volume;

	int source = Sound->al_Source;
	alSourcef(source, AL_GAIN, Sound->volume);
}


Loaded_Sound snd_LoadOggFromMemory(String *SoundData)
{
	ALuint buffer;
	size_t decodedDataSize;
	Loaded_Sound Result = {0};

	// Uncomment this to return a blank audio data handle (for debugging)
	// return Result;

	// stb_vorbis
	int chanRet;
	int sampleRateRet;
	short *dataRet;
	int sampleCount = stb_vorbis_decode_memory((const unsigned char *)SoundData->data, SoundData->size, &chanRet, &sampleRateRet, &dataRet);

	// If the data could not be recognized as an ogg file, we just return with null.
	// The subsequent play_sound code checks for null before playing any sound
	// Note:
	// stb_vorbis_decode_memory returns -1 on pointer error (if we somehow passed a NULL pointer as the memory)
	// It returns -2 if there was a realloc error
	// This detail is missing from the general documentation of the function

	if(sampleCount < 0)
	{
		snd_Log("Data could not be recognized as an OGG file!\n");
		return Result;
	}

	alGenBuffers(1, &buffer);

	snd_Assert(sampleRateRet == SND_DEFAULT_FREQUENCY);
	snd_Assert(chanRet == 1 || chanRet == 2);

	decodedDataSize = sampleCount * chanRet * 2;
	// Through testing, this is what we have to do to give OpenAL the correct number of bytes

	// Is this accurately reflecting the original sounds?
	const int fmt = (chanRet == 1) ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;
	alBufferData(buffer, fmt, dataRet, decodedDataSize, sampleRateRet);
	free(dataRet);
	// Data allocated by stb_vorbis_decode_memory can safely be passed to free()

	Result.al_DefaultPitch = 1.0f;
	Result.al_Buffer = buffer;

	return Result;
}

#if 1

// Note: this function, and snd_AsyncLoadOggFromMemory require common_jsys.c and common_jsys.h (threaded job system implementation)
// If these files are not present, you can fallback to the single-threaded snd_LoadOggFromMemory function
JSYS_JOB_FUNC(snd_LoadOggFromMemoryCallback)
{
	Loaded_Sound *Sound = JobInfo.data1;
	String s;
	s.data = JobInfo.data2;
	s.size = (intptr_t)JobInfo.data3;
	*Sound = snd_LoadOggFromMemory(&s);
	Sound->assetId = (intptr_t)JobInfo.data4;
}

void snd_AsyncLoadOggFromMemory(Loaded_Sound *LoadedSound, String S, int asset)
{
	job_info Info;
	LoadedSound->assetId = asset;
	Info.data1 = LoadedSound;
	Info.data2 = S.data;
	Info.data3 = (void *)(intptr_t)S.size;
	Info.data4 = (void *)(intptr_t)asset;
	jsys_AddJob(Info, snd_LoadOggFromMemoryCallback);
}

#else

// Enable this if the common_jsys.c source is missing.
void snd_AsyncLoadOggFromMemory(Loaded_Sound *LoadedSound, String S, int asset)
{
	job_info Info;
	*LoadedSound = snd_AsyncLoadOggFromMemory(&S);
	LoadedSound->assetId = asset;
}

#endif /* 1 */

Loaded_Sound snd_LoadWavFromMemory(String *SoundData)
{
	ALuint buffer;
	Loaded_Sound Result = {0};

	// Uncomment this to return a blank audio data handle (for debugging)
	// return Result;


	void *al_WavData = NULL;
	ALsizei size;
	ALenum format;
	float frequency;
	const ALsizei freq = SND_DEFAULT_FREQUENCY;
	// We assume that the frequency is always 44100 khz

	alGenBuffers(1, &buffer);
	al_WavData = snd_LoadMemoryFromFileImage(SoundData->data, SoundData->size, &format, &size, &frequency);
	snd_Assert(al_WavData != NULL);
	snd_Assert(frequency == SND_DEFAULT_FREQUENCY);

	alBufferData(buffer, format, al_WavData, size, freq);
	free(al_WavData);
	int channels;
	alGetBufferi(buffer, AL_CHANNELS, &channels);
	float defaultPitch = 1.0f;

	// This may or may not be correct
	// Uncomment to heighten the pitch of sounds with 1 audio channel
	// Or... we can do this properly and change the WAV format
	if(channels == 1)
	{
		// defaultPitch = 2;
	}

	Result.al_DefaultPitch = defaultPitch;
	Result.al_Buffer = buffer;

	return Result;
}

void snd_FadeIn(Playing_Sound *Sound)
{
	// These numbers are completely arbitrary and were figured out by trial and error
	Sound->volume = move_towards(Sound->volume, Sound->target_volume, (Sound->fading_speed * 1000.0f) / (float)SND_DEFAULT_FREQUENCY);

}

void snd_Update()
{
	// We need to separate the sources from the respective buffers
	// The buffers can be used as a bank of sound effects,
	// whereas the sources can pick the same sound effect simultaneously and modify them to their liking
	{
		int i;
		for(i = 0; i < snd_NonLoopingSounds.pointerIndex; i++)
		{
			Playing_Sound *pSound = snd_NonLoopingSounds.pSound[i];
			int source = pSound->al_Source;
			alSourceStop(source);
			alSourcef(source, AL_PITCH, pSound->speed_multiplier);
			alSourcef(source, AL_GAIN, pSound->volume);
			alSourcePlay(source);
		}

		snd_NonLoopingSounds.pointerIndex = 0;
	}
	{
		int i;
		for(i = 0; i < snd_LoopingSounds.arrayIndex; i++)
		{
			Playing_Sound *pSound = snd_LoopingSounds.pSound[i];
			int source = pSound->al_Source;


			// The panning effect does not sound very good :(
			// Comment out to hear for yourself
			// if(pSound->source_x)
			{
				// pSound->pan = move_towards(pSound->pan, (*pSound->source_x - *player_visual_p_x_ptr) * 0.005f, 100.0f / SND_DEFAULT_FREQUENCY);
				// alSource3f(pSound->al_Source, AL_POSITION, pSound->pan, 0, 0.1);
			}
			snd_FadeIn(pSound);
			alSourcef(source, AL_GAIN, pSound->volume);
			alSourcef(source, AL_PITCH, pSound->speed_multiplier);
		}
	}
}


Playing_Sound *snd_NextFreeSound(b32 looping)
{
	// @Speed: Could make this branchless by using the looping bool as an array index
	if(looping)
	{
		// For persistent sounds only!
		int idx = snd_LoopingSounds.arrayIndex;
		snd_Assert(idx < SND_PERSISTENT_SOUND_QUANTITY);
		snd_LoopingSounds.pSound[idx] = &snd_LoopingSounds.Sound[idx];
		snd_LoopingSounds.arrayIndex++;
		return snd_LoopingSounds.pSound[idx];
	}
	else
	{
		int idxa = snd_NonLoopingSounds.arrayIndex;
		int idxp = snd_NonLoopingSounds.pointerIndex;

		snd_Assert(idxp < SND_SINGLE_SOUND_QUANTITY);
		snd_NonLoopingSounds.pSound[idxp] = &snd_NonLoopingSounds.Sound[idxa];
		snd_NonLoopingSounds.pointerIndex = (snd_NonLoopingSounds.pointerIndex + 1) % SND_SINGLE_SOUND_QUANTITY;
		snd_NonLoopingSounds.arrayIndex = (snd_NonLoopingSounds.arrayIndex + 1) % SND_SINGLE_SOUND_QUANTITY;
		return snd_NonLoopingSounds.pSound[idxp];
	}
}

Playing_Sound *snd_PlaySound(Loaded_Sound *Sound, b32 looping)
{
	Playing_Sound *Result = NULL;

	if(Sound->al_Buffer && snd_IsInitialized)
	{
		// int soundIdx = Sound->assetId;
		// snd_Log("Sound idx: %d [%s]\n", soundIdx, snd_HumanReadableSoundId(soundIdx));

		Result = snd_NextFreeSound(looping);

		Result->looping = looping;
		Result->volume = 1.0f;
		Result->target_volume = 1.0f;
		Result->speed_multiplier = Sound->al_DefaultPitch;
		Result->fading_speed = 3.0f;
		Result->sound = Sound;
		Result->source_x = NULL;
		Result->active = 1;

		int source = Result->al_Source;
		if(!looping)
		{
			alSourceStop(source);
		}

		alSourcei(source, AL_BUFFER, Sound->al_Buffer);
		alSourcei(source, AL_LOOPING, looping);

		// Begin the looping sounds here
		if(looping)
		{
			// Maybe, check if sound is already playing
			alSourcePlay(source);
		}
	}
	else
	{
		// Enable this assert to figure out if something went wrong with loading the sound.
		// Otherwise, it's unneeded since having an empty sound is also valid.
		// snd_Assert(0);

		Result = &snd_DummySound;
		Result->sound = Sound;

	}
	return Result;
}

Playing_Sound *snd_PlaySoundWithVariation(Loaded_Sound *Sound, f32 variation, f32 *source)
{
	Playing_Sound *Result = NULL;
	if(Sound->al_Buffer && snd_IsInitialized)
	{
		Result = snd_PlaySound(Sound, false);
		snd_SetVolume(Result, random_f32_in_range(1.0f - variation, 1.0f + variation));
		Result->speed_multiplier = random_f32_in_range(1.f-variation, 1.f+variation) * Sound->al_DefaultPitch;

		// This enables panning of certain sounds
		// At the moment, it doesn't sound that great with headphones, so it's disabled
#if 0
		if(source)
		{
			int s = Result->al_Source;
			alSource3f(s, AL_POSITION, - *source, 0, 0.2f);
		}
		else
		{
			int s = Result->al_Source;
			alSource3f(s, AL_POSITION, 0, 0, 0);
		}
#endif

	}
	else
	{

		// Enable this assert to figure out if something went wrong with loading the sound.
		// Otherwise, it's unneeded since having an empty sound is also valid.
		// snd_Assert(0);

		Result = &snd_DummySound;
		Result->sound = Sound;
	}
	return Result;
}

void snd_PauseAllSources()
{
	int playingStatus;
	for(int i = 0; i < SND_SINGLE_SOUND_QUANTITY; i++)
	{
		ALuint source = snd_NonLoopingSounds.Sound[i].al_Source;
		alGetSourcei(source, AL_SOURCE_STATE, &playingStatus);
		if(playingStatus == AL_PLAYING)
		{
			alSourcePause(source);
		}
	}

	for(int i = 0; i < snd_LoopingSounds.arrayIndex; i++)
	{
		ALuint source = snd_LoopingSounds.Sound[i].al_Source;
		alSourcePause(source);
	}
}

void snd_ResumeAllSources()
{
	int playingStatus;
	for(int i = 0; i < SND_SINGLE_SOUND_QUANTITY; i++)
	{
		ALuint source = snd_NonLoopingSounds.Sound[i].al_Source;
		alGetSourcei(source, AL_SOURCE_STATE, &playingStatus);
		if(playingStatus == AL_PAUSED)
		{
			alSourcePlay(source);
		}
	}

	for(int i = 0; i < snd_LoopingSounds.arrayIndex; i++)
	{
		ALuint source = snd_LoopingSounds.Sound[i].al_Source;
		alSourcePlay(source);
	}
}

#endif /* _COMMON_AUDIO_OPENAL_C_ */
