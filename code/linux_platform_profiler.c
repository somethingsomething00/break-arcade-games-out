#if PLATFORM_PROFILER
#ifndef _LINUX_PLATFORM_PROFILER_C_
#define _LINUX_PLATFORM_PROFILER_C_

double platform_GetTimerFrequency()
{
	return 1E9;
}

u64 platform_GetTime()
{
	struct timespec Ts;
	clock_gettime(CLOCK_MONOTONIC, &Ts);
	return (Ts.tv_sec * (u64)1E9) + Ts.tv_nsec;
}

// #include <x86intrin.h>
// Note: We don't want to include a heavy intrinsic library like x86intrin for a simple rdtsc call.
// Therefore it is inlined. It's probably less portable this way though.
u64 GetCycles()
{
	// rdtsc stores its result in rax and rdx respectively
	// rax contains the low 32 bits (as a 32 bit value)
	// rdx contains the high 32 bits (as a 32 bit value)
	// When compiling for 32 bits, the registers become eax and edx respectively
	// Note: GNU assembler syntax
	u64 result = 0;

#if __WORDSIZE == 32
	u32 eax; // Lo
#elif __WORDSIZE == 64
	u64 eax; // Lo
	// gcc and clang perform an extra move when using a u32 variable so we make this distinction
#else
	#error "Word size must be 32 or 64 bit"
#endif

	u32 edx; // Hi
	__asm__ volatile("rdtsc" : "=a"(eax), "=d"(edx));
	result = eax | ((u64)edx << 32);
	return result;

	// return __rdtsc();
}

#endif /* _LINUX_PLATFORM_PROFILER_C_ */

#endif /* PLATFORM_PROFILER */
