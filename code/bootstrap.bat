@echo off
REM To customize the build file, see build_win32.c
REM After making your changes, run this again

echo Compiling 'build.c'
setlocal
set cc=cl
%cc% /nologo build.c && echo build.exe now compiled. Run build.exe to build the project!
endlocal
