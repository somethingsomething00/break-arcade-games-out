#ifndef _COMMON_AUDIO_H_
#define _COMMON_AUDIO_H_

void snd_Init();
void snd_Shutdown();

void snd_SetVolume(Playing_Sound *Sound, f32 volume);

Loaded_Sound snd_LoadOggFromMemory(String *SoundData);
Loaded_Sound snd_LoadWavFromMemory(String *SoundData);

Playing_Sound *snd_PlaySound(Loaded_Sound *Sound, b32 looping);
Playing_Sound *snd_PlaySoundWithVariation(Loaded_Sound *Sound, f32 variation, f32 *source);

void snd_Update();



// For compatability with the main game code
#define set_volume(Sound, volume) snd_SetVolume(Sound, volume)
#define play_sound(Sound, looping) snd_PlaySound(Sound, looping)
#define play_sound_with_variation(Sound, variation, source) snd_PlaySoundWithVariation(Sound, variation, source)

#endif /* _COMMON_AUDIO_H_ */
