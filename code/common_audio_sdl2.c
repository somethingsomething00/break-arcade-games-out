#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#define MIXER_VOLUME_MAX 128
#define SND_NUM_CHANNELS 128
#define SND_DEFAULT_FREQUENCY 44100

/**********************************
* Types
**********************************/
typedef struct
{
	// Asset ID as defined in cooker_common.c
	int assetId;
	
	// OGG, or WAV?
	int originalSoundFormat;
	
	
	// SDL Data
	Mix_Chunk *MixChunk;
	int channelNum;
} Loaded_Sound;


typedef struct Playing_Sound Playing_Sound;
struct Playing_Sound
{
    b32 active;
	
    Loaded_Sound *sound;
    // f32 position; //Sample position
	
    b32 looping;
    f32 volume;
    f32 target_volume;
    f32 fading_speed;
    f32 pan;
    f32 speed_multiplier;
	
    f32 *source_x;
	
    struct Playing_Sound *synced_sound;
    struct Playing_Sound *next_free;
	
	int channel;
};

/**********************************
* Globals
**********************************/
#define SND_SOUND_QUANTITY 128

int snd_IsInitialized = 0;
int snd_SoundsThisFrame = 0;
Playing_Sound snd_AllPlayingSounds[SND_SOUND_QUANTITY];
Playing_Sound *snd_PlayingSoundsThisFrame[SND_SOUND_QUANTITY];
Playing_Sound Dummy;

/**********************************
* Interface
**********************************/

void snd_Init()
{
	if(SDL_Init(SDL_INIT_AUDIO) != 0)
	{
		snd_Log("snd_Init: Could not init audio!\n");
		snd_Log("snd_Init: Continuing without audio\n");
		return;
	}
	
	Mix_Init(MIX_INIT_OGG | MIX_INIT_OPUS);
	
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 512);
	
	int allocatedChannels = Mix_AllocateChannels(SND_NUM_CHANNELS);
	const char *driver = SDL_GetAudioDriver(0);
	snd_Log("snd_Init: SDL audio driver: %s\n", driver);
	snd_Log("snd_Init: Requested %d channels, got %d\n", SND_NUM_CHANNELS, allocatedChannels);
}

void snd_Shutdown()
{
	// Empty
}

Loaded_Sound snd_LoadSoundFromMemory(String *SoundData)
{
	Loaded_Sound Result;
	Mix_Chunk *Chunk = NULL;
	SDL_RWops *Obj = SDL_RWFromConstMem(SoundData->data, SoundData->size);
	snd_Assert(Obj);
	Chunk = Mix_LoadWAV_RW(Obj, 1);
	snd_Assert(Chunk);
	Result.MixChunk = Chunk;
	
	Result.channelNum = SND_NUM_CHANNELS + 1;
	
	return Result;
}


Loaded_Sound snd_LoadOggFromMemory(String *SoundData)
{
	return snd_LoadSoundFromMemory(SoundData);
	
}

Loaded_Sound snd_LoadWavFromMemory(String *SoundData)
{
	return snd_LoadSoundFromMemory(SoundData);
}

// @Duplication: This code is identical for both audio layers
// Note: this function requires common_jsys.c and common_jsys.h (threaded job system implementation)
JSYS_JOB_FUNC(snd_LoadOggFromMemoryCallback)
{
	Loaded_Sound *Sound = JobInfo.data1;
	String s;
	s.data = JobInfo.data2;
	s.size = (s64)JobInfo.data3;
	*Sound = snd_LoadOggFromMemory(&s);
	Sound->assetId = (intptr_t)JobInfo.data4;
}

// @Hack
// For some reason, SDL2 mixer is actually slower to load OGGs when it's multi-threaded!
// We redefine the function so that the main program is none the wiser :<
#if 0
void snd_AsyncLoadOggFromMemory(Loaded_Sound *LoadedSound, String S, int asset)
{
	job_info Info;
	LoadedSound->assetId = asset;
	Info.data1 = LoadedSound;
	Info.data2 = S.data;
	Info.data3 = (void *)S.size;
	Info.data4 = (void *)(intptr_t)asset;
	jsys_AddJob(Info, snd_LoadOggFromMemoryCallback);
}
#else
void snd_AsyncLoadOggFromMemory(Loaded_Sound *LoadedSound, String S, int asset)
{
	*LoadedSound = snd_LoadSoundFromMemory(&S);
	LoadedSound->assetId = asset;
}
#endif

void snd_FadeIn(Playing_Sound *Sound)
{
	// These numbers are completely arbitrary and were figured out by trial and error
	Sound->volume = move_towards(Sound->volume, Sound->target_volume, (Sound->fading_speed * 1000.0f) / (float)SND_DEFAULT_FREQUENCY);
}

void snd_SetVolume(Playing_Sound *Sound, f32 volume)
{
	volume = max(volume, 0);
	Sound->volume = volume;
	Sound->target_volume = volume;
	
	if(Sound->looping && Sound->active)
	{
		// Mix_VolumeChunk(Sound->sound->MixChunk, (int)((Sound->volume) * 128.0f));
		// @Temp
		// We set the volume explicitly while we work out pitch shifting in snd_Update for persistent sounds
		// Mix_Volume(Sound->channel, (int)((Sound->volume) * 128.0f));
	}
}

Playing_Sound *snd_NextFreeSound()
{
	int i;
	Playing_Sound *Result = NULL;
	for(i = 0; i < SND_SOUND_QUANTITY; i++)
	{
		Playing_Sound *pSound = &snd_AllPlayingSounds[i];
		if(!pSound->active)
		{
			Result = pSound;
			Result->active = true;
			snd_Assert(snd_SoundsThisFrame < SND_SOUND_QUANTITY);
			snd_PlayingSoundsThisFrame[snd_SoundsThisFrame] = pSound;
			snd_SoundsThisFrame++;
			goto end;
		}
	}
	end:
	return Result;
	
}

Playing_Sound *snd_PlaySound(Loaded_Sound *Sound, b32 looping)
{
	Playing_Sound *Result;
	
	// For debugging
	// Dummy.sound = Sound;
	// return &Dummy;
	
	Result = snd_NextFreeSound();
	
	Result->looping = looping;
	Result->volume = 1.0f;
	Result->target_volume = 1.0f;
	Result->speed_multiplier = 1.0f;
	Result->fading_speed = 3.0f;
	Result->sound = Sound;
	Result->active = 1;
	
	return Result;
}

Playing_Sound *snd_PlaySoundWithVariation(Loaded_Sound *Sound, f32 variation, f32 *source)
{
	Playing_Sound *Result = NULL;
	
	Result = snd_PlaySound(Sound, false);
	snd_SetVolume(Result, random_f32_in_range(1.0f - variation, 1.0f + variation));
	Result->speed_multiplier = random_f32_in_range(1.f-variation, 1.f+variation);
	return Result;
	
	// Dummy.sound = Sound;
	// return &Dummy;
}
#if 1
extern Loaded_Sound sounds[LAST_SOUND-FIRST_SOUND+1];
Loaded_Sound*
get_sound(int asset);

void snd_Update()
{
	int i;
	for(i = 0; i < snd_SoundsThisFrame; i++)
	{
		Playing_Sound *S = snd_PlayingSoundsThisFrame[i];
		Mix_Chunk *Chunk = S->sound->MixChunk;
		
		// -1 is the value that SDL_Mixer equates to infnite looping
		int looping = S->looping ? -1 : 0;
		
		if(!S->looping)
		{
			// Mix_VolumeChunk(Chunk, (int)((S->volume) * 128.0f));
			S->active = 0;
		}
		
		int channel = Mix_PlayChannel(-1, Chunk, looping);
		S->channel = channel;
		if(S->looping)
		{
			Mix_Volume(channel, (int)(S->volume * 128.0f));
		}
		else
		{
			if(channel != -1)
			{
				Mix_Volume(channel, (int)(S->volume * 128.0f));
			}
		}
	}
	snd_SoundsThisFrame = 0;
	
	// For looping sounds
	for(i = 0; i < SND_SOUND_QUANTITY; i++)
	{
		Playing_Sound *S = &snd_AllPlayingSounds[i];
		if(S->looping && S->active)
		{
			snd_FadeIn(S);
			float v = S->volume * 128.0f;
			// Mix_Volume(S->channel, (int)v);
		}
	}
}
#else
void snd_Update()
{
	int i;
	for(i = 0; i < snd_SoundsThisFrame; i++)
	{
		Playing_Sound *S = snd_PlayingSoundsThisFrame[i];
		Mix_Chunk *Chunk = S->sound->MixChunk;
		
		// -1 is the value that SDL_Mixer equates to infnite looping
		int looping = S->looping ? -1 : 0;
		
		Mix_HaltChannel(S->sound->channelNum);
		Mix_VolumeChunk(Chunk, (int)((S->volume) * 128.0f));
		
		int channel = Mix_PlayChannel(-1, Chunk, looping);
		S->sound->channelNum = channel;
		
		if(!S->looping)
		{
			S->active = 0;
		}
	}
	snd_SoundsThisFrame = 0;
}
#endif

// Compatability with the game
#define snd_ResumeAllSources(...)
#define snd_PauseAllSources(...)
