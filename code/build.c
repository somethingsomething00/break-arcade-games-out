#include "build.h"

#include "config.h"
// The main game config
// Used to determine compile-time parameters


// Common defines

// Note: This doesn't actually matter from a C API standpoint
// The forward-slash is compatible in Windows.
// This is purely for printing reasons
#ifdef _WIN32
#define BUILD_DIR "..\\build"
#else
#define BUILD_DIR "../build"
#endif /* _WIN32 */

#define DATA_DIR "data"


void build_directories_create()
{
	mkdirs(BUILD_DIR, DATA_DIR);
}

void copy_assets()
{
	copy_file(
			  make_path("data", "assets.pak"),
			  make_path(BUILD_DIR, "data"),
			  CPY_NO_CLOBBER);
}

// A dummy config file with default options
void create_config_file()
{
	const char config[] =
		"mouse_sensitivity = 1.0\n"
		"screen_shake = true\n"
		"windowed = false\n";
	
	// -1 so we don't write the null byte
	char *path = make_path(BUILD_DIR, "config.txt");
	file_create_from_memory(path, config, array_count(config) - 1, false);
}

#if defined (_WIN32)
	#include "build_win32.c"
#elif defined __linux__
	#include "build_linux.c"
#else
	#error "Unsupported OS! You will have to write a build file to compile this project." \
	"See build_linux.c for an exmaple."
#endif
