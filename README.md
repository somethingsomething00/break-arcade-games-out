# Break Arcade Games Out

This is a fork of [Break Arcade Games Out](https://github.com/DanZaidan/break_arcade_games_out) by [Dan Zaidan](https://github.com/DanZaidan).

Break Arcade Games Out is a breakout-style game that is themed after popular arcade games.

It features 3 new ports to Linux, as well as a new OpenGL and SDL2 port to Windows.

In addition, there are several bug fixes and optimizations.

![thumbnail](./thumbnail.png)

![thumbnail](./scr00.png)


## Quick Start
If you just want to jump in and play the game.

NB: If you're in Windows, you will need to set up **vcvarsall.bat** if you haven't already.

Run these commands from the **code** directory.

### Linux
```console
./bootstrap.sh
./build
cd ../build
./break-arcade-games-out-linux-gl
```

### Windows
```console
bootstrap.bat
build.exe
cd ..\build
break-arcade-games-out-win32-gl.exe
```


## Controls
| Key(s)            | Action                            | Notes            |
|-------------------|-----------------------------------|------------------|
| Mouse-Movement    | Move the player paddle            |                  |
| Left-Mouse-Button | Select active level               |                  |
| F, Alt-Enter, F11 | Toggle fullscreen                 |                  |
| Space, P          | Toggle pause                      |                  |
| L                 | Reload the config file            |                  |
| Esc               | Exit the current level            |                  |
| 1                 | Enable the hardware renderer      | OpenGL, SDL2     |
| 2                 | Enable the software renderer      | OpenGL, SDL2     |
| 3                 | Set borderless fullscreen mode    | OpenGL           |
| 4                 | Set complete fullscreen mode      | OpenGL           |
| V                 | Toggle wireframe mode             | OpenGL           |
| Up                | Invincibility                     | Development Mode |
| Down              | Break blocks while key is held    | Development Mode |
| Right             | Advance to next level             | Development Mode |
| R                 | Clear the platform profiler       | Profiler         |
| W                 | Print the profiler to the console | Profiler         |
| Q, Alt-F4         | Quit the game                     |                  |


## Config (Game)
The game supports a rudimentary config file, `config.txt`.

The following options are accepted:
| Key                   | Value        | Desc                                                    |
|-----------------------|--------------|---------------------------------------------------------|
| **mouse_sensitivity** | 0.1 to 20.0  | -                                                       |
| **screen_shake**      | true / false | -                                                       |
| **windowed**          | true / false | *Sets window mode on game launch.                       |
| **lock_fps**          | true / false | *Locks framerate to native refresh rate of the monitor. |



**windowed** is only implemented in the GDI and SDL versions for now. Defaults to fullscreen otherwise.

**lock_fps** does not do anything at the moment in the new ports, the frame rate is always capped at the native refresh rate of the monitor.

You may set the options in the following way:

`key = value`


```ini
mouse_sensitivity = 5.0

windowed = false
```


## Building (in depth)

I wrote a basic build system in C that unifies both Linux and Windows based operating systems. It uses a C file as its build recipe.

Each OS has its own build file:
- [Linux](code/build_linux.c)
- [Windows](code/build_win32.c)

Each file is included in [build.c](code/build.c) based on the host OS.

[build.c](code/build.c) is then compiled into the executable that builds the project.

For the implementation, see [build.h](code/build.h)


You may add or remove build targets as you see fit.

Uses `gcc` on Linux, and `cl` on Windows. To change the compiler, modify the `CC` define in the respective platform file.

The executable produced by `build.c` accepts command-line options that are passed to all tagets.

However, due to Windows' peculiarity concerning the `-link` flag when using `cl`, this will not work as intended Windows.

**Tested with:** `gcc`, `clang`, `cl`, `clang-cl`, `mingw-gcc`


## Supported targets

### Linux
- GL / GLFW
- X11
- SDL2

### Windows
- GL / GLFW
- GDI
- SDL2



## Building
By default, both Linux and Windows compile the `OpenGL` target.

Note: Run these commands from the **code** directory.

### Linux
```console
./bootstrap.sh
./build

# Pass command line options to the compiler
./build -DPROFILER=1
```

### Windows
```console
bootstrap.bat
build.exe
```

## Running

After compiling, check the `build` directory for your game!


## Dependencies
### All
- 64 bit C compiler.
- Note: A 32 bit executable will also compile, provided you supply the 32 bit libraries.

## Linux
### GL
- OpenGL 1.2
- GLFW 3
- OpenAL (required if compiling with audio support)

### X11
- X Shared Memory extension
- X Double Buffer extension
- GLX (required if using GLX for vsync)
- OpenAL (required if compiling with audio support)

### SDL2
- SDL2
- SDL2_mixer (required if compiling with audio support)


## Windows
### GL
- OpenGL 1.2 (ships with Windows natively)
- GLFW (dll included)
- OpenAL (required if compiling with audio support, dll included)

### SDL2
- SDL2 (dll included)
- SDL2_mixer (required if compiling with audio support, dll included)
- libvorbis, libogg (required if compiling with audio support, dll included)


## Compile time configuration
See [config.h](code/config.h)


## Modifying build.c
If you'd like to change build options, modify the following files to your liking:
- [build.c](code/build.c)
- [Linux](code/build_linux.c)
- [Windows](code/build_win32.c)

Note that you will have to recompile the file after your modifications.


## Technical features
- Software renderer (Dan Zaidan)
- GDI platform layer (Dan Zaidan, improvements by Z)
- Hardware renderer (Z)
- Cross platform audio support (Z)
- SDL2 platform layer (Z)
- GLFW platform layer (Z)
- X11 platform layer (Z)
- Win32 multithreaded job system (Dan Zaidan)
- Cross platform multithreaded job system (Z)
- Win32 mini pthreads wrapper (Z)


## Known bugs
- The audio implementations differ between the GDI, OpenAL and SDL2 versions. This will result in some slight inconsistencies. The most complete and bug-free implementation is done in OpenAL.

- The audio loading code that Mix_LoadWAV_RW uses in the Windows version is 3x slower than the Linux version. This might be dependent on which DLL versions are in use, but I didn't investigate this one too much.


## Porting
If you wish to port this code to a different platform or a different language, here is some pseudocode to get you familiar with the basics. Note that I did not include sound code in this example, nor the file IO that that game does. See [`platform_common.c`](code/platform_common.c) to understand what functions the game expects to have provided.

For sound, see [common_audio_openal.c](code/common_audio_openal.c) for an example of a sound API.

```c
int main()
{
	int render_buffer_width = 720;
	int render_buffer_height = 1280;
	// Can be anything, but the game is hardcoded to run at a 16:9 aspect ratio so this looks the nicest.
	
	global Render_Buffer render_buffer;
	// This is the global pixel buffer that the game uses. 
	// Note that `render_buffer` is the actual variable name that the game expects internally.
	// Declare this in the global scope of your language
	// The pixel format is BGRA_32 LE
	// Defined in platform_common.c
	
	Input game_input;
	// Input struct that's used internally within the game to represent various mouse and key states.
	// Defined in platform_common.c
	
	
	init_game_buffer(render_buffer, render_buffer_width, render_buffer_height); 
	// Allocate enough memory for it and set its members accordingly.
	
	init_native_buffer(); 
	// This is your native framebuffer. In some APIs you don't need to do this and can use the game framebuffer directly.
	
	while(!time_to_quit)
	{
		handle_input(game_input);
		// Platform layer call. Map your platform's input into what the game can use.
		
		update_game(game_input, delta_time_in_seconds);
		// Call the game code. All of the magic happens here.
		
		update_sound();
		// Platform layer call.
		// If sound is supported, go ahead and update it here.
		
		draw_game();
		// Platform layer call. Blit the resulting pixel buffer to the screen.
		
		vsync();
		// Optional: Some function that caps the frame rate at your monitor's native refresh rate.
	}
}

// Here you implement each function in whatever way is best suited for your OS/language to do each task.
// The game calls these to do basic IO.
os_read_entire_file() {}
os_write_save_file() {}
os_free_file() {}
...
// These are declared in platform_common.c
```

## Final notes
Do check out the archived [livestreams](https://www.youtube.com/playlist?list=PL7Ej6SUky1357r-Lqf_nogZWHssXP-hvH) by Dan. He shows the entire process of creating the original game, from the first line of code to the last, and there is some great educational value to be had from individual episodes even if you don't plan on watching the entire series.


## References
[Break Arcade Games Out (Github - Original source)](https://github.com/DanZaidan/break_arcade_games_out)

[Break Arcade Games Out Development Stream (By Dan Zaidan)](https://www.youtube.com/playlist?list=PL7Ej6SUky1357r-Lqf_nogZWHssXP-hvH)

[Xlib Programming Manual](https://tronche.com/gui/x/xlib/)

[X11 Shared memory extension](https://x.org/releases/X11R7.7/doc/xextproto/shm.html)

[Extended Window Manager Hints](https://specifications.freedesktop.org/wm-spec/wm-spec-1.3.html)

[GLFW3 Documentation](https://www.glfw.org/docs/latest/)

[OpenGL Documentation](https://docs.gl/)

[SDL Documentation](http://wiki.libsdl.org/)

[STB Libraries (Github)](https://github.com/nothings/stb)

[WAVE Format Specification](http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html)

[VSync on Windows](https://docs.microsoft.com/en-us/windows/win32/api/dwmapi/nf-dwmapi-dwmflush)
